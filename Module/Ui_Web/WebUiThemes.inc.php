<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Load themes
  // Include Javascript
  $sr_Filename = '' ;
  foreach (
    glob (
      __DIR__ .
      DIRECTORY_SEPARATOR .
      'Theme_Ui_Web' .
      DIRECTORY_SEPARATOR .
      'js' .
      DIRECTORY_SEPARATOR .
      '*.inc.js'
    )
    as $sr_Filename
  ) {
    $ar_Ui['ar_Uri']['ar_Js'][] = str_replace (
      DIRECTORY_SEPARATOR ,
      '/' ,
      str_replace ( $ar_App['ar_Path']['sr_Base'] , $ar_Ui['ar_Uri']['sr_Base'] , realpath ( $sr_Filename ) )
    ) ;
  }

  // Include CSS
  $sr_Filename = '' ;
  foreach (
    glob (
      __DIR__ .
      DIRECTORY_SEPARATOR .
      'Theme_Ui_Web' .
      DIRECTORY_SEPARATOR .
      'css' .
      DIRECTORY_SEPARATOR .
      '*.css'
    )
    as $sr_Filename
  ) {
    
    $ar_Ui['ar_ThemeList'][] = array (
      'sr_Name' => ucwords ( array_shift ( explode ( '.' , array_pop ( explode ( DIRECTORY_SEPARATOR , $sr_Filename ) ) ) ) ) ,
      'sr_Url' => str_replace (
        DIRECTORY_SEPARATOR ,
        '/' ,
        str_replace ( $ar_App['ar_Path']['sr_Base'] , $ar_Ui['ar_Uri']['sr_Base'] , realpath ( $sr_Filename ) )
      )
    ) ;
    
  } // foreach css file in Web Ui theme directory
  
  // The following code adds the first element of the theme list array to the UI CSS list, so it becomes the active theme
  // This is usually alphabetical, but not guaranteed
  // This should be changed to follow Odux active module selection logic
  $ar_Ui['ar_Uri']['ar_Css'][] = $ar_Ui['ar_ThemeList'][0]['sr_Url'] ;

?>