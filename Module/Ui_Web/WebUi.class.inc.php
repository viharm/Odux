<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  require_once ( realpath ( __DIR__ . DIRECTORY_SEPARATOR . 'Renderer.class.php' ) ) ;

  class cl_WebUi {
    
    // Create a Smarty object as soon as an object is created from this class
    public function __construct() {
      require_once ( realpath ( __DIR__ . DIRECTORY_SEPARATOR . 'Command.dict.php' ) ) ;
      @fn_Debug ( 'Loading command map from included file into class variable' , $ar_Ui_Command ) ;
      $this->ar_Ui_Command = $ar_Ui_Command ;
      @fn_Debug ( 'Command map loaded? included command map variable' , $this->ar_Ui_Command ) ;
      unset($ar_Ui_Command) ;
      @fn_Debug ( 'Checking if renderer class exists' ) ;
      if ( class_exists('cl_Renderer') ) {
        @fn_Debug ( 'Found renderer class' ) ;
        $this->ob_Renderer = new cl_Renderer() ;
        if ( $GLOBALS['bl_DebugSwitch'] == TRUE ) {
          $this->ob_Renderer->debugging = TRUE ;
          $this->ob_Renderer->clearAllCache() ;
        }
      }
    }
    
    // Define the abstraction layer to transfer functions from the controller to the renderer
    function __call ( $sr_CommandName , $vr_MethodArg ) {
      
      @fn_Debug ( 'Command' , $sr_CommandName ) ;
      fn_Debug ( 'Arguments provided' , $vr_MethodArg ) ;
      fn_Debug ( 'Translated method' , $this->ar_Ui_Command[$sr_CommandName]['sr_ModuleCmd'] ) ;
      $rt_Result = call_user_func_array (
        array (
          $this->ob_Renderer ,
          $this->ar_Ui_Command[$sr_CommandName]['sr_ModuleCmd']
        ) ,
        $vr_MethodArg
      ) ;
      @fn_Debug ( 'Returning result' , $rt_Result ) ;
      return $rt_Result ;
    }
  }
?>