{* Smary *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
      <div id="dv_PageFooter" class="container-fluid">
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            Status
            <a href="#dv_PageNav" title="Go to top of page">
              <span class="glyphicon glyphicon-chevron-up pull-right btn btn-default btn-xs"></span>
            </a>
          </div>
          <div class="panel-body">
            <div class="row">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
              <div class="col-xs-12 col-sm-4 text-center">
                <small>
                  <span class="glyphicon glyphicon-play-circle text-primary"></span>&nbsp;{$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountActive}
                  <span class="glyphicon glyphicon-time text-info"></span>&nbsp;{$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountWaiting}
                  <span class="glyphicon glyphicon-exclamation-sign text-warning"></span>&nbsp;{$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountStopped}
                </small>
      {$nm_TaskCountTotal=$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountActive+$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountWaiting+$ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountStopped}
                <div class="progress cl_GlobalStatus">
                  {$nm_TaskPercentActive=($ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountActive/$nm_TaskCountTotal)*100}
                  <div class="progress-bar" style="width: {$nm_TaskPercentActive}%">
                    <span class="sr-only">{$nm_TaskPercentActive}% active</span>
                  </div>
                  {$nm_TaskPercentWaiting=($ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountWaiting/$nm_TaskCountTotal)*100}
                  <div class="progress-bar progress-bar-info" style="width: {$nm_TaskPercentWaiting}%">
                    <span class="sr-only">$nm_TaskPercentWaitin% waiting</span>
                  </div>
                  {$nm_TaskPercentStopped=($ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountStopped/$nm_TaskCountTotal)*100}
                  <div class="progress-bar progress-bar-warning" style="width: {$nm_TaskPercentStopped}%">
                    <span class="sr-only">{$nm_TaskPercentStopped}% stopped</span>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 text-center">
                <small>
                  <span class="glyphicon glyphicon-download"></span>
                  {$ar_Ui.ar_Downloader.ar_Global.ar_Stat.sr_SpeedDown}
                  /
                  {$ar_Ui.ar_Downloader.ar_Global.ar_Config.sr_GlobalLimitDown}
                </small>
                <div class="progress cl_GlobalStatus">
      {$nm_SpeedPercent=($ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_SpeedDown/$ar_Ui.ar_Downloader.ar_Global.ar_Config.nm_GlobalLimitDown)*100}
      {if $nm_SpeedPercent > 100}
        {assign var=sr_ProgressTypeWarning value=' progress-bar-warning'}
      {else}
        {assign var=sr_ProgressTypeWarning value=''}
      {/if}
                  <div class="progress-bar{$sr_ProgressTypeWarning}" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="{$nm_SpeedPercent}" style="width:{$nm_SpeedPercent}%;">
                    <span class="sr-only">{$ar_Ui.ar_Downloader.ar_Global.ar_Stat.sr_SpeedDown}</span>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 text-center">
                <small>
                  <span class="glyphicon glyphicon-upload"></span>
                  {$ar_Ui.ar_Downloader.ar_Global.ar_Stat.sr_SpeedUp}
                  /
                  {$ar_Ui.ar_Downloader.ar_Global.ar_Config.sr_GlobalLimitUp}
                </small>
                <div class="progress cl_GlobalStatus">
      {$nm_SpeedPercent=($ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_SpeedUp/$ar_Ui.ar_Downloader.ar_Global.ar_Config.nm_GlobalLimitUp)*100}
      {if $nm_SpeedPercent > 100}
        {assign var=sr_ProgressTypeWarning value=' progress-bar-warning'}
      {else}
        {assign var=sr_ProgressTypeWarning value=''}
      {/if}
                  <div class="progress-bar{$sr_ProgressTypeWarning}" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="{$nm_SpeedPercent}" style="width:{$nm_SpeedPercent}%;">
                    <span class="sr-only">{$ar_Ui.ar_Downloader.ar_Global.ar_Stat.sr_SpeedUp}</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-center">
                <small>
                  <span class="glyphicon glyphicon-hdd"></span>
                  {$ar_Ui.ar_Downloader.ar_Disk.sr_UsedSpace}
                  /
                  {$ar_Ui.ar_Downloader.ar_Disk.sr_Size}
                </small>
                <div class="progress cl_GlobalStatus">
      {$nm_DiskPercent=($ar_Ui.ar_Downloader.ar_Disk.nm_UsedSpace/$ar_Ui.ar_Downloader.ar_Disk.nm_Size)*100}
      {if $nm_DiskPercent > 90}
        {assign var=sr_ProgressTypeWarning value=' progress-bar-danger'}
      {elseif $nm_DiskPercent > 70}
        {assign var=sr_ProgressTypeWarning value=' progress-bar-warning'}
      {else}
        {assign var=sr_ProgressTypeWarning value=''}
      {/if}
                  <div class="progress-bar{$sr_ProgressTypeWarning}" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="{$nm_DiskPercent}" style="width:{$nm_DiskPercent}%;">
                    <span class="sr-only">{$ar_Ui.ar_Downloader.ar_Disk.sr_UsedSpace}</span>
                  </div>
                </div>
              </div>
    {else}
              <div class="col-xs-12">
                <div class="alert alert-warning" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Error:</span>
                  No connection to downloader
                </div>
              </div>
    {/if}
            </div>
          </div> <!-- Body of status panel .panel-body -->
    {if $ar_Ui.ar_Status.sr_Mesg}
          <div class="panel-footer text-center">{$ar_Ui.ar_Status.sr_Mesg}</div>
    {/if}
        </div> <!-- .panel-default Status panel -->
        
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-6">
                <p>
                  &copy; {$smarty.now|date_format:"%Y"} {$ar_App.sr_Author}
{/nocache}
                </p>
              </div>
              <div class="col-xs-6">
                <p class="text-right">
                  Powered by <span class="cl_Odux text-lg">{$ar_App.sr_Name}</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
