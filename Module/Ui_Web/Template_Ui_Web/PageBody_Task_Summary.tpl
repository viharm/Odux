{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_TabulateArrayInRows nm_Level=0 bl_ValOnly=false}
    {foreach from=$ar_InputArray item=vr_ArrayItem}
      <tr>
      {if ! $bl_ValOnly}
        <th>{$vr_ArrayItem.sr_Title}</th>
      {/if}
        <td>
        {if is_array($vr_ArrayItem.vr_Value)}
          <table class="table-condensed table-striped" id="tl_Downloader_{$sr_InfoType}_{$vr_ArrayItem@key}_{$nm_Level+1}">
            <tbody>
              {fn_TabulateArrayInRows ar_InputArray=$vr_ArrayItem.vr_Value nm_Level=$nm_Level+1}
            </tbody>
          </table>
        {else}
          {$vr_ArrayItem.vr_Value}
        {/if}
        </td>
      </tr>
    {/foreach}
  {/function}
{* Page start *}
      <div id="dv_PageBody" class="container-fluid">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
      {if $ar_Ui.ar_Downloader.ar_Task.bl_Context}
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            <h3 class="panel-title">Task <em>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}</em></h3>
          </div> <!-- .panel-heading -->
          <table class="table text-center cl_CompactNav">
            <tr class="list-group">
              <td>
                <a id="ln_Task_Summary" href="Task_Summary.php?sr_DownloaderInstruction=SummariseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}__Basic&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item active">
                  <span class="hidden-xs">Summary</span>
                  <span class="glyphicon glyphicon-info-sign"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Source" href="Task_Source.php?sr_DownloaderInstruction=SummariseTaskSource__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Sources</span>
                  <span class="glyphicon glyphicon-cloud"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Conn" href="Task_Conn.php?sr_DownloaderInstruction=SummariseTaskConn__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Connections</span>
                  <span class="glyphicon glyphicon-link"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Peer" href="Task_Peer.php?sr_DownloaderInstruction=SummariseTaskPeer__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Peers</span>
                  <span class="glyphicon glyphicon-user"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_File" href="Task_File.php?sr_DownloaderInstruction=SummariseTaskFile__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Files</span>
                  <span class="glyphicon glyphicon-file"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Manage" href="Task_Manage.php?sr_DownloaderInstruction=SummariseTaskConfig__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Manage</span>
                  <span class="glyphicon glyphicon-cog"></span>
                </a>
              </td>
            </tr>
          </table>
        </div> <!-- .panel -->
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Active"}
        <div class="panel panel-primary">
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Queued" or $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Paused"}
        <div class="panel panel-info">
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Complete"}
        <div class="panel panel-success">
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Error"}
        <div class="panel panel-warning">
        {else}
        <div class="panel panel-default">
        {/if}
          <div class="panel-heading">
            <h3 class="panel-title">
              <span>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value}</span>
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Active"}
              <span class="glyphicon glyphicon-transfer pull-right"></span>
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Queued" or $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Paused"}
              <span class="glyphicon glyphicon-hourglass pull-right"></span>
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Complete"}
              <span class="glyphicon glyphicon-ok pull-right"></span>
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Removed"}
              <span class="glyphicon glyphicon-remove pull-right"></span>
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Error"}
              <span class="glyphicon glyphicon-warning-sign pull-right"></span>
        {else}
              <span class="glyphicon glyphicon-asterisk pull-right"></span>
        {/if}
            </h3>
          </div> <!-- .panel-heading -->
          <form id="fm_Downloader_Task" name="fm_Downloader_Task" method="post" action="Task_Summary.php" class="form-horizontal" role="form">
            <input name="sr_TaskRef" value="{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" type="hidden" />
            <table class="table text-center cl_CompactNav">
              <tr class="list-group">
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Active" or $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Queued"}
                <td>
                    <button id="bt_TaskPause" name="bt_DownloaderControl" value="PauseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Pause</span>
                      <span class="glyphicon glyphicon-pause"></span>
                    </button>
                </td>
                <td>
                    <button id="bt_TaskPauseForce" name="bt_DownloaderControl" value="PauseTaskForce__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Force pause</span>
                      <span class="glyphicon glyphicon-stop"></span>
                    </button>
                </td>
        {elseif $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Paused"}
                <td>
                    <button id="bt_TaskResume" name="bt_DownloaderControl" value="ResumeTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Resume</span>
                      <span class="glyphicon glyphicon-play"></span>
                    </button>
                </td>
        {/if}
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Error"}
                <td>
                    <button id="bt_TaskErrorInfo" data-target="#dg_ErrorInfo" name="bt_DownloaderControl" value="ErrorInfo" data-toggle="modal" class="list-group-item" type="button">
                      <span class="hidden-xs">Error info</span>
                      <span class="glyphicon glyphicon-comment"></span>
                    </button>
                </td>
        {/if}
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Removed" or $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Error" or $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Complete"}
                <td>
                    <button id="bt_TaskResume" name="bt_DownloaderControl" value="RemoveStoppedTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Remove stopped</span>
                      <span class="glyphicon glyphicon-erase"></span>
                    </button>
                </td>
        {else}
                <td>
                    <button id="bt_TaskRemove" name="bt_DownloaderControl" value="RemoveTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Remove</span>
                      <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </td>
                <td>
                    <button id="bt_TaskRemoveForce" name="bt_DownloaderControl" value="RemoveTaskForce__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item" type="submit">
                      <span class="hidden-xs">Force remove</span>
                      <span class="glyphicon glyphicon-fire"></span>
                    </button>
                </td>
        {/if}
              </tr>
            </table>
            <input name="sr_DownloaderInstruction" value="SummariseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}__Basic" type="hidden" />
          </form>
            <ul class="list-group">
              <li data-target="#cl_ProgressMap" data-toggle="modal"  class="list-group-item cl_Clickable">
        {$nm_SizePercent=($ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSizeCompleted.vr_Value/$ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSizeTotal.vr_Value)*100}
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-save"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Downloaded
                      <div>
                        {$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSizeCompleted.vr_Value}
                        <span class="pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSizeTotal.vr_Value}</span>
                        <div class="progress">
                          <span aria-valuenow="{$nm_SizePercent}" aria-valuemin="0" aria-valuemax="100" style="width:{$nm_SizePercent}%;" class="progress-bar progress-bar-success" role="progressbar"></span>
                        </div>
                      </div>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
        {$nm_SizePercent=($ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSpeedDown.vr_Value/$ar_Ui.ar_Downloader.ar_Task.ar_Config.nm_TaskSpeedDownCeiling.vr_Value)*100}
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-download"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Download speed
                      <div>
                        {$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSpeedDown.vr_Value}
                        <span class="pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Config.sr_TaskSpeedDownCeiling.vr_Value}</span>
                        <div class="progress">
                          <span aria-valuenow="{$nm_SizePercent}" aria-valuemin="0" aria-valuemax="100" style="width:{$nm_SizePercent}%;" class="progress-bar progress-bar-success" role="progressbar"></span>
                        </div>
                      </div>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
        {$nm_SizePercent=($ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSizeUp.vr_Value/$ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSizeTotal.vr_Value)*100}
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-open"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Uploaded
                      <div>
                        {$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSizeUp.vr_Value}
                        <span class="pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSizeTotal.vr_Value}</span>
                        <div class="progress">
                          <span aria-valuenow="{$nm_SizePercent}" aria-valuemin="0" aria-valuemax="100" style="width:{$nm_SizePercent}%;" class="progress-bar progress-bar-success" role="progressbar"></span>
                        </div>
                      </div>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
        {$nm_SizePercent=($ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskSpeedUp.vr_Value/$ar_Ui.ar_Downloader.ar_Task.ar_Config.nm_TaskSpeedUpCeiling.vr_Value)*100}
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-upload"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Upload speed
                      <div>
                        {$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSpeedUp.vr_Value}
                        <span class="pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Config.sr_TaskSpeedUpCeiling.vr_Value}</span>
                        <div class="progress">
                          <span aria-valuenow="{$nm_SizePercent}" aria-valuemin="0" aria-valuemax="100" style="width:{$nm_SizePercent}%;" class="progress-bar progress-bar-success" role="progressbar"></span>
                        </div>
                      </div>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-link"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Connections
                      <span class="badge pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskConnCount.vr_Value}</span>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-folder-open"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Directory
                    </span>
                    <p><code>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSavePath.vr_Value}</code></p>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-th"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Piece count
                      <span class="badge pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskPieceCount.vr_Value}</span>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <span class="glyphicon glyphicon-th-large"></span>
                  </div> <!-- .media-left -->
                  <div class="media-body">
                    <span class="media-heading">
                      Piece size
                      <span class="badge pull-right">{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskSizePiece.vr_Value}</span>
                    </span>
                  </div> <!-- .media-body -->
                </div> <!-- .media -->
              </li>
            </ul>
          </div> <!-- .panel -->
          <div id="cl_ProgressMap" aria-labelledby="hd_ProgressMap" class="modal cl_ProgressMap" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button data-dismiss="modal" aria-label="Close" class="close" type="button"><span aria-hidden="true">&times;</span></button>
                  <h4 id="hd_ProgressMap" class="modal-title">Progress map</h4>
                </div> <!-- .modal-header -->
                <div class="modal-body">
                  {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Summary.ar_ProgressMap.vr_Value item=sr_Colour}
                    <span style="border: 1px solid black;background-color:{$sr_Colour};">&nbsp;</span>
                  {/foreach}
                </div> <!-- .modal-body -->
                <div class="modal-footer">
                  <span class="pull-left text-left">
                    <h5>Legend</h5>
                    None
                    <span style="border: 1px solid black;background-color:#ffffff;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#eeeeee;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#dddddd;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#cccccc;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#bbbbbb;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#aaaaaa;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#999999;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#888888;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#777777;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#666666;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#555555;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#444444;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#333333;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#222222;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#111111;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#000000;">&nbsp;</span>
                    Full
                  </span>
                  <button data-dismiss="modal" class="btn btn-default" type="button" >Close</button>
                </div> <!-- .modal-footer -->
              </div> <!-- .modal-content -->
            </div> <!-- .modal-dialog -->
          </div> <!-- .modal -->
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskStatus.vr_Value === "Error"}
          <div id="dg_ErrorInfo" aria-labelledby="hd_ErrorInfo" class="modal cl_ErrorInfo" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button data-dismiss="modal" aria-label="Close" class="close" type="button"><span aria-hidden="true">&times;</span></button>
                  <h4 id="hd_ErrorInfo" class="modal-title">Error information</h4>
                </div> <!-- .modal-header -->
                <div class="modal-body">
                  <p>
                    {$ar_Ui.ar_Downloader.ar_Task.ar_Summary.nm_TaskErrCode.vr_Value}
                  </p>
                </div> <!-- .modal-body -->
                <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-default" type="button" >Close</button>
                </div> <!-- .modal-footer -->
              </div> <!-- .modal-content -->
            </div> <!-- .modal-dialog -->
          </div> <!-- .modal -->
        {/if}
      {else}
        <div class="alert alert-warning" role="alert">
          <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Task context not available
        </div> <!-- .alert -->
      {/if}
    {else}
        <div class="row">
          <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            No access to configuration
          </div> <!-- .alert -->
        </div> <!-- .row -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
