{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_ListTask}
    {if $ar_Task.sr_TaskStatus == "Active"}
            <div class="panel panel-primary">
    {elseif $ar_Task.sr_TaskStatus == "Queued" or  $ar_Task.sr_TaskStatus == "Paused"}
            <div class="panel panel-info">
    {elseif $ar_Task.sr_TaskStatus == "Complete"}
            <div class="panel panel-success">
    {elseif $ar_Task.sr_TaskStatus == "Error"}
            <div class="panel panel-warning">
    {else}
            <div class="panel panel-default">
    {/if}
    {$nm_SizePercent=($ar_Task.nm_TaskSizeCompleted/$ar_Task.nm_TaskSizeTotal)*100}
              <div class="progress cl_ThinProgress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="{$nm_SizePercent}" style="width:{$nm_SizePercent}%;"></div>
              </div>
              <div id="hd_TaskItem_{$ar_Task.sr_TaskRef}" class="panel-heading cl_Task" role="tab">
                <h5 class="panel-title">
                  <a href="#bd_TaskItem_{$ar_Task.sr_TaskRef}" aria-controls="bd_TaskItem_{$ar_Task.sr_TaskRef}" data-parent="#gr_TaskList" class="collapsed" role="button" data-toggle="collapse">
                    {$ar_Task.sr_TaskRef}
                    <span class="pull-right hidden-xs">{$ar_Task.sr_TaskStatus}</span>
    {if $ar_Task.sr_TaskStatus == "Active"}
                    <span class="glyphicon glyphicon-transfer pull-right visible-xs"></span>
    {elseif $ar_Task.sr_TaskStatus == "Queued" or $ar_Task.sr_TaskStatus == "Paused"}
                    <span class="glyphicon glyphicon-hourglass pull-right visible-xs"></span>
    {elseif $ar_Task.sr_TaskStatus == "Complete"}
                    <span class="glyphicon glyphicon-ok pull-right visible-xs"></span>
    {elseif $ar_Task.sr_TaskStatus == "Removed"}
                    <span class="glyphicon glyphicon-remove pull-right visible-xs"></span>
    {elseif $ar_Task.sr_TaskStatus == "Error"}
                    <span class="glyphicon glyphicon-warning-sign pull-right visible-xs"></span>
    {else}
                    <span class="glyphicon glyphicon-asterisk pull-right visible-xs"></span>
    {/if}
                  </a>
                </h5>
              </div> <!-- .panel-heading -->
              <div id="bd_TaskItem_{$ar_Task.sr_TaskRef}" aria-labelledby="hd_TaskItem_{$ar_Task.sr_TaskRef}" class="panel-collapse collapse" role="tabpanel">
                <table class="table text-center cl_CompactNav">
                  <tr class="list-group">
        {if $ar_Task.sr_TaskStatus == "Active" or $ar_Task.sr_TaskStatus == "Queued"}
                    <td>
                      <button id="bt_TaskPause_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="PauseTask__{$ar_Task.sr_TaskRef}" class="list-group-item" type="submit">
                        <span class="hidden-xs hidden-sm">Pause</span>
                        <span class="glyphicon glyphicon-pause"></span>
                      </button>
                    </td>
        {elseif $ar_Task.sr_TaskStatus == "Paused"}
                    <td>
                      <button id="bt_TaskResume_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="ResumeTask__{$ar_Task.sr_TaskRef}" class="list-group-item" type="submit">
                        <span class="hidden-xs hidden-sm"><small>Resume</small></span>
                        <span class="glyphicon glyphicon-play"></span>
                      </button>
                    </td>
        {/if}
                    <td>
                      <button id="bt_TaskDetail_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="SummariseTask__{$ar_Task.sr_TaskRef}__Basic" class="list-group-item" type="submit">
                        <span class="hidden-xs hidden-sm"><small>Detail</small></span>
                        <span class="glyphicon glyphicon-info-sign"></span>
                      </button>
                    </td>
                    <td>
                      <button id="bt_TaskMoveFwd_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="ChangePosition__{$ar_Task.sr_TaskRef}__MoveFwd" class="list-group-item" type="submit"> {* Need a command dictionary for arguments *}
                        <span class="hidden-xs hidden-sm"><small>Move forward</small></span>
                        <span class="glyphicon glyphicon-menu-up"></span>
                      </button>
                    </td>
                    <td>
                      <button id="bt_TaskMoveBwd_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="ChangePosition__{$ar_Task.sr_TaskRef}__MoveBkwd" class="list-group-item" type="submit">
                        <span class="hidden-xs hidden-sm"><small>Move backward</small></span>
                        <span class="glyphicon glyphicon-menu-down"></span>
                      </button>                          
                    </td>
                    <td>
                      <button id="bt_TaskMoveFront_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="ChangePosition__{$ar_Task.sr_TaskRef}__MoveFront" class="list-group-item" type="submit"> {* Need a command dictionary for arguments *}
                        <span class="hidden-xs hidden-sm"><small>Move to front</small></span>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                      </button>
                    </td>
                    <td>
                      <button id="bt_TaskMoveBack_{$ar_Task.sr_TaskRef}" name="bt_DownloaderControl" value="ChangePosition__{$ar_Task.sr_TaskRef}__MoveBack" class="list-group-item" type="submit">
                        <span class="hidden-xs hidden-sm"><small>Move to back</small></span>
                        <span class="glyphicon glyphicon-thumbs-down"></span>
                      </button>                          
                    </td>
                  </tr>
                </table>
                <div class="panel-body">
                  <div class="row cl_OverviewTaskSummary">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <table class="table table-condensed">
                        <tr>
                          <th>Size</th>
                          <td>{$ar_Task.sr_TaskSizeTotal}</td>
                        </tr>
                        <tr>
                          <th>Connections</th>
                          <td>{$ar_Task.nm_TaskConnCount}</td>
                        </tr>
                        <tr>
                          <th>Files</th>
                          <td>{$ar_Task.ar_TaskFileList|@count}</td>
                        </tr>
                      </table>
                    </div> <!-- col -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <table class="table table-condensed">
                        <tr>
                          <th>Downloaded</th>
                          <td>{$ar_Task.sr_TaskSizeCompleted}</td>
                        </tr>
                        <tr>
                          <th>Download speed</th>
                          <td>{$ar_Task.sr_TaskSpeedDown}</td>
                        </tr>
                      </table>
                    </div> <!-- col -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <table class="table table-condensed">
                        <tr>
                          <th>Uploaded</th>
                          <td>{$ar_Task.sr_TaskSizeUp}</td>
                        </tr>
                        <tr>
                          <th>Upload speed</th>
                          <td>{$ar_Task.sr_TaskSpeedUp}</td>
                        </tr>
                      </table>
                    </div> <!-- col -->
                  </div> <!-- .row -->
                </div> <!-- .panel-body -->
              </div> <!-- panel-collapse -->
            </div> <!-- .panel-[STATUS] -->
  {/function}
      <div id="dv_PageBody" class="container-fluid">
{nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountActive > 0 or $ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountWaiting > 0 or $ar_Ui.ar_Downloader.ar_Global.ar_Stat.nm_CountStopped > 0}
        <form name="fm_Downloader_Control" method="post" action="index.php">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h3 class="panel-title">{$ar_Page.sr_Title}</h3>
            </div> <!-- .panel-heading -->
            <table class="table text-center cl_CompactNav">
              <tr class="list-group">
                <td>
                  <button id="bt_TaskResumeAll" name="bt_DownloaderControl" value="ResumeAll" class="list-group-item" type="submit">
                    <span class="hidden-xs text-center ">Resume all</span>
                    <span class="glyphicon glyphicon-play"></span>
                  </button>
                </td>
                <td>
                  <button id="bt_TaskPauseAll" name="bt_DownloaderControl" value="PauseAll" class="list-group-item" type="submit">
                    <span class="hidden-xs">Pause all</span>
                    <span class="glyphicon glyphicon-pause"></span>
                  </button>
                </td>
                <td>
                  <button id="bt_TaskPurgeStopped" name="bt_DownloaderControl" value="PurgeStoppedTasks" class="list-group-item" type="submit">
                    <span class="hidden-xs">Purge stopped</span>
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                </td>
                <td>
                  <button id="bt_TaskPauseAllForce" name="bt_DownloaderControl" value="PauseAllForce" class="list-group-item" type="submit">
                    <span class="hidden-xs">Force pause all</span>
                    <span class="glyphicon glyphicon-stop"></span>
                  </button>
                </td>
              </tr>
            </table>
          </div> <!-- .panel -->
          <div class="panel-group" id="gr_TaskList" role="tablist" aria-multiselectable="true">
      {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Active item=ar_Task}
        {fn_ListTask ar_Task=$ar_Task}
      {/foreach}
      {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Queued item=ar_Task}
        {fn_ListTask ar_Task=$ar_Task}
      {/foreach}
      {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Stopped item=ar_Task}
        {fn_ListTask ar_Task=$ar_Task}
      {/foreach}
          </div> <!-- .panel-group -->
        </form>
    {else}
        <div class="alert alert-info" role="alert">
          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          No tasks
        </div> <!-- .alert -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
