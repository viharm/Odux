{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
        <div id="dv_PageBody" class="container-fluid">
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h3 class="panel-title">Manage</h3>
            </div> <!-- .panel-heading -->
            <table class="table text-center cl_CompactNav">
              <tr class="list-group">
                <td>
                <a id="ln_Manage_Odux" href="Manage.php" class="list-group-item">
                  <span class="hidden-xs">Odux</span>
                  <span class="glyphicon glyphicon-picture"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Downloader" href="Manage_Downloader.php" class="list-group-item">
                  <span class="hidden-xs">Downloader</span>
                  <span class="glyphicon glyphicon-cd"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Ui" href="Manage_Ui.php" class="list-group-item">
                  <span class="hidden-xs">UI</span>
                  <span class="glyphicon glyphicon-tint"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Admin" href="Manage_Admin.php" class="list-group-item active">
                  <span class="hidden-xs">Admin</span>
                  <span class="glyphicon glyphicon-wrench"></span>
                </a>
                </td>
              </tr>
            </table>
          </div> <!-- .panel -->
          <form class="form-horizontal" role="form" id="fm_Admin" method="post" action="Manage_Admin.php">
            <div class="panel-group" id="gr_Admin" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div id="hd_Admin_Ui" class="panel-heading" role="tab">
                  <h3 class="panel-title">
                    <a href="#bd_Admin_Ui" aria-controls="bd_Admin_Ui" data-parent="#gr_Admin" aria-expanded="false" class="collapsed" role="button" data-toggle="collapse">
                      UI admin
                    </a>
                  </h3>
                </div> <!-- .panel-heading -->
                <div id="bd_Admin_Ui" aria-labelledby="hd_Admin_Ui" class="panel-collapse collapse" role="tabpanel">
                  <div class="panel-body">
                    <div class="form-group">
                      <label class="control-label col-sm-3">Test setup</label>
                      <div class="col-sm-9">
                        <button id="bt_UiControl_TestSetup" name="bt_UiControl" value="TestSetup" class="btn btn-info" type="submit">
                          <span class="glyphicon glyphicon-scale"></span>
                          Test setup
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <div class="form-group">
                      <label class="control-label col-sm-3">Disable security</label>
                      <div class="col-sm-9">
                        <button id="bt_UiControl_SecureDisable" name="bt_UiControl" value="SecureDisable" class="btn btn-warning" type="submit">
                          <span class="glyphicon glyphicon-globe"></span>
                          Disable security
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <div class="form-group">
                      <label class="control-label col-sm-3">Enable security</label>
                      <div class="col-sm-9">
                        <button id="bt_UiControl_SecureEnable" name="bt_UiControl" value="SecureEnable" class="btn btn-success" type="submit">
                          <span class="glyphicon glyphicon-certificate"></span>
                          Enable security
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                  </div> <!-- .panel-body -->
                </div> <!-- panel-collapse -->
              </div> <!-- .panel -->
              <div class="panel panel-default">
                <div id="hd_Admin_Downloader" class="panel-heading" role="tab">
                  <h3 class="panel-title">
                    <a href="#bd_Admin_Downloader" aria-controls="bd_Admin_Downloader" data-parent="#gr_Admin" aria-expanded="false" class="collapsed" role="button" data-toggle="collapse">
                      Downloader admin
                    </a>
                  </h3>
                </div> <!-- .panel-heading -->
                <div id="bd_Admin_Downloader" aria-labelledby="hd_Admin_Downloader" class="panel-collapse collapse" role="tabpanel">
                  <div class="panel-body">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
                    <div class="form-group">
                      <label class="control-label col-sm-3">Save downloader session</label>
                      <div class="col-sm-9">
                        <button id="bt_Downloader_SaveSession" name="bt_DownloaderControl" value="SaveSession" class="btn btn-success" type="submit">
                          <span class="glyphicon glyphicon-export"></span>
                          Save session
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <div class="form-group">
                      <label class="control-label col-sm-3">Shutdown downloader</label>
                      <div class="col-sm-9">
                        <button id="bt_Downloader_Exit" name="bt_DownloaderControl" value="Exit" class="btn btn-warning" type="submit">
                          <span class="glyphicon glyphicon-off"></span>
                          Close
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <div class="form-group">
                      <label class="control-label col-sm-3">Force shutdown Aria2</label>
                      <div class="col-sm-9">
                        <button id="bt_Downloader_Kill" name="bt_DownloaderControl" value="Kill" class="btn btn-danger" type="submit">
                          <span class="glyphicon glyphicon-exclamation-sign"></span>
                          Force close
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <h4>Downloader custom configuration</h4>
                    <div class="form-group">
                      <div class="col-sm-3">
                        <input id="tx_Downloader_Option_Name" name="tx_Downloader_Option_Name" placeholder="Option name" class="form-control" maxlength="64" type="text" />
                      </div> <!-- .col -->
                      <div class="col-sm-9">
                        <input id="tx_Downloader_Option_Value" name="tx_Downloader_Option_Value" placeholder="Option value" class="form-control" maxlength="64" type="text" />
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
                    <div class="form-group">        
                      <div class="col-sm-offset-3 col-sm-9">
                        <button id="bt_Downloader_SetGlobalConfig" name="bt_DownloaderControl" value="SetGlobalConfig" class="btn btn-success" type="submit">
                          <span class="glyphicon glyphicon-floppy-disk"></span>
                          Save
                        </button>
                      </div> <!-- .col -->
                    </div> <!-- .form-group -->
    {else}
                    <div class="alert alert-info" role="alert">
                      <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                      <span class="sr-only">Error:</span>
                      No access to administration. Please see status for more information.
                    </div> <!-- .alert -->
    {/if}
  {/nocache}
                  </div> <!-- .panel-body -->

                </div> <!-- panel-collapse -->
              </div> <!-- .panel -->
            </div> <!-- .panel-group -->
          </form>
        </div>
