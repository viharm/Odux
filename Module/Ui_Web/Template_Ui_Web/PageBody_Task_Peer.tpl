{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_ListTaskPeer}
    {if $ar_TaskPeer.bl_TaskPeerSeederFlag.vr_Value}
      {assign var='sr_TaskPeerSeederFlag' value='Yes'}
    {else}
      {assign var='sr_TaskPeerSeederFlag' value='No'}
    {/if}
    {if $ar_TaskPeer.bl_PeerChokedRemote.vr_Value}
      {assign var='sr_PeerChokedRemote' value='Yes'}
    {else}
      {assign var='sr_PeerChokedRemote' value='No'}
    {/if}
    {if $ar_TaskPeer.bl_PeerChokedLocal.vr_Value}
      {assign var='sr_PeerChokedLocal' value='Yes'}
    {else}
      {assign var='sr_PeerChokedLocal' value='No'}
    {/if}
      {if $ar_TaskPeer.bl_TaskPeerSeederFlag.vr_Value}
          <div class="panel panel-primary">
      {else}
          <div class="panel panel-info">
      {/if}
            <div id="hd_TaskPeer_{$nm_TaskPeerIndex}" class="panel-heading" role="tab">
              <h5 class="panel-title">
                <a href="#bd_TaskPeer_{$nm_TaskPeerIndex}" aria-controls="bd_TaskPeer_{$nm_TaskPeerIndex}" data-toggle="collapse" data-parent="#gr_TaskPeerList" class="collapsed" role="button">
                  {$ar_TaskPeer.sr_TaskPeerRef.vr_Value|escape}
                </a>
              </h5>
            </div> <!-- .panel-heading -->
            <div id="bd_TaskPeer_{$nm_TaskPeerIndex}" aria-labelledby="hd_TaskPeer_{$nm_TaskPeerIndex}" class="panel-collapse collapse" role="tabpanel">
              <ul class="list-group">
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-screenshot"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Address
                      </span>
                      <p><code>{$ar_TaskPeer.sr_TaskPeerIp.vr_Value}:{$ar_TaskPeer.nm_TaskPeerPort.vr_Value}</code></p>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-cloud-download"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Seeder?
                        <span class="badge pull-right">{$sr_TaskPeerSeederFlag}</span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-ban-circle"></span>
                      <span class="glyphicon glyphicon-import"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Local choked
                        <span class="badge pull-right">{$sr_PeerChokedLocal}</span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-ban-circle"></span>
                      <span class="glyphicon glyphicon-export"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Remote choked
                        <span class="badge pull-right">{$sr_PeerChokedRemote}</span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-download"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Download speed
                        <span class="badge pull-right">{$ar_TaskPeer.sr_TaskPeerSpeedDown.vr_Value}</span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-upload"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Upload speed
                        <span class="badge pull-right">{$ar_TaskPeer.sr_TaskPeerSpeedUp.vr_Value}</span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li data-target="#cl_ProgressMap_{$nm_TaskPeerIndex}" data-toggle="modal" class="list-group-item cl_Clickable">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-equalizer"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Progress map
                        <span class="badge pull-right"><span class="glyphicon glyphicon-hand-up"></span></span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
              </ul>
            </div> <!-- panel-collapse -->
          </div> <!-- .panel-[STATUS] -->
  {/function}
{* Page start *}
      <div id="dv_PageBody" class="container-fluid">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
      {if $ar_Ui.ar_Downloader.ar_Task.bl_Context}
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            <h3 class="panel-title">Task <em>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}</em></h3>
          </div> <!-- .panel-heading -->
          <table class="table text-center cl_CompactNav">
            <tr class="list-group">
              <td>
                <a id="ln_Task_Summary" href="Task_Summary.php?sr_DownloaderInstruction=SummariseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}__Basic&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Summary</span>
                  <span class="glyphicon glyphicon-info-sign"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Source" href="Task_Source.php?sr_DownloaderInstruction=SummariseTaskSource__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Sources</span>
                  <span class="glyphicon glyphicon-cloud"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Conn" href="Task_Conn.php?sr_DownloaderInstruction=SummariseTaskConn__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Connections</span>
                  <span class="glyphicon glyphicon-link"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Peer" href="Task_Peer.php?sr_DownloaderInstruction=SummariseTaskPeer__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item active">
                  <span class="hidden-xs">Peers</span>
                  <span class="glyphicon glyphicon-user"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_File" href="Task_File.php?sr_DownloaderInstruction=SummariseTaskFile__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Files</span>
                  <span class="glyphicon glyphicon-file"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Manage" href="Task_Manage.php?sr_DownloaderInstruction=SummariseTaskConfig__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Manage</span>
                  <span class="glyphicon glyphicon-cog"></span>
                </a>
              </td>
            </tr>
          </table>
        </div> <!-- .panel -->
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Peer|@count > 0}
        <div class="panel-group" id="gr_TaskPeerList" role="tablist" aria-multiselectable="true">
          {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Peer item=ar_TaskPeer key=nm_TaskPeerKey}
            {assign var='nm_TaskPeerIndex' value=$nm_TaskPeerKey+1}
            {fn_ListTaskPeer ar_TaskPeer=$ar_TaskPeer nm_TaskPeerIndex=$nm_TaskPeerIndex}
          <div id="cl_ProgressMap_{$nm_TaskPeerIndex}" aria-labelledby="hd_ProgressMap_{$nm_TaskPeerIndex}" class="modal cl_ProgressMap" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button data-dismiss="modal" aria-label="Close" class="close" type="button"><span aria-hidden="true">&times;</span></button>
                  <h4 id="hd_ProgressMap_{$nm_TaskPeerIndex}" class="modal-title">Progress map</h4>
                </div> <!-- .modal-header -->
                <div class="modal-body">
            {foreach from=$ar_TaskPeer.ar_ProgressMap.vr_Value item=sr_Colour}
                    <span style="border: 1px solid black;background-color:{$sr_Colour};">&nbsp;</span>
            {/foreach}
                </div> <!-- .modal-body -->
                <div class="modal-footer">
                  <span class="pull-left text-left">
                    <h5>Legend</h5>
                    None
                    <span style="border: 1px solid black;background-color:#ffffff;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#eeeeee;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#dddddd;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#cccccc;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#bbbbbb;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#aaaaaa;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#999999;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#888888;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#777777;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#666666;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#555555;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#444444;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#333333;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#222222;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#111111;">&nbsp;</span>
                    <span style="border: 1px solid black;background-color:#000000;">&nbsp;</span>
                    Full
                  </span>
                  <button data-dismiss="modal" class="btn btn-default" type="button" >Close</button>
                </div> <!-- .modal-footer -->
              </div> <!-- .modal-content -->
            </div> <!-- .modal-dialog -->
          </div> <!-- cl_ProgressMap -->
          {/foreach}
        </div> <!-- .panel-group -->
        {else}
        <div class="alert alert-info" role="alert">
          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          No peers
        </div> <!-- .alert -->
        {/if}
      {else}
        <div class="alert alert-warning" role="alert">
          <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Task context not available
        </div> <!-- .alert -->
      {/if}
    {else}
          <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            No connection to downloader
          </div> <!-- .alert -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
