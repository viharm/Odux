{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
      <div id="dv_PageNav" class="container-fluid">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button id="bt_ToggleNavbar_Page" data-toggle="collapse" data-target="#nv_Page" class="navbar-toggle" type="button">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="{$ar_Ui.ar_Uri.sr_Base}">{$ar_Site.sr_Name}</a>
          </div>
          <div class="collapse navbar-collapse" id="nv_Page">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="{$ar_Ui.ar_Uri.sr_Base}" class="text-center">
                  <span class="glyphicon glyphicon-tasks"></span>
                  Overview
                </a>
              </li>
              <li role="separator" class="divider"><hr /></li>
              <li>
                <a href="{$ar_Ui.ar_Uri.sr_Base}Add.php" class="text-center">
                  <span class="glyphicon glyphicon-plus"></span>
                  Add
                </a>
              </li>
              <li role="separator" class="divider"><hr /></li>
              <li>
                <a href="{$ar_Ui.ar_Uri.sr_Base}Manage.php" class="text-center">
                  <span class="glyphicon glyphicon-cog"></span>
                  Manage
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
