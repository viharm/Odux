{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_TabulateArrayInRows nm_Level=0 bl_ValOnly=false}
    {foreach $ar_InputArray as $vr_ArrayItem}
      <tr>
      {if ! $bl_ValOnly}
        <th>{$vr_ArrayItem@key}</th>
      {/if}
        <td>
        {if is_array($vr_ArrayItem)}
          <table class="table-condensed table-striped" id="tl_Downloader_{$sr_InfoType}_{$vr_ArrayItem@key}_{$nm_Level+1}">
            <tbody>
              {fn_TabulateArrayInRows ar_InputArray=$vr_ArrayItem nm_Level=$nm_Level+1}
            </tbody>
          </table>
        {else}
          {$vr_ArrayItem}
        {/if}
        </td>
      </tr>
    {/foreach}
  {/function}
  {function name=fn_OrganiseArrayInDescr bl_ExcludeNull=false}
    {foreach $ar_InputArray as $vr_ArrayItem}
      {if is_null($vr_ArrayItem.vr_Value) and $bl_ExcludeNull}
      {else}
      <dl class="dl-horizontal">
        <dt>{$vr_ArrayItem.sr_Title}</dt>
        <dd>
        {if is_array($vr_ArrayItem.vr_Value)}
          <table class="table-condensed table-striped">
          <tbody>
            {fn_TabulateArrayInRows ar_InputArray=$vr_ArrayItem.vr_Value bl_ValOnly=true}
            </tbody>
          </table>
        {else}
          {$vr_ArrayItem.vr_Value}
        {/if}
        </dd>
      </dl>
      {/if}
    {/foreach}
  {/function}
      <div id="dv_PageBody" class="container-fluid">
{nocache}
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h3 class="panel-title">{$ar_Page.sr_Title}</h3>
            </div> <!-- .panel-heading -->
            <table class="table text-center cl_CompactNav">
              <tr class="list-group">
                <td>
                <a id="ln_Manage_Odux" href="Manage.php" class="list-group-item active">
                  <span class="hidden-xs">Odux</span>
                  <span class="glyphicon glyphicon-picture"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Downloader" href="Manage_Downloader.php" class="list-group-item">
                  <span class="hidden-xs">Downloader</span>
                  <span class="glyphicon glyphicon-cd"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Ui" href="Manage_Ui.php" class="list-group-item">
                  <span class="hidden-xs">UI</span>
                  <span class="glyphicon glyphicon-tint"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Admin" href="Manage_Admin.php" class="list-group-item">
                  <span class="hidden-xs">Admin</span>
                  <span class="glyphicon glyphicon-wrench"></span>
                </a>
                </td>
              </tr>
            </table>
          </div> <!-- .panel -->
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="panel-title">Client information</h4>
            </div>
            <div class="panel-body">
              {fn_OrganiseArrayInDescr ar_InputArray=$ar_Ui.ar_ClientInfo bl_ExcludeNull=true}
            </div> <!-- .panel-body -->
          </div> <!-- .panel -->
  {/nocache}
      </div> <!-- #dv_PageBody -->
