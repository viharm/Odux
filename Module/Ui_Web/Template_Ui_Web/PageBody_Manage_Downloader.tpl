{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_TabulateArrayInRows nm_Level=0 bl_ValOnly=false}
    {foreach $ar_InputArray as $vr_ArrayItem}
      <tr>
      {if ! $bl_ValOnly}
        <th>{$vr_ArrayItem@key}</th>
      {/if}
        <td>
        {if is_array($vr_ArrayItem)}
          <table class="table-condensed table-striped" id="tl_Downloader_{$sr_InfoType}_{$vr_ArrayItem@key}_{$nm_Level+1}">
            <tbody>
              {fn_TabulateArrayInRows ar_InputArray=$vr_ArrayItem nm_Level=$nm_Level+1}
            </tbody>
          </table>
        {else}
          {$vr_ArrayItem}
        {/if}
        </td>
      </tr>
    {/foreach}
  {/function}
  {function name=fn_ListArrayUnordered nm_Level=0}
    <ul>
    {foreach $ar_InputArray as $vr_ArrayItem}
      <li>
        {if is_array($vr_ArrayItem)}
        <ul>
          {fn_ListArrayUnordered ar_InputArray=$vr_ArrayItem nm_Level=$nm_Level+1}
        </ul>
        {else}
          {$vr_ArrayItem}
        {/if}
      </li>
    {/foreach}
    </ul>
  {/function}
  {function name=fn_OrganiseArrayInDescr bl_ExcludeNull=false}
    {foreach $ar_InputArray as $vr_ArrayItem}
      {if is_null($vr_ArrayItem.vr_Value) and $bl_ExcludeNull}
      {else}
      <dl class="dl-horizontal">
        <dt>{$vr_ArrayItem.sr_Title}</dt>
        <dd>
        {if is_array($vr_ArrayItem.vr_Value)}
          <table class="table-condensed table-striped">
          <tbody>
            {fn_ListArrayUnordered ar_InputArray=$vr_ArrayItem.vr_Value}
            </tbody>
          </table>
        {else}
          <span class="cl_StringWrap">
            {$vr_ArrayItem.vr_Value}
     		  </span>
        {/if}
        </dd>
      </dl>
      {/if}
    {/foreach}
  {/function}
  {function name=fn_ConfigArrayLayout}
    {foreach $ar_ConfigArray item=ar_ConfigVal key=vr_ConfigParam}
      {assign var='sr_ValType' value=$vr_ConfigParam|substr:0:2}
      {if $ar_ConfigVal.bl_Readonly}
        {if $sr_ValType == 'bl'}
          {if $ar_ConfigVal.vr_Value === true}
            {$ar_ConfigVal.vr_Value = 'On'}
          {else}
            {$ar_ConfigVal.vr_Value = 'Off'}
          {/if}
        {/if}
        {$sr_ValType='sr'}
        {assign var='sr_Readonly' value=' readonly'}
      {else}
        {assign var='sr_Readonly' value=''}
      {/if}
        <div class="form-group">
          <label class="control-label col-sm-3">{$ar_ConfigVal.sr_Title}</label>
          <div class="col-sm-7">
      {if $sr_ValType == 'bl'}
        {if $ar_ConfigVal.vr_Value == true}
          {assign var='sr_CheckBoxStatus' value='checked'}
        {else}
          {assign var='sr_CheckBoxStatus' value=''}
        {/if}
            <input id="ck_Downloader_{$vr_ConfigParam}" name="ck_Downloader_{$vr_ConfigParam}" {$sr_CheckBoxStatus} onchange="fn_IdentifyChangedField('ck_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')" type="checkbox"{$sr_Readonly} />
      {else}
        {if is_array($ar_ConfigVal.ar_AltValueList) and $ar_ConfigVal.bl_Readonly === false}
            <select id="cb_Downloader_{$vr_ConfigParam}" name="cb_Downloader_{$vr_ConfigParam}" checked onchange="fn_IdentifyChangedField('cb_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')"  class="form-control"{$sr_Readonly}>
          {foreach $ar_ConfigVal.ar_AltValueList item=vr_AltValue}
            {if $vr_AltValue == $ar_ConfigVal}
              {assign var='sr_DefaultOption' value=' selected="true"'}
            {else}
              {assign var='sr_DefaultOption' value=''}
            {/if}
              <option{$sr_DefaultOption}>{$vr_AltValue}</option>
          {/foreach}
            </select>
        {else}
          {if $ar_ConfigVal.bl_Readonly}
            <p class="form-control-static">{$ar_ConfigVal.vr_Value}</p>
          {else}
            <input id="tx_Downloader_{$vr_ConfigParam}" name="tx_Downloader_{$vr_ConfigParam}" value="{$ar_ConfigVal.vr_Value}" placeholder="{$ar_ConfigVal.sr_DefaultVal}" onchange="fn_IdentifyChangedField('tx_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')" class="form-control" type="text"{$sr_Readonly} />
          {/if}
        {/if}
      {/if}
            <span id="lb_Help_{$vr_ConfigParam}" class="help-block">{$ar_ConfigVal.sr_Descr|escape}</span>
          </div> <!-- .col -->
          <div class="col-sm-2">
      {if ! $ar_ConfigVal.bl_Readonly}
            <div class="checkbox text-right">
              <label>
                <input id="ck_Downloader_Changed_{$vr_ConfigParam}" name="ck_Downloader_Changed_{$vr_ConfigParam}" type="checkbox" /> Changed
              </label>
            </div> <!-- .checkbox -->
      {/if}
          </div> <!-- .col -->
        </div> <!-- .form-group -->
    {/foreach}
  {/function}
{* Page start *}
      <div id="dv_PageBody" class="container-fluid">
  {nocache}
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h3 class="panel-title">{$ar_Page.sr_Title}</h3>
            </div> <!-- .panel-heading -->
            <table class="table text-center cl_CompactNav">
              <tr class="list-group">
                <td>
                <a id="ln_Manage_Odux" href="Manage.php" class="list-group-item">
                  <span class="hidden-xs">Odux</span>
                  <span class="glyphicon glyphicon-picture"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Downloader" href="Manage_Downloader.php" class="list-group-item active">
                  <span class="hidden-xs">Downloader</span>
                  <span class="glyphicon glyphicon-cd"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Ui" href="Manage_Ui.php" class="list-group-item">
                  <span class="hidden-xs">UI</span>
                  <span class="glyphicon glyphicon-tint"></span>
                </a>
                </td>
                <td>
                <a id="ln_Manage_Admin" href="Manage_Admin.php" class="list-group-item">
                  <span class="hidden-xs">Admin</span>
                  <span class="glyphicon glyphicon-wrench"></span>
                </a>
                </td>
              </tr>
            </table>
          </div> <!-- .panel -->
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
          <form id="fm_Downloader_Config" name="fm_Downloader_Config" method="post" action="Manage_Downloader.php" class="form-horizontal" role="form">
            <ul class="nav nav-tabs nav-justified" role="tablist">
              <li class="active" role="presentation">
                <a href="#tp_App" aria-controls="tp_App" role="tab" data-toggle="tab">
                  App
                </a>
              </li>
              <li role="presentation">
                <a href="#tp_Task" aria-controls="tp_Task" role="tab" data-toggle="tab">
                  Task defaults
                </a>
              </li>
            </ul> <!-- .nav-tabs -->
            <br />
            <div class="tab-content">
              <div id="tp_App" class="tab-pane active" role="tabpanel">
        {fn_ConfigArrayLayout ar_ConfigArray=$ar_Ui.ar_Downloader.ar_Global.ar_ConfigPage.ar_App}
              </div> <!-- .tab-pane -->
              <div id="tp_Task" class="tab-pane" role="tabpanel">
                <div id="gr_TaskConfigList" class="panel-group" role="tablist" aria-multiselectable="true">
        {foreach $ar_Ui.ar_Downloader.ar_Global.ar_ConfigPage.ar_Task item=ar_ConfigTaskConfig key=sr_ConfigTaskType}
          {if $ar_ConfigTaskConfig|@count > 0 }
                  <div class="panel panel-default">
                    <div id="hd_TaskConfigItem_{$sr_ConfigTaskType}" class="panel-heading" role="tab">
                      <h5 class="panel-title">
                        <a href="#bd_TaskConfigItem_{$sr_ConfigTaskType}" aria-controls="bd_TaskConfigItem_{$sr_ConfigTaskType}" aria-expanded="false" data-parent="#gr_TaskConfigList" role="button" data-toggle="collapse">
                          {$sr_ConfigTaskType} ({$ar_ConfigTaskConfig|@count})
                        </a>
                      </h5>
                    </div> <!-- .panel-heading -->
                    <div id="bd_TaskConfigItem_{$sr_ConfigTaskType}" aria-labelledby="hd_TaskConfigItem_{$sr_ConfigTaskType}" class="panel-collapse collapse" role="tabpanel">
                      <div class="panel-body">
            {fn_ConfigArrayLayout ar_ConfigArray=$ar_ConfigTaskConfig}
                      </div>
                    </div> <!-- .panel-collapse -->
                  </div> <!-- .panel -->
          {/if}
        {/foreach}
                </div> <!-- .panel-group -->
              </div> <!-- .tab-pane -->
            </div> <!-- .tab-content -->
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button id="bt_Downloader_ResetGlobalConfig" name="bt_DownloaderControl" value="ResetGlobalConfig" class="btn btn-default" type="reset">
                  <span class="glyphicon glyphicon-refresh"></span>
                  Reset
                </button>                          
                <button id="bt_Downloader_SetGlobalConfig" name="bt_DownloaderControl" value="SetGlobalConfig" class="btn btn-primary" type="submit">
                  <span class="glyphicon glyphicon-floppy-disk"></span>
                  Save
                </button>                          
              </div> <!-- .col -->
            </div><!-- .form-group -->
          </form>
          <div class="panel panel-default">
            <div class="panel-heading text-center">
              <h4 class="panel-title">Downloader information</h4>
            </div>
            <div class="panel-body">
        {fn_OrganiseArrayInDescr ar_InputArray=$ar_Ui.ar_Downloader.ar_Global.ar_ConfigPage.ar_Info sr_InfoType='Downloader' bl_ExcludeNull=true}
            </div> <!-- .panel-body -->
          </div> <!-- .panel -->
    {else}
          <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            No access to configuration. Please see status for more information.
          </div> <!-- .alert -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
