{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  <head>
    <meta charset="{$ar_App.sr_CharSet}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="{$ar_App.sr_Descr}" />
    <meta name="author" content="{$ar_App.sr_Author}" />
    <link rel="icon" type="image/x-icon" href="{$ar_App.ar_Art.ar_Favicon.sr_Uri}" />
  {nocache}
    <title>{$ar_Page.sr_Title} | {$ar_App.sr_Name} | {$ar_Site.sr_Name}</title>
    {foreach $ar_Ui.ar_Uri.ar_Css as $sr_Filename}
    <link rel="stylesheet" type="text/css" href="{$sr_Filename}" />
    {/foreach}
  {/nocache}
  </head>
