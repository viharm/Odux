{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_ConfigArrayLayout}
    {foreach $ar_ConfigArray item=ar_ConfigVal key=vr_ConfigParam}
      {assign var='sr_ValType' value=$vr_ConfigParam|substr:0:2}
      {if $ar_ConfigVal.bl_Readonly}
        {if $sr_ValType == 'bl'}
          {if $ar_ConfigVal.vr_Value === true}
            {$ar_ConfigVal.vr_Value = 'On'}
          {else}
            {$ar_ConfigVal.vr_Value = 'Off'}
          {/if}
        {/if}
        {$sr_ValType='sr'}
        {assign var='sr_Readonly' value=' readonly'}
      {else}
        {assign var='sr_Readonly' value=''}
      {/if}
        <div class="form-group">
          <label class="control-label col-sm-3">{$ar_ConfigVal.sr_Title}</label>
          <div class="col-sm-7">
      {if $sr_ValType == 'bl'}
        {if $ar_ConfigVal.vr_Value == true}
          {assign var='sr_CheckBoxStatus' value='checked'}
        {else}
          {assign var='sr_CheckBoxStatus' value=''}
        {/if}
            <input id="ck_Downloader_{$vr_ConfigParam}" name="ck_Downloader_{$vr_ConfigParam}" {$sr_CheckBoxStatus} onchange="fn_IdentifyChangedField('ck_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')" type="checkbox"{$sr_Readonly} />
      {else}
        {if is_array($ar_ConfigVal.ar_AltValueList) and $ar_ConfigVal.bl_Readonly === false}
            <select id="cb_Downloader_{$vr_ConfigParam}" name="cb_Downloader_{$vr_ConfigParam}" checked onchange="fn_IdentifyChangedField('cb_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')"  class="form-control"{$sr_Readonly}>
          {foreach $ar_ConfigVal.ar_AltValueList item=vr_AltValue}
            {if $vr_AltValue == $ar_ConfigVal}
              {assign var='sr_DefaultOption' value=' selected="true"'}
            {else}
              {assign var='sr_DefaultOption' value=''}
            {/if}
              <option{$sr_DefaultOption}>{$vr_AltValue}</option>
          {/foreach}
            </select>
        {else}
          {if $ar_ConfigVal.bl_Readonly}
            <p class="form-control-static">{$ar_ConfigVal.vr_Value}</p>
          {else}
            <input id="tx_Downloader_{$vr_ConfigParam}" name="tx_Downloader_{$vr_ConfigParam}" value="{$ar_ConfigVal.vr_Value}" placeholder="{$ar_ConfigVal.sr_DefaultVal}" onchange="fn_IdentifyChangedField('tx_Downloader_{$vr_ConfigParam}','ck_Downloader_Changed_{$vr_ConfigParam}')" class="form-control" type="text"{$sr_Readonly} />
          {/if}
        {/if}
      {/if}
            <span id="lb_Help_{$vr_ConfigParam}" class="help-block">{$ar_ConfigVal.sr_Descr|escape}</span>
          </div> <!-- .col -->
          <div class="col-sm-2">
      {if ! $ar_ConfigVal.bl_Readonly}
            <div class="checkbox text-right">
              <label>
                <input id="ck_Downloader_Changed_{$vr_ConfigParam}" name="ck_Downloader_Changed_{$vr_ConfigParam}" type="checkbox" /> Changed
              </label>
            </div> <!-- .checkbox -->
      {/if}
          </div> <!-- .col -->
        </div> <!-- .form-group -->
    {/foreach}
  {/function}
{* Page start *}
      <div id="dv_PageBody" class="container-fluid">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
      {if $ar_Ui.ar_Downloader.ar_Task.bl_Context}
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            <h3 class="panel-title">Task <em>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}</em></h3>
          </div> <!-- .panel-heading -->
          <table class="table text-center cl_CompactNav">
            <tr class="list-group">
              <td>
                <a id="ln_Task_Summary" href="Task_Summary.php?sr_DownloaderInstruction=SummariseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}__Basic&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Summary</span>
                  <span class="glyphicon glyphicon-info-sign"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Source" href="Task_Source.php?sr_DownloaderInstruction=SummariseTaskSource__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Sources</span>
                  <span class="glyphicon glyphicon-cloud"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Conn" href="Task_Conn.php?sr_DownloaderInstruction=SummariseTaskConn__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Connections</span>
                  <span class="glyphicon glyphicon-link"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Peer" href="Task_Peer.php?sr_DownloaderInstruction=SummariseTaskPeer__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Peers</span>
                  <span class="glyphicon glyphicon-user"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_File" href="Task_File.php?sr_DownloaderInstruction=SummariseTaskFile__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Files</span>
                  <span class="glyphicon glyphicon-file"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Manage" href="Task_Manage.php?sr_DownloaderInstruction=SummariseTaskConfig__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item active">
                  <span class="hidden-xs">Manage</span>
                  <span class="glyphicon glyphicon-cog"></span>
                </a>
              </td>
            </tr> 
          </table>
        </div> <!-- .panel -->
        <form id="fm_Downloader_TaskConfig" name="fm_Downloader_TaskConfig" method="post" action="Task_Manage.php" class="form-horizontal" role="form">
          <input name="sr_TaskRef" value="{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" type="hidden" />
          <input name="sr_DownloaderInstruction" value="SummariseTaskConfig__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" type="hidden" />
          <div id="gr_TaskConfigList" class="panel-group" role="tablist" aria-multiselectable="true">
        {foreach $ar_Ui.ar_Downloader.ar_Task.ar_Config.ar_Task item=ar_TaskConfigTaskConfig key=sr_TaskConfigTaskType}
          {if $ar_TaskConfigTaskConfig|@count > 0 }
            <div class="panel panel-default">
              <div id="hd_TaskConfigItem_{$sr_TaskConfigTaskType}" class="panel-heading" role="tab">
                <h5 class="panel-title">
                  <a href="#bd_TaskConfigItem_{$sr_TaskConfigTaskType}" aria-controls="bd_TaskConfigItem_{$sr_TaskConfigTaskType}" aria-expanded="false" data-parent="#gr_TaskConfigList" role="button" data-toggle="collapse">
                    {$sr_TaskConfigTaskType} ({$ar_TaskConfigTaskConfig|@count})
                  </a>
                </h5>
              </div> <!-- .panel-heading -->
              <div id="bd_TaskConfigItem_{$sr_TaskConfigTaskType}" aria-labelledby="hd_TaskConfigItem_{$sr_TaskConfigTaskType}" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
            {fn_ConfigArrayLayout ar_ConfigArray=$ar_TaskConfigTaskConfig}
                </div>
              </div> <!-- .panel-collapse -->
            </div> <!-- .panel -->
          {/if}
        {/foreach}
          </div> <!-- .panel-group -->
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button id="bt_Downloader_ResetTaskConfig" name="bt_DownloaderControl" value="ResetTaskConfig" class="btn btn-default" type="reset">
                <span class="glyphicon glyphicon-refresh"></span>
                Reset
              </button>                          
              <button id="bt_Downloader_SetTaskConfig" name="bt_DownloaderControl" value="SetTaskConfig" class="btn btn-primary" type="submit">
                <span class="glyphicon glyphicon-floppy-disk"></span>
                Save
              </button>                          
            </div> <!-- .col -->
          </div><!-- .form-group -->
        </form>
      {else}
        <div class="alert alert-warning" role="alert">
          <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Task context not available
        </div> <!-- .alert -->
      {/if}
    {else}
          <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            No connection to downloader
          </div> <!-- .alert -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
