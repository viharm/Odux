{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_AddTaskArrayLayout}
    {foreach $ar_ConfigArray item=ar_ConfigVal key=vr_ConfigParam}
      {if ! $ar_ConfigVal.bl_Readonly}
        {assign var='sr_ValType' value=$vr_ConfigParam|substr:0:2}
          <div class="form-group">
            <label class="control-label col-sm-3">{$ar_ConfigVal.sr_Title}</label>
            <div class="col-sm-7">
        {if $sr_ValType == 'bl'}
          {if $ar_ConfigVal.vr_Value == true}
            {assign var='sr_CheckBoxStatus' value='checked'}
          {else}
            {assign var='sr_CheckBoxStatus' value=''}
          {/if}
              <input id="ck_Downloader_{$vr_ConfigParam}" name="ck_Downloader_{$vr_ConfigParam}" {$sr_CheckBoxStatus} onchange="fn_IdentifyChangedField('ck_Downloader_{$vr_ConfigParam}','ck_Changed_Downloader_{$vr_ConfigParam}')" type="checkbox" />
        {else}
          {if is_array($ar_ConfigVal.ar_AltValueList)}
              <select id="cb_Downloader_{$vr_ConfigParam}" name="cb_Downloader_{$vr_ConfigParam}" checked onchange="fn_IdentifyChangedField('cb_Downloader_{$vr_ConfigParam}','ck_Changed_Downloader_{$vr_ConfigParam}')"  class="form-control">
            {foreach $ar_ConfigVal.ar_AltValueList item=vr_AltValue}
              {if $vr_AltValue == $ar_ConfigVal}
                {assign var='sr_DefaultOption' value=' selected="true"'}
              {else}
                {assign var='sr_DefaultOption' value=''}
              {/if}
                <option{$sr_DefaultOption}>{$vr_AltValue}</option>
            {/foreach}
              </select>
          {else}
              <input id="tx_Downloader_{$vr_ConfigParam}" name="tx_Downloader_{$vr_ConfigParam}" value="{$ar_ConfigVal.vr_Value}" placeholder="{$ar_ConfigVal.sr_DefaultVal}" onchange="fn_IdentifyChangedField('tx_Downloader_{$vr_ConfigParam}','ck_Changed_Downloader_{$vr_ConfigParam}')" class="form-control" type="text" />
          {/if}
        {/if}
              <span id="lb_Help_{$vr_ConfigParam}" class="help-block">{$ar_ConfigVal.sr_Descr|escape}</span>
            </div> <!-- .col -->
            <div class="col-sm-2">
              <div class="checkbox text-right">
                <label>
                  <input id="ck_Changed_Downloader_{$vr_ConfigParam}" name="ck_Downloader_Changed_{$vr_ConfigParam}" type="checkbox" /> Changed
                </label>
              </div> <!-- .checkbox -->
            </div> <!-- .col -->
          </div> <!-- .form-group -->
      {/if}
    {/foreach}
  {/function}
{* Start page *}
      <div id="dv_PageBody" class="container-fluid">
        <form id="fm_TaskAdd" action="Add.php" class="form-horizontal" method="post" role="form">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
          <div class="panel panel-default">
            <div id="hd_TaskAdd" class="panel-heading text-center">
              <h3 class="panel-title">{$ar_Page.sr_Title}</h3>
            </div> <!-- .panel-heading -->
            <div class="panel-body">
              <div class="form-group">
                <label class="control-label col-sm-3" for="txt__UrlInput">URL</label>
                <div class="col-sm-9">
                  <textarea id="tx_Source" class="form-control" name="tx_Source"></textarea>
                  <span id="lb_Help_tx_Source" class="help-block">URL(s) of download, separated by commas, semi-colons, spaces, pipes or line breaks</span>
                </div> <!-- .col -->
              </div> <!-- .form-group -->
            </div> <!-- .panel-body -->
          </div> <!-- .panel -->
          <h4>Options</h4>
          <div id="gr_TaskAddList" class="panel-group" role="tablist" aria-multiselectable="true">
    {foreach $ar_Ui.ar_Downloader.ar_Task.ar_AddPage.ar_Task item=ar_AddTaskConfig key=sr_AddTaskType}
      {if $ar_AddTaskConfig|@count > 0 }
            <div class="panel panel-default">
              <div id="hd_TaskConfigItem_{$sr_AddTaskType}" class="panel-heading" role="tab">
                <h5 class="panel-title">
                  <a href="#bd_TaskConfigItem_{$sr_AddTaskType}" aria-controls="bd_TaskConfigItem_{$sr_AddTaskType}" aria-expanded="false" data-parent="#gr_TaskAddList" role="button" data-toggle="collapse">
                    {$sr_AddTaskType}
                  </a>
                </h5>
              </div> <!-- .panel-heading -->
              <div id="bd_TaskConfigItem_{$sr_AddTaskType}" aria-labelledby="hd_TaskConfigItem_{$sr_AddTaskType}" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
        {fn_AddTaskArrayLayout ar_ConfigArray=$ar_AddTaskConfig}
                </div> <!-- .panel-body -->
              </div> <!-- .panel-collapse -->
            </div> <!-- .panel -->
      {/if}
    {/foreach}
          </div> <!-- .panel-group -->
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button id="bt_Downloader_ResetGlobalConfig" name="bt_DownloaderControl" value="ResetTaskAddConfig" class="btn btn-default" type="reset">
                <span class="glyphicon glyphicon-refresh"></span>
                Reset
              </button>                          
              <button id="bt_Downloader_SetGlobalConfig" name="bt_DownloaderControl" value="AddTask" class="btn btn-primary" type="submit">
                <span class="glyphicon glyphicon-plus"></span>
                Add
              </button>                          
            </div> <!-- .col -->
          </div><!-- .form-group -->
    {else}
          <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            New task cannot be added. Please see status for more information.
        </div> <!-- .row -->
    {/if}
  {/nocache}
        </form>
      </div> <!-- #dv_PageBody -->