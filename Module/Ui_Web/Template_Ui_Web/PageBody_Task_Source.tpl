{* Smarty *}
  {*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  *}
  {function name=fn_ListTaskSource}
    {if $ar_TaskSource.sr_TaskSourceStatus.vr_Value === 'Active'}
          <div class="panel panel-primary">
    {else}
          <div class="panel panel-info">
    {/if}
            <div id="hd_TaskSource_{$nm_TaskSourceKey}" class="panel-heading cl_Task" role="tab">
              <h5 class="panel-title">
                <a href="#bd_TaskSource_{$nm_TaskSourceKey}" class="collapsed" aria-controls="bd_TaskSource_{$nm_TaskSourceKey}" data-parent="#gr_TaskSourceList" data-toggle="collapse" role="button">
                  {$ar_TaskSource.sr_TaskSourceName.vr_Value}
                  <span class="pull-right"># {$nm_TaskSourceKey}</span>
                </a>
              </h5>
            </div> <!-- .panel-heading -->
            <div id="bd_TaskSource_{$nm_TaskSourceKey}" aria-labelledby="hd_TaskSource_{$nm_TaskSourceKey}" class="panel-collapse collapse" role="tabpanel">
              <ul class="list-group">
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-bookmark"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        Status
                        <span class="badge pull-right">
    {if $ar_TaskSource.sr_TaskSourceStatus.vr_Value === 'Active'}
                          <span class="glyphicon glyphicon-transfer"></span>
    {else}
                          <span class="glyphicon glyphicon-hourglass"></span>
    {/if}
                        </span>
                      </span>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
                <li class="list-group-item">
                  <div class="media">
                    <div class="media-left">
                      <span class="glyphicon glyphicon-paperclip"></span>
                    </div> <!-- .media-left -->
                    <div class="media-body">
                      <span class="media-heading">
                        URL
                      </span>
                      <p><code>{$ar_TaskSource.sr_TaskSourceUrl.vr_Value}</code></p>
                    </div> <!-- .media-body -->
                  </div> <!-- .media -->
                </li>
              </ul>
            </div> <!-- panel-collapse -->
          </div> <!-- .panel-[STATUS] -->
  {/function}
{* Page start *}
      <div id="dv_PageBody" class="container-fluid">
  {nocache}
    {if $ar_Ui.ar_Downloader.ar_Global.bl_ConnStatus}
      {if $ar_Ui.ar_Downloader.ar_Task.bl_Context}
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            <h3 class="panel-title">Task <em>{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}</em></h3>
          </div> <!-- .panel-heading -->
          <table class="table text-center cl_CompactNav">
            <tr class="list-group">
              <td>
                <a id="ln_Task_Summary" href="Task_Summary.php?sr_DownloaderInstruction=SummariseTask__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}__Basic&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Summary</span>
                  <span class="glyphicon glyphicon-info-sign"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Source" href="Task_Source.php?sr_DownloaderInstruction=SummariseTaskSource__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item active">
                  <span class="hidden-xs">Sources</span>
                  <span class="glyphicon glyphicon-cloud"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Conn" href="Task_Conn.php?sr_DownloaderInstruction=SummariseTaskConn__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Connections</span>
                  <span class="glyphicon glyphicon-link"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Peer" href="Task_Peer.php?sr_DownloaderInstruction=SummariseTaskPeer__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Peers</span>
                  <span class="glyphicon glyphicon-user"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_File" href="Task_File.php?sr_DownloaderInstruction=SummariseTaskFile__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Files</span>
                  <span class="glyphicon glyphicon-file"></span>
                </a>
              </td>
              <td>
                <a id="ln_Task_Manage" href="Task_Manage.php?sr_DownloaderInstruction=SummariseTaskConfig__{$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}&sr_TaskRef={$ar_Ui.ar_Downloader.ar_Task.ar_Summary.sr_TaskRef.vr_Value}" class="list-group-item">
                  <span class="hidden-xs">Manage</span>
                  <span class="glyphicon glyphicon-cog"></span>
                </a>
              </td>
            </tr>
          </table>
        </div> <!-- .panel -->
        {if $ar_Ui.ar_Downloader.ar_Task.ar_Source|@count > 0}
        <div class="panel-group" id="gr_TaskSourceList" role="tablist" aria-multiselectable="true">
          {foreach from=$ar_Ui.ar_Downloader.ar_Task.ar_Source item=ar_TaskSource key=nm_TaskSourceKey}
            {fn_ListTaskSource ar_TaskSource=$ar_TaskSource nm_TaskSourceKey=$nm_TaskSourceKey+1}
          {/foreach}
        </div> <!-- .panel-group -->
        {else}
        <div class="alert alert-info" role="alert">
          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          No sources
        </div> <!-- .alert -->
        {/if}
      {else}
        <div class="alert alert-warning" role="alert">
          <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Task context not available
        </div> <!-- .alert -->
      {/if}
    {else}
        <div class="alert alert-info" role="alert">
          <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          No access to configuration
        </div> <!-- .alert -->
    {/if}
  {/nocache}
      </div> <!-- #dv_PageBody -->
