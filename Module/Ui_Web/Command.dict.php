<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * This file consists of variable declaration for an associative array of strings
  * Each key name corresponds to Odux UI commands
  * Each corresponding value contains the actual commands for the UI/renderer module
  */
  
  $ar_Ui_Command = array (
    'TransferVar'    => array (
      'sr_ModuleCmd' => 'assign'
    ) ,
    'TransferVarAdd' => array (
      'sr_ModuleCmd' => 'append'
    ) ,
    'Render'        => array (
      'sr_ModuleCmd' => 'display'
    ) ,
    'TestSetup'        => array (
      'sr_ModuleCmd' => 'testInstall'
    ) ,
    'ClearCacheAll'    => array (
      'sr_ModuleCmd' => 'clearAllCache'
    ) ,
    'ClearCompiled'    => array (
      'sr_ModuleCmd' => 'clearCompiledTemplate'
    ) ,
    'SecureDisable'        => array (
      'sr_ModuleCmd' => 'disableSecurity'
    ) ,
    'SecureEnable'        => array (
      'sr_ModuleCmd' => 'enableSecurity'
    ) ,
    'CacheDir'    => array (
      'sr_ModuleCmd' => 'getCacheDir'
    ) ,
    'CompiledDir'    => array (
      'sr_ModuleCmd' => 'getCompileDir'
    ) ,
    'ConfigDir'    => array (
      'sr_ModuleCmd' => 'getConfigDir'
    ) ,
    'TemplateDir'    => array (
      'sr_ModuleCmd' => 'getTemplateDir'
    ) ,
    'PluginDir'    => array (
      'sr_ModuleCmd' => 'getPluginsDir'
    ) ,
    'ListTemplateVar'    => array (
      'sr_ModuleCmd' => 'getTemplateVars'
    ) ,
    'ListConfigVar'    => array (
      'sr_ModuleCmd' => 'getConfigVars'
    ) ,
    'SummariseRegObj'    => array (
      'sr_ModuleCmd' => 'getRegisteredObject'
    ) ,
    'ListTag'    => array (
      'sr_ModuleCmd' => 'getTags'
    )
  ) ;
?>