<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    '*.default.config.php'
  ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // Include customr configurations; must be after the default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    '*.custom.config.php'
  ) as $sr_Filename ) {
    include_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // THE FOLLOWING LOGIC TO SPECIFY SMARTY PATH HAS BEEN DELEGATED TO CONFIG FILES INCLUDED ABOVE. DELETE AFTER TESTING
  // Path to Smarty (include trailing slash or backslash)
//$sr_SmartyPath = '/var/www/Framework/Smarty/Current/' ;
  
  // Check if path exists
  if (file_exists($sr_SmartyPath)) {
    
    // Set path to class file
    $sr_SmartyClassPath =  realpath (
      $sr_SmartyPath .
      'libs' .
      DIRECTORY_SEPARATOR .
      'Smarty.class.php'
    ) ;
    
    // Check if class file exists
    if (file_exists($sr_SmartyClassPath)) {

      // load Smarty library
      require_once ( $sr_SmartyClassPath ) ;

      // Check if class exists
      if (class_exists('Smarty')) {

        // Define custom class
        class cl_Renderer extends Smarty {
          function __construct ( ) {

            // Class Constructor.
            // These automatically get set with each new instance.
            parent::__construct() ;
            // Set template directory
            $this->setTemplateDir (
              __DIR__ .
              DIRECTORY_SEPARATOR .
              'Template_Ui_Web' .
              DIRECTORY_SEPARATOR
            ) ;
            // Set compile directory, this should be writeable by the web server
            $this->setCompileDir (
              __DIR__ .
              DIRECTORY_SEPARATOR .
              'Compiled_Ui_Web' .
              DIRECTORY_SEPARATOR
            ) ;
            // Set configuration directory
            $this->setConfigDir (
              __DIR__ .
              DIRECTORY_SEPARATOR .
              'Config_Ui_Web' .
              DIRECTORY_SEPARATOR
            ) ;
            // Set cache directory, this should be writeable by the web server
            $this->setCacheDir (
              __DIR__ .
              DIRECTORY_SEPARATOR .
              'Cache_Ui_Web' .
              DIRECTORY_SEPARATOR
            ) ;
            $this->caching = Smarty::CACHING_LIFETIME_CURRENT ;
      /*
            // Specify local plugins/modules directory
            $smarty->plugins_dir[] = 'Module_Ui_Web' ;
      */
          } // __construct()
        } // class
      } // Class exists
      else {
        // Abort
        fn_Abort ( 'Could not find Smarty class file. Please ensure the Smarty library exists in ' . $sr_SmartyPath ) ;
      } // Class does not exist
    } // Class file exists
    else {
      // Abort
      fn_Abort ( 'Could not find Smarty class file. Please ensure the Smarty library exists in ' . $sr_SmartyPath ) ;
    } // class file does not exist
  } // Smarty path exists
  else {
    // Abort
    fn_Abort ( 'Could not find Smarty library. Please check $sr_SmartyPath in ' . __DIR__ . DIRECTORY_SEPARATOR . 'Ui.custom.config.php' ) ;
  } // Smarty path does not exist

  // Define an abort function
  function fn_Abort($sr_Mesg) {
    die ( $sr_Mesg ) ;
  }
?>