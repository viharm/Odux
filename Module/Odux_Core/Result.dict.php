<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * This file consists of variable declaration for an associative array of strings
  * Each key name corresponds to  module output keys
  * Each corresponding value contains the Odux normalised output.
  */

  $ar_Odux_Result = array (
    // Client info
    'sr_Geo_Ip'   => array (
      'sr_Title'    => 'IP address (Geo)' ,
    ) ,
    'sr_Geo_ContinentCode'     => array (
      'sr_Title'    => 'Continent code' ,
    ) ,
    'sr_Geo_CountryCode'       => array (
      'sr_Title'    => 'Country code' ,
    ) ,
    'sr_Geo_CountryName'      => array (
      'sr_Title'    => 'Country name' ,
    ) ,
    'sr_User_Auth'      => array (
      'sr_Title'    => 'Authenticated user' ,
    ) ,
    'sr_Remote_User' => array (
      'sr_Title'    => 'User' ,
    ) ,
    'sr_Remote_Ip'   => array (
      'sr_Title'    => 'IP address' ,
    ) ,
    'sr_UserAgent'     => array (
      'sr_Title'    => 'Browser' ,
    )
    
  ) ;
?>