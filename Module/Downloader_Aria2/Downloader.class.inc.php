<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    '*.default.config.php'
  ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // Include customr configurations; must be after the default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    '*.custom.config.php'
  ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // Include provider library
  require_once ( realpath (
    __DIR__ . DIRECTORY_SEPARATOR .
    'phpAria2rpc' . DIRECTORY_SEPARATOR .
    'phpAria2rpc.class.inc.php'
  ) ) ;

  // Define class
  class cl_Downloader {
    
    // Define class variables
    private $ob_Fetcher ;
    private $ar_CommandDict ;
    
    // Create a phpAria2rpc object as soon as an object is created from this class
    public function __construct ( $ar_ServerConfig ) {
      require_once ( realpath ( __DIR__ . DIRECTORY_SEPARATOR . 'Command.dict.php' ) ) ;
      @fn_Debug ( 'Command dictionary file included; now transferring dictionary' , $ar_Downloader_Command ) ;
      $this->ar_CommandDict = $ar_Downloader_Command ;
      unset($ar_Downloader_Command) ;
      @fn_Debug ( 'Checking if class exists' , 'phpAria2rpc' ) ;
      if ( class_exists('phpAria2rpc') ) {
        @fn_Debug ( 'Found class' ) ;
        $this->ob_Fetcher = new phpAria2rpc($ar_ServerConfig) ;
        @fn_Debug ( 'Created object from class, transferring status from child object to self' , $this->ob_Fetcher ) ;
        $this->bl_ConnStatus = $this->ob_Fetcher->bl_ConnStatus ;
        @fn_Debug ( 'Connection status transferred  from child object to self' , $this->bl_ConnStatus ) ;
      }
    }
    
    // Define the abstraction layer to transfer functions from the controller to the renderer
    function __call ( $sr_CommandName , $vr_MethodArg ) {
      @fn_Debug ( 'Command' , $sr_CommandName ) ;
      @fn_Debug ( 'Arguments provided' , $vr_MethodArg ) ;
      @fn_Debug ( 'Translated method' , $this->ar_CommandDict[$sr_CommandName]['sr_ModuleCmd'] ) ;

      @fn_Debug ( 'Checking if any of the supplied arguments exists as an alternative parameter' , $this->ar_CommandDict[$sr_CommandName] ) ;
      if ( array_key_exists ( 'ar_AltArgList' , $this->ar_CommandDict[$sr_CommandName] ) ) {
        @fn_Debug ( 'alternative arguments list exists' ) ;
        @fn_Debug ( 'Setting a loop counter for identifying working array index' ) ;
        $nm_LoopCounter_01 = 0 ;
        foreach ( $vr_MethodArg as $vr_IndivArg ) {
          @fn_Debug ( 'Checking argument' , $vr_IndivArg ) ;
          if ( array_key_exists ( $vr_IndivArg , $this->ar_CommandDict[$sr_CommandName]['ar_AltArgList'] ) ) {
            @fn_Debug ( 'Alternative argument found in list; checking if array' , $this->ar_CommandDict[$sr_CommandName]['ar_AltArgList'][$vr_IndivArg] ) ;
            if ( is_array($this->ar_CommandDict[$sr_CommandName]['ar_AltArgList'][$vr_IndivArg]) ) {
              @fn_Debug ( 'Argument is an array; Replacing Odux argument with multiple downloader arguments' ) ;
              array_splice ( $vr_MethodArg , $nm_LoopCounter_01 , 1 , $this->ar_CommandDict[$sr_CommandName]['ar_AltArgList'][$vr_IndivArg] ) ;
            } // argument expands to an array
          } // argument exists
          @fn_Debug ( 'Incrementing loopcounter from...' , $nm_LoopCounter_01 ) ;
          $nm_LoopCounter_01 ++ ;
        } // forach supplied method argument
        unset($nm_LoopCounter_01) ;
      } // alternative arguments list exists
      
      @fn_Debug ( 'Arguments to be used' , $vr_MethodArg ) ;

      $ar_FetcherResult = call_user_func_array (
        array (
          $this->ob_Fetcher ,
          $this->ar_CommandDict[$sr_CommandName]['sr_ModuleCmd']
        ) ,
        $vr_MethodArg
      ) ;
      @fn_Debug ( 'Checking type of return value' , $ar_FetcherResult ) ;
      if ( array_key_exists ( 'result' , $ar_FetcherResult ) ) {
        return $ar_FetcherResult['result'] ;
      } // Contains 'result' struct
      elseif ( array_key_exists ( 'error' , $ar_FetcherResult ) ) {
        return 'Error ' . strval($ar_FetcherResult['error']['code']) . ' : ' . $ar_FetcherResult['error']['message'] ;
      }
      else {
        return $ar_FetcherResult ;
      } // contains direct output
    }
  }
?>