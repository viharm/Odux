<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  $ar_Downloader_ErrorCode = array (
    00 => array (
      'sr_Descr' => 'Download successful'
    ) ,
    01 => array (
      'sr_Descr' => 'Unknown error'
    ) ,
    02 => array (
      'sr_Descr' => 'Time out occurred'
    ) ,
    03 => array (
      'sr_Descr' => 'Resource was not found'
    ) ,
    04 => array (
      'sr_Descr' => 'Exceeded "resource not found" error limit'
    ) ,
    05 => array (
      'sr_Descr' => 'Download aborted, too slow'
    ) ,
    06 => array (
      'sr_Descr' => 'Network problem'
    ) ,
    07 => array (
      'sr_Descr' => 'Unfinished downloads'
    ) ,
    08 => array (
      'sr_Descr' => 'Resume not supported by remote server'
    ) ,
    09 => array (
      'sr_Descr' => 'Not enough disk space'
    ) ,
    10 => array (
      'sr_Descr' => 'Piece length different from in control file'
    ) ,
    11 => array (
      'sr_Descr' => 'Downloading same file'
    ) ,
    12 => array (
      'sr_Descr' => 'Downloading same info hash torrent'
    ) ,
    13 => array (
      'sr_Descr' => 'File exists'
    ) ,
    14 => array (
      'sr_Descr' => 'Renaming file failed'
    ) ,
    15 => array (
      'sr_Descr' => 'Could not open existing file'
    ) ,
    16 => array (
      'sr_Descr' => 'Could not create new or truncate existing file'
    ) ,
    17 => array (
      'sr_Descr' => 'File I/O error'
    ) ,
    18 => array (
      'sr_Descr' => 'Could not create directory'
    ) ,
    19 => array (
      'sr_Descr' => 'Name resolution failed'
    ) ,
    20 => array (
      'sr_Descr' => 'Could not parse metalink'
    ) ,
    21 => array (
      'sr_Descr' => 'FTP command failed'
    ) ,
    22 => array (
      'sr_Descr' => 'Unexpected HTTP response header'
    ) ,
    23 => array (
      'sr_Descr' => 'Too many redirects'
    ) ,
    24 => array (
      'sr_Descr' => 'HTTP authorization failed'
    ) ,
    25 => array (
      'sr_Descr' => 'Could not parse bencoded file'
    ) ,
    26 => array (
      'sr_Descr' => 'Torrent file corrupt or missing information'
    ) ,
    27 => array (
      'sr_Descr' => 'Bad Magnet URI'
    ) ,
    28 => array (
      'sr_Descr' => 'Unexpected option'
    ) ,
    29 => array (
      'sr_Descr' => 'Remote server unable to handle request'
    ) ,
    30 => array (
      'sr_Descr' => 'Could not parse JSON-RPC request'
    )
/*
    'Download successful'                            => 00 ,
    'Unknown error'                                  => 01 ,
    'Time out occurred'                              => 02 ,
    'Resource was not found'                         => 03 ,
    'Exceeded "resource not found" error limit'      => 04 ,
    'Download aborted, too slow'                     => 05 ,
    'Network problem'                                => 06 ,
    'Unfinished downloads'                           => 07 ,
    'Resume not supported by remote server'          => 08 ,
    'Not enough disk space'                          => 09 ,
    'Piece length different from in control file'    => 10 ,
    'Downloading same file'                          => 11 ,
    'Downloading same info hash torrent'             => 12 ,
    'File exists'                                    => 13 ,
    'Renaming file failed'                           => 14 ,
    'Could not open existing file'                   => 15 ,
    'Could not create new or truncate existing file' => 16 ,
    'File I/O error'                                 => 17 ,
    'Could not create directory'                     => 18 ,
    'Name resolution failed'                         => 19 ,
    'Could not parse metalink'                       => 20 ,
    'FTP command failed'                             => 21 ,
    'Unexpected HTTP response header'                => 22 ,
    'Too many redirects'                             => 23 ,
    'HTTP authorization failed'                      => 24 ,
    'Could not parse bencoded file'                  => 25 ,
    'Torrent file corrupt or missing information'    => 26 ,
    'Bad Magnet URI'                                 => 27 ,
    'Unexpected option'                              => 28 ,
    'Remote server unable to handle request'         => 29 ,
    'Could not parse JSON-RPC request'               => 30
*/
  )
?>