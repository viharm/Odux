﻿<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* Define a variable to store fields for peers */
  $ar_Downloader_TaskPeer = array (
    'peerId' => array (
      'sr_OduxVarName'    => 'sr_TaskPeerRef' ,
      'sr_Title'          => 'Peer reference' ,
      'sr_Descr'          => 'Percent-encoded peer ID' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'ip' => array (
      'sr_OduxVarName'    => 'sr_TaskPeerIp' ,
      'sr_Title'          => 'Peer IP address' ,
      'sr_Descr'          => 'IP address of the peer' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'port' => array (
      'sr_OduxVarName'    => 'nm_TaskPeerPort' ,
      'sr_Title'          => 'Peer port' ,
      'sr_Descr'          => 'Port number of the peer' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'bitfield' => array (
      'sr_OduxVarName'    => 'sr_TaskPeerProgressPiece' ,
      'sr_Title'          => 'Peer piece progress' ,
      'sr_Descr'          => 'Hexadecimal representation of the download progress of the peer' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'amChoking' => array (
      'sr_OduxVarName'    => 'bl_PeerChokedRemote' ,
      'sr_Title'          => 'Peer choked' ,
      'sr_Descr'          => 'Downloader choking peer' ,
      'ar_AltValueList'   => array (
        'true'  => TRUE ,
        'false' => FALSE
      ) ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'peerChoking' => array (
      'sr_OduxVarName'    => 'bl_PeerChokedLocal' ,
      'sr_Title'          => 'Downloader choked' ,
      'sr_Descr'          => 'Peer choking Downloader' ,
      'ar_AltValueList'   => array (
        'true'  => TRUE ,
        'false' => FALSE
      ) ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'downloadSpeed' => array (
      'sr_OduxVarName'    => 'nm_TaskPeerSpeedDown' ,
      'sr_Title'          => 'Peer download speed' ,
      'sr_Descr'          => 'Download speed that client obtains from the peer' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'uploadSpeed' => array (
      'sr_OduxVarName'    => 'nm_TaskPeerSpeedUp' ,
      'sr_Title'          => 'Peer upload speed' ,
      'sr_Descr'          => 'Upload speed that client uploads to the peer' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
    'seeder' => array (
      'sr_OduxVarName'    => 'bl_TaskPeerSeederFlag' ,
      'sr_Title'          => 'Peer is seeder' ,
      'sr_Descr'          => 'Flag if peer is a seeder' ,
      'ar_AltValueList'   => array (
        'true'  => TRUE ,
        'false' => FALSE
      ) ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 0 ,
        'nm_Task_Ftp'      => 0 ,
        'nm_Task_Sftp'     => 0 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 0
      )
    ) ,
  ) ;
?>