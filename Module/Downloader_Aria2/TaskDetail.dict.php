<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* Define a variable to store fields for tasks */
  $ar_Downloader_TaskDetail = array (
    'bitfield' => array (
      'sr_OduxVarName'    => 'sr_TaskProgressBit' ,
      'sr_Title'          => 'Piece progress' ,
      'sr_Descr'          => 'Hexadecimal representation of the download progress' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'completedLength' => array (
      'sr_OduxVarName'    => 'nm_TaskSizeCompleted' ,
      'sr_Title'          => 'Downloaded size' ,
      'sr_Descr'          => 'Completed size of the task in bytes' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'connections' => array (
      'sr_OduxVarName'    => 'nm_TaskConnCount' ,
      'sr_Title'          => 'Connection count' ,
      'sr_Descr'          => 'The number of peers/servers connected' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'dir' => array (
      'sr_OduxVarName'    => 'sr_TaskSavePath' ,
      'sr_Title'          => 'Download directory',
      'sr_Descr'          => 'The directory to store the downloaded file(s)' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'downloadSpeed' => array (
      'sr_OduxVarName'    => 'nm_TaskSpeedDown' ,
      'sr_Title'          => 'Download speed' ,
      'sr_Descr'          => 'Download speed of this task measured in bytes/sec' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'gid' => array (
      'sr_OduxVarName'    => 'sr_TaskRef' ,
      'sr_Title'          => 'Ref' ,
      'sr_Descr'          => 'Unique task reference' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'numPieces' => array (
      'sr_OduxVarName'    => 'nm_TaskPieceCount' ,
      'sr_Title'          => 'Piece count for task' ,
      'sr_Descr'          => 'Number of chunks' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'pieceLength' => array (
      'sr_OduxVarName'    => 'nm_TaskSizePiece' ,
      'sr_Title'          => 'Piece size' ,
      'sr_Descr'          => 'Size of individual piece in bytes' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'status' => array (
      'sr_OduxVarName'    => 'sr_TaskStatus' ,
      'sr_Title'          => 'Task status' ,
      'sr_Descr'          => 'Status of task' ,
      'ar_AltValueList'   => array (
        'active'   => 'Active' ,
        'waiting'  => 'Queued' ,
        'paused'   => 'Paused' ,
        'error'    => 'Error' ,
        'complete' => 'Complete' ,
        'removed'  => 'Removed'
      ) ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'totalLength' => array (
      'sr_OduxVarName'    => 'nm_TaskSizeTotal' ,
      'sr_Title'          => 'Task size' ,
      'sr_Descr'          => 'Total size of task in bytes' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'uploadLength' => array (
      'sr_OduxVarName'    => 'nm_TaskSizeUp' ,
      'sr_Title'          => 'Uploaded size' ,
      'sr_Descr'          => 'Uploaded size in bytes' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'uploadSpeed' => array (
      'sr_OduxVarName'    => 'nm_TaskSpeedUp' ,
      'sr_Title'          => 'Upload speed' ,
      'sr_Descr'          => 'Upload speed of this task measured in bytes/sec' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'infoHash' => array (
      'sr_OduxVarName'    => 'sr_TaskBtInfohash' ,
      'sr_Title'          => 'Info hash' ,
      'sr_Descr'          => 'Unique task reference (BitTorrent)' ,
      'nm_Scope_Http'     => 0 ,
      'nm_Scope_Ftp'      => 0 ,
      'nm_Scope_Sftp'     => 0 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 0
    ) ,
    'numSeeders' => array (
      'sr_OduxVarName'    => 'nm_TaskBtSeederCount' ,
      'sr_Title'          => 'Seeder count' ,
      'sr_Descr'          => 'The number of seeders (BitTorrent)' ,
      'nm_Scope_Http'     => 0 ,
      'nm_Scope_Ftp'      => 0 ,
      'nm_Scope_Sftp'     => 0 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 0
    ) ,
    'seeder' => array (
      'sr_OduxVarName'    => 'bl_TaskBtSeedingStatus' ,
      'sr_Title'          => 'Seeding status' ,
      'sr_Descr'          => 'Status whether this task is seeding' ,
      'nm_Scope_Http'     => 0 ,
      'nm_Scope_Ftp'      => 0 ,
      'nm_Scope_Sftp'     => 0 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 0
    ) ,
    'errorCode' => array (
      'sr_OduxVarName'    => 'nm_TaskErrCode' ,
      'sr_Title'          => 'Error code' ,
      'sr_Descr'          => 'The code of the last error for this task if stopped/completed downloads' ,
      'ar_AltValueList'   => array (
        '00' => 'Download successful'                            ,
        '01' => 'Unknown error'                                  ,
        '02' => 'Time out occurred'                              ,
        '03' => 'Resource was not found'                         ,
        '04' => 'Exceeded "resource not found" error limit'      ,
        '05' => 'Download aborted, too slow'                     ,
        '06' => 'Network problem'                                ,
        '07' => 'Unfinished downloads'                           ,
        '08' => 'Resume not supported by remote server'          ,
        '09' => 'Not enough disk space'                          ,
        '10' => 'Piece length different from in control file'    ,
        '11' => 'Downloading same file'                          ,
        '12' => 'Downloading same info hash torrent'             ,
        '13' => 'File exists'                                    ,
        '14' => 'Renaming file failed'                           ,
        '15' => 'Could not open existing file'                   ,
        '16' => 'Could not create new or truncate existing file' ,
        '17' => 'File I/O error'                                 ,
        '18' => 'Could not create directory'                     ,
        '19' => 'Name resolution failed'                         ,
        '20' => 'Could not parse metalink'                       ,
        '21' => 'FTP command failed'                             ,
        '22' => 'Unexpected HTTP response header'                ,
        '23' => 'Too many redirects'                             ,
        '24' => 'HTTP authorization failed'                      ,
        '25' => 'Could not parse bencoded file'                  ,
        '26' => 'Torrent file corrupt or missing information'    ,
        '27' => 'Bad Magnet URI'                                 ,
        '28' => 'Unexpected option'                              ,
        '29' => 'Remote server unable to handle request'         ,
        '30' => 'Could not parse JSON-RPC request'
      ) ,
      'nm_Scope_Http'     => 30 ,
      'nm_Scope_Ftp'      => 30 ,
      'nm_Scope_Sftp'     => 30 ,
      'nm_Scope_Bt'       => 30 ,
      'nm_Scope_Metalink' => 30
    ) ,
    'errorMessage' => array (
      'sr_OduxVarName'    => 'sr_TaskErrMesg' ,
      'sr_Title'          => 'Error message' ,
      'sr_Descr'          => 'Human readable error message for task associated to numeric error code' ,
      'nm_Scope_Http'     => 30 ,
      'nm_Scope_Ftp'      => 30 ,
      'nm_Scope_Sftp'     => 30 ,
      'nm_Scope_Bt'       => 30 ,
      'nm_Scope_Metalink' => 30
    ) ,
    'followedBy' => array (
      'sr_OduxVarName'    => 'vr_TaskFollowerList' ,
      'sr_Title'          => 'List of follower tasks' ,
      'sr_Descr'          => 'List of references which are generated as the result of this download' ,
      'nm_Scope_Http'     => 0 ,
      'nm_Scope_Ftp'      => 0 ,
      'nm_Scope_Sftp'     => 0 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'following' => array (
      'sr_OduxVarName'    => 'sr_TaskFollowedRef' ,
      'sr_Title'          => 'Followed task' ,
      'sr_Descr'          => 'Reference which generated this task' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'belongsTo' => array (
      'sr_OduxVarName'    => 'sr_TaskParentRef' ,
      'sr_Title'          => 'Parent task reference' ,
      'sr_Descr'          => 'Reference of parent of this task' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'files' => array (
      'sr_OduxVarName'    => 'ar_TaskFileList' ,
      'sr_Title'          => 'File list' ,
      'sr_Descr'          => 'List of files in the task' ,
      'sr_Dict'           => 'TaskFile' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'verifiedLength' => array (
      'sr_OduxVarName'    => 'nm_TaskSizeVerified' ,
      'sr_Title'          => 'Verified size' ,
      'sr_Descr'          => 'Size (bytes) hash checked (only when being hash checked)' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    'verifyIntegrityPending' => array (
      'sr_OduxVarName'    => 'bl_TaskVerifyPendingStatus' ,
      'sr_Title'          => 'Verification pending' ,
      'sr_Descr'          => 'Task is waiting for the hash check in a queue (only when task is in the queue)' ,
      'nm_Scope_Http'     => 31 ,
      'nm_Scope_Ftp'      => 31 ,
      'nm_Scope_Sftp'     => 31 ,
      'nm_Scope_Bt'       => 31 ,
      'nm_Scope_Metalink' => 31
    ) ,
    
    
    'BitTorrent' => array (
      'bittorent' => array (
        'announceList' => array (
          'sr_Title'       => 'Announce list' ,
          'sr_Description' => 'Is the peer choking Aria2?' ,
        ) ,
        'comment' => array (
          'sr_Title'       => 'Comment' ,
          'sr_Description' => 'Is the peer choking Aria2?' ,
        ) ,
        'creationDate' => array (
          'sr_Title'       => 'Create date' ,
          'sr_Description' => 'Is the peer choking Aria2?' ,
        ) ,
        'info' => array (
          'name' => array (
            'sr_Title'       => 'Name' ,
            'sr_Description' => 'Is the peer choking Aria2?' ,
          ) ,
        ) ,
        'mode' => array (
          'sr_Title'       => 'Mode' ,
          'sr_Description' => 'Is the peer choking Aria2?' ,
        ) ,
      ) ,
    ) ,
  ) ;
?>