<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * This file consists of variable declaration for an associative array of strings
  * Each key name corresponds to Odux downloader commands
  * Each corresponding value contains the actual commands for the downloader module
  */
  $ar_Downloader_Command = array (
    // Global summaries
    'ListGlobalStat'        => array (
      'sr_ModuleCmd'  => 'getGlobalStat' ,
    ) ,
    'SessionInfo'           => array (
      'sr_ModuleCmd'  => 'getSessionInfo' ,
    ) ,
    'Version'               => array (
      'sr_ModuleCmd'  => 'getVersion' ,
    ) ,
    'SummariseStopped'      => array (
      'sr_ModuleCmd'  => 'tellStopped' ,
    ) ,
    'SummariseQueued'       => array (
      'sr_ModuleCmd'  => 'tellWaiting' ,
    ) ,
    'SummariseActive'       => array (
      'sr_ModuleCmd'  => 'tellActive' ,
    ) ,

    // Global operations
    'PauseAll'              => array (
      'sr_ModuleCmd'  => 'pauseAll' ,
    ) ,
    'PauseAllForce'         => array (
      'sr_ModuleCmd'  => 'forcePauseAll' ,
    ) ,
    'ResumeAll'             => array (
      'sr_ModuleCmd'  => 'unpauseAll' ,
    ) ,
    'PurgeStoppedTasks'     => array (
      'sr_ModuleCmd'  => 'purgeDownloadResult' ,
    ) ,
    'Exit'                  => array (
      'sr_ModuleCmd'  => 'shutdown' ,
    ) ,
    'Kill'                  => array (
      'sr_ModuleCmd'  => 'forceShutdown' ,
    ) ,
    'SaveSession'           => array (
      'sr_ModuleCmd'  => 'saveSession' ,
    ) ,

    // Global configuration
    'SummariseGlobalConfig' => array (
      'sr_ModuleCmd'  => 'getGlobalOption' ,
    ) ,
    'SetGlobalConfig'       => array (
      'sr_ModuleCmd'  => 'changeGlobalOption' ,
    ) ,

    // Task information
    'SummariseTask'   => array (
      'sr_ModuleCmd'  => 'tellStatus' ,
      'ar_AltArgList' => array (
        'Minimal' => array ( array (
          'gid' ,
          'status'
        ) ) ,
        'Concise' => array ( array (
          'gid' ,
          'status' ,
          'totalLength' ,
          'completedLength' ,
          'uploadLength' ,
          'downloadSpeed' ,
          'uploadSpeed'
        ) ) ,
        'Basic' => array ( array (
          'gid' ,
          'status' ,
          'totalLength' ,
          'completedLength' ,
          'uploadLength' ,
          'bitfield' ,
          'downloadSpeed' ,
          'uploadSpeed' ,
          'pieceLength' ,
          'numPieces' ,
          'connections' ,
          'errorCode' ,
          'errorMessage' ,
          'dir' ,
        ) ) ,
        'ErrorInfo' => array ( array (
          'gid' ,
          'status' ,
          'errorMessage'
        ) ) ,
        'All'   => array()
      ) ,
    ) ,
    'SummariseTaskConn'  => array (
      'sr_ModuleCmd'  => 'getServers' ,
    ) ,
    'SummariseTaskPeer'    => array (
      'sr_ModuleCmd'  => 'getPeers' ,
    ) ,
    'SummariseTaskFile'    => array (
      'sr_ModuleCmd'  => 'getFiles' ,
    ) ,
    'SummariseTaskSource'     => array (
      'sr_ModuleCmd'  => 'getUris' ,
    ) ,

    // Task operations
    'AddTask'               => array (
      'sr_ModuleCmd'  => 'addUri' ,
    ) ,
    'ChangeTaskFileUrl'     => array (
      'sr_ModuleCmd'  => 'changeUri' , // Need a map of arguments
    ) ,
    'AddTaskMetalink'       => array (
      'sr_ModuleCmd'  => 'addMetalink' ,
    ) ,
    'AddTaskBittorrent'     => array (
      'sr_ModuleCmd'  => 'addTorrent' ,
    ) ,
    'PauseTask'             => array (
      'sr_ModuleCmd'  => 'pause' ,
    ) ,
    'ResumeTask'            => array (
      'sr_ModuleCmd'  => 'unpause' ,
    ) ,
    'RemoveTask'            => array (
      'sr_ModuleCmd'  => 'remove' ,
    ) ,
    'PauseTaskForce'        => array (
      'sr_ModuleCmd'  => 'forcePause' ,
    ) ,
    'RemoveTaskForce'       => array (
      'sr_ModuleCmd'  => 'forceRemove' ,
    ) ,
    'RemoveStoppedTask'     => array (
      'sr_ModuleCmd'  => 'removeDownloadResult' ,
    ) ,
    'ChangePosition'        => array (
      'sr_ModuleCmd'  => 'changePosition' ,
      'sr_Title'      => 'Change task position/priority' ,
      'sr_Descr'      => 'Change the position of the task to customise sequence of tasks' ,
      'ar_AltArgList' => array (
        'MoveFwd'   => array ( -1 , 'POS_CUR' ) ,
        'MoveBkwd'  => array ( 1 , 'POS_CUR' ) ,
        'MoveFront' => array ( 0 , 'POS_SET' ) ,
        'MoveBack'  => array ( 0 , 'POS_END' )
      ) ,
    ) ,

    // Task configuration
    'SummariseTaskConfig'   => array (
      'sr_ModuleCmd'  => 'getOption' ,
    ) ,
    'SetTaskConfig'         => array (
      'sr_ModuleCmd'  => 'changeOption' ,
    )
  ) ;
?>