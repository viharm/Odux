﻿<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* Define a variable to store fields for sources (URL) */
  $ar_Downloader_TaskSource = array (
    'status' => array (
      'sr_OduxVarName'    => 'sr_TaskSourceStatus' ,
      'sr_Title'          => 'Status' ,
      'sr_Descr'          => 'Status of source' ,
      'ar_AltValueList'   => array (
        'used'     => 'Active',
        'waiting'  => 'Queued'
      ) ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 31 ,
        'nm_Task_Ftp'      => 31 ,
        'nm_Task_Sftp'     => 31 ,
        'nm_Task_Bt'       => 31 ,
        'nm_Task_Metalink' => 31
      )
    ) ,
    'uri' => array (
      'sr_OduxVarName'    => 'sr_TaskSourceUrl' ,
      'sr_Title'          => 'Source URL' ,
      'sr_Descr'          => 'URL of the source used' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 31 ,
        'nm_Task_Ftp'      => 31 ,
        'nm_Task_Sftp'     => 31 ,
        'nm_Task_Bt'       => 31 ,
        'nm_Task_Metalink' => 31
      )
    )
  ) ;
?>