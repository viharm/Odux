<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * This file consists of variable declaration for an associative array of strings
  * Each key name corresponds to downloader module output keys
  * Each corresponding value contains the Odux normalised output.
  */

//static $ar_Downloader_Result ;
  $ar_Downloader_Result = array (
    // Global summaries
    'downloadSpeed'   => array (
      'sr_OduxVarName'    => 'nm_SpeedDown' ,
      'sr_Title'          => 'Download speed'
    ) ,
    'uploadSpeed'     => array (
      'sr_OduxVarName'    => 'nm_SpeedUp' ,
      'sr_Title'          => 'Upload speed'
    ) ,
    'numActive'       => array (
      'sr_OduxVarName'    => 'nm_CountActive' ,
      'sr_Title'          => 'Active tasks'
    ) ,
    'numWaiting'      => array (
      'sr_OduxVarName'    => 'nm_CountWaiting' ,
      'sr_Title'          => 'Waiting tasks'
    ) ,
    'numStopped'      => array (
      'sr_OduxVarName'    => 'nm_CountStopped' ,
      'sr_Title'          => 'Stopped tasks'
    ) ,
    'numStoppedTotal' => array (
      'sr_OduxVarName'    => 'nm_CountStoppedTotal' ,
      'sr_Title'          => 'Stopped tasks (uncapped)'
    ) ,
    
    // Task information
    'bitfield'   => array (
      'sr_OduxVarName'    => 'sr_TaskPieceMap' ,
      'sr_Title'          => 'Piece map'
    ) ,
    'completedLength'     => array (
      'sr_OduxVarName'    => 'nm_TaskSizeCompleted' ,
      'sr_Title'          => 'Downloaded size'
    ) ,
    'connections'       => array (
      'sr_OduxVarName'    => 'nm_TaskConnCount' ,
      'sr_Title'          => 'Connections'
    ) ,
    'dir'      => array (
      'sr_OduxVarName'    => 'sr_TaskSavePath' ,
      'sr_Title'          => 'Save path'
    ) ,
    'gid' => array (
      'sr_OduxVarName'    => 'sr_TaskRef' ,
      'sr_Title'          => 'Task ref'
    ) ,
    'numPieces'   => array (
      'sr_OduxVarName'    => 'nm_TaskPieceCount' ,
      'sr_Title'          => 'Piece count'
    ) ,
    'pieceLength'     => array (
      'sr_OduxVarName'    => 'sr_PieceSize' ,
      'sr_Title'          => 'Piece size'
    ) ,
    'status'       => array (
      'sr_OduxVarName'    => 'sr_TaskStatus' ,
      'sr_Title'          => 'Status'
    ) ,
    'totalLength'      => array (
      'sr_OduxVarName'    => 'nm_TaskSizeTotal' ,
      'sr_Title'          => 'Total size'
    ) ,
    'uploadLength'      => array (
      'sr_OduxVarName'    => 'nm_TaskSizeUp' ,
      'sr_Title'          => 'Uploaded size'
    ) ,
    
    // Downlaoder info
    'enabledFeatures' => array (
      'sr_OduxVarName'    => 'ar_FeatureList' ,
      'sr_Title'          => 'Features'
    ) ,
    'version' => array (
      'sr_OduxVarName'    => 'sr_DownloaderVersion' ,
      'sr_Title'          => 'Version'
    ) ,
    'sessionId' => array (
      'sr_OduxVarName'    => 'sr_DownloaderSession' ,
      'sr_Title'          => 'Session'
    ) ,
    
  ) ;
?>