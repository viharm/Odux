﻿<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* Define a variable to store fields for connections */
  $ar_Downloader_TaskConn = array (
    'index' => array (
      'sr_OduxVarName'    => 'nm_TaskFileIndex' ,
      'sr_Title'          => 'File index' ,
      'sr_Descr'          => 'Index of file' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'servers' => array (
      'sr_OduxVarName'    => 'ar_TaskConnConn' ,
      'sr_Title'          => 'Connection list' ,
      'sr_Descr'          => 'List of connections for the specified file in the task' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'uri' => array (
      'sr_OduxVarName'    => 'sr_TaskConnUrlOrig' ,
      'sr_Title'          => 'Original URL' ,
      'sr_Descr'          => 'Original URL for the connection' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'currentUri' => array (
      'sr_OduxVarName'    => 'sr_TaskConnUrlCurrent' ,
      'sr_Title'          => 'Current URL' ,
      'sr_Descr'          => 'Current URL of the connection' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'downloadSpeed' => array (
      'sr_OduxVarName'    => 'nm_TaskConnSpeedDown' ,
      'sr_Title'          => 'Download speed' ,
      'sr_Descr'          => 'Download speed for connection' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    )
  ) ;
?>