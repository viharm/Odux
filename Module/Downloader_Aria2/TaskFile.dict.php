<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  $ar_Downloader_TaskFile = array (
    'index' => array (
      'sr_OduxVarName'    => 'nm_TaskFileIndex' ,
      'sr_Title'          => 'File index' ,
      'sr_Descr'          => 'Index of file' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'path' => array (
      'sr_OduxVarName'    => 'sr_TaskFilePath' ,
      'sr_Title'          => 'File location' ,
      'sr_Descr'          => 'Location of file' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'length' => array (
      'sr_OduxVarName'    => 'nm_TaskFileSizeTotal' ,
      'sr_Title'          => 'File size' ,
      'sr_Descr'          => 'Size of file' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'completedLength' => array (
      'sr_OduxVarName'    => 'nm_TaskFileSizeDown' ,
      'sr_Title'          => 'File downloaded size' ,
      'sr_Descr'          => 'Downloaded size of file' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'selected' => array (
      'sr_OduxVarName'    => 'bl_TaskFileEnabled' ,
      'sr_Title'          => 'File selected' ,
      'sr_Descr'          => 'Indicator if file is selected for download' ,
      'ar_AltValueList'   => array (
        'true'  => TRUE ,
        'false' => FALSE
      ) ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    ) ,
    'uris' => array ( // suspect not for BitTorrent, but trying anyway
      'sr_OduxVarName'    => 'ar_TaskFileSource' ,
      'sr_Title'          => 'Source list' ,
      'sr_Descr'          => 'List of sources for the selected file in the task' ,
      'sr_Dict'           => 'TaskSource' ,
      'ar_Scope'          => array (
        'nm_App'           => 0 ,
        'nm_Task_Http'     => 4 ,
        'nm_Task_Ftp'      => 4 ,
        'nm_Task_Sftp'     => 4 ,
        'nm_Task_Bt'       => 4 ,
        'nm_Task_Metalink' => 4
      )
    )
  ) ;
?>