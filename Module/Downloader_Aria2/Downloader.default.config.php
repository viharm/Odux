<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Set Downloader configuration
  $ar_App['ar_Downloader']['ar_Conn']['ar_Config'] = array (
    'host'      => '127.0.0.1' ,
    'port'      => '6800' ,
    'rpcsecret' => 'RpcSecretToken' ,
    'secure'    => FALSE ,
    'cacert'    => NULL ,
    'rpcuser'   => NULL ,
    'rpcpass'   => NULL ,
    'proxy'     => array (
      'type' => NULL ,
      'host' => NULL ,
      'port' => NULL ,
      'user' => NULL ,
      'pass' => NULL
    )
  ) ;
?>