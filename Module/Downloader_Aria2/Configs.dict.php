<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * This file consists of variable declaration for an associative array of strings
  * Each key name corresponds to downloader module configuration options
  * Each corresponding value contains the Odux normalised options.
  */
  $ar_Downloader_Configs = array (
        'download-result'                  => array (
          'sr_OduxVarName'    => 'sr_TaskSummaryVerbosity' ,
          'sr_Title'          => 'Download results format' ,
          'sr_Descr'          => 'This option changes the way download results is formatted' ,
          'ar_AltValueList'   => array ( 
            'default' => 'Default' ,
            'full'    => 'Full' ,
            'hide'    => 'None'
          ) ,
          'sr_DefaultVal'     => 'default' ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'log'                         => array (
          'sr_OduxVarName'    => 'sr_LogFile' ,
          'sr_Title'          => 'Log file' ,
          'sr_Descr'          => 'The path and file name of the log file; "-" ⇒ STDOUT; Blank ⇒ disable' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'log-level'                        => array (
          'sr_OduxVarName'    => 'sr_LogVerbosity' ,
          'sr_Title'          => 'Log level' ,
          'sr_Descr'          => 'Set log level to output' ,
          'ar_AltValueList'   => array (
            'debug'  => 'Debug' ,
            'info'   => 'Info' ,
            'notice' => 'Notice' ,
            'warn'   => 'Warn' ,
            'error'  => 'Error'
          ) ,
          'sr_DefaultVal'     => 'Debug' ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-concurrent-downloads'          => array (
          'sr_OduxVarName'    => 'nm_MaxParallelPerTask' ,
          'sr_Title'          => 'Maximum concurrent downloads per task' ,
          'sr_Descr'          => 'Set the maximum number of parallel downloads per task' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 5 ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-download-result'               => array (
          'sr_OduxVarName'    => 'nm_MaxSavedTaskResult' ,
          'sr_Title'          => 'Maximum download results saved' ,
          'sr_Descr'          => 'Set maximum number of download result kept in memory' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 1000 ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-overall-download-limit'         => array (
          'sr_OduxVarName'    => 'nm_GlobalLimitDown' ,
          'sr_Title'          => 'Maximum overall download limit' ,
          'sr_Descr'          => 'Set max overall download speed in bytes/sec. 0 ⇒ unrestricted' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-overall-upload-limit'           => array (
          'sr_OduxVarName'    => 'nm_GlobalLimitUp' ,
          'sr_Title'          => 'Maximum overall upload limit' ,
          'sr_Descr'          => 'Set max overall upload speed in bytes/sec. 0 ⇒ unrestricted' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'optimize-concurrent-downloads'           => array (
          'sr_OduxVarName'    => 'bl_AutoParallelPerTask' ,
          'sr_Title'          => 'Optimise concurrent downloads per task' ,
          'sr_Descr'          => 'Optimizes the number of concurrent downloads according to the bandwidth available, based on recent history' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'save-cookies'                     => array (
          'sr_OduxVarName'    => 'sr_CookieFile' ,
          'sr_Title'          => 'Save cookies to (file)' ,
          'sr_Descr'          => 'Specify file path and name to save cookies (in Mozilla/Firefox(1.x/2.x)/ Netscape format); Overwritten if exists' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'save-session'                     => array (
          'sr_OduxVarName'    => 'sr_SessionFile' ,
          'sr_Title'          => 'Save session to (file)' ,
          'sr_Descr'          => 'Save error/unfinished downloads to specified file on exit' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'server-stat-of'            => array (
          'sr_OduxVarName'    => 'sr_ServerStatsOutputFile' ,
          'sr_Title'          => 'Server statistics output file' ,
          'sr_Descr'          => 'Specify the file name to which performance profile of the servers is saved' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dir'               => array (
          'sr_OduxVarName'    => 'sr_SavePath' ,
          'sr_Title'          => 'Download directory' ,
          'sr_Descr'          => 'The directory to store the downloaded file(s)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'max-download-limit'           => array (
          'sr_OduxVarName'    => 'nm_TaskSpeedDownCeiling' ,
          'sr_Title'          => 'Maximum download speed limit' ,
          'sr_Descr'          => 'Set max download speed per each download in bytes/sec. 0 ⇒ unrestricted' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 31 ,
            'nm_Task_Http'     => 31 ,
            'nm_Task_Ftp'      => 31 ,
            'nm_Task_Sftp'     => 31 ,
            'nm_Task_Bt'       => 31 ,
            'nm_Task_Metalink' => 31
          )
        ) ,
        'max-upload-limit'             => array (
          'sr_OduxVarName'    => 'nm_TaskSpeedUpCeiling' ,
          'sr_Title'          => 'Maximum upload speed limit' ,
          'sr_Descr'          => 'Set max upload speed per each download in bytes/sec. 0 ⇒ unrestricted' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 31 ,
            'nm_Task_Http'     => 31 ,
            'nm_Task_Ftp'      => 31 ,
            'nm_Task_Sftp'     => 31 ,
            'nm_Task_Bt'       => 31 ,
            'nm_Task_Metalink' => 31
          )
        ) ,
        'lowest-speed-limit' => array (
          'sr_OduxVarName'    => 'nm_TaskSpeedDownFloor' ,
          'sr_Title'          => 'Minimum download speed' ,
          'sr_Descr'          => 'Stop task if download speed is lower than or equal to this value(bytes per sec). 0 ⇒ no lowest speed limit' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 31 ,
            'nm_Task_Http'     => 31 ,
            'nm_Task_Ftp'      => 31 ,
            'nm_Task_Sftp'     => 31 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 31
          )
        ) ,
        'piece-length' => array (
          'sr_OduxVarName'    => 'sr_PieceSize' ,
          'sr_Title'          => 'Piece size' ,
          'sr_Descr'          => 'Set a piece size for downloads. This is the boundary when aria2 splits a file. Ignored if Metalink file contains piece hashes' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '1M' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'pause-metadata' => array (
          'sr_OduxVarName'    => 'bl_PauseAfterMetadataFetch' ,
          'sr_Title'          => 'Pause after metadata download' ,
          'sr_Descr'          => 'Pause downloads created as a result of metadata download. Applicable only when Aria2 RPC interface is enabled' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 24 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'rpc-save-upload-metadata' => array (
          'sr_OduxVarName'    => 'bl_SaveUploadMetadata' ,
          'sr_Title'          => 'Save upload metadata' ,
          'sr_Descr'          => 'Save the uploaded torrent or metalink meta data in the configured save path as a file' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'show-files' => array (
          'sr_OduxVarName'    => 'bl_ShowFileList' ,
          'sr_Title'          => 'Show list of files and exit' ,
          'sr_Descr'          => 'Print file listing of ".torrent" (include infohash, piece length, etc), ".meta4" and ".metalink" file and exit' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'auto-file-renaming'          => array (
          'sr_OduxVarName'    => 'bl_RenameFileIfExist' ,
          'sr_Title'          => 'Rename if file exists' ,
          'sr_Descr'          => 'Rename file name if the same file already exists' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'connect-timeout'          => array (
          'sr_OduxVarName'    => 'nm_ConnectTimeout' ,
          'sr_Title'          => 'Connection timeout' ,
          'sr_Descr'          => 'Set the connect timeout in seconds to establish connection to HTTP/FTP/proxy server' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'continue'        => array (
          'sr_OduxVarName'    => 'bl_ResumeTask' ,
          'sr_Title'          => 'Resume file or task' ,
          'sr_Descr'          => 'Continue downloading a partially downloaded file which may have been started by another program; expect data to be saved sequentially from the beginning' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-connection-per-server' => array (
          'sr_OduxVarName'    => 'nm_ThreadPerServer_Ceiling' ,
          'sr_Title'          => 'Maximum connections per server' ,
          'sr_Descr'          => 'The maximum number of connections to one server for each download' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 1 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-file-not-found'    => array (
          'sr_OduxVarName'    => 'nm_RetryCountIfMissing_Ceiling' ,
          'sr_Title'          => 'Maximum retries if not found' ,
          'sr_Descr'          => 'Fail task if file not found after specified number of retries. Counts toward overall task retry count ceiling. 0 ⇒ disabled' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'max-tries'                => array (
          'sr_OduxVarName'    => 'nm_RetryCount_Ceiling' ,
          'sr_Title'          => 'Maximum retries' ,
          'sr_Descr'          => 'Maximum number of tries. 0 => disabled' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 5 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'min-split-size'            => array (
          'sr_OduxVarName'    => 'sr_SplitSize_Floor' ,
          'sr_Title'          => 'Minimum file size for splitting' ,
          'sr_Descr'          => 'Minimum size of split; File is not split if file is less than twice this size (allowed 1M ~ 1024M)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '20M' ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'no-proxy'  => array (
          'sr_OduxVarName'    => 'sr_ProxyExceptionList' ,
          'sr_Title'          => 'Locations to bypass proxy' ,
          'sr_Descr'          => 'comma separated list of host names, domains and network addresses with or without a subnet mask where no proxy should be used' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'proxy-method'             => array (
          'sr_OduxVarName'    => 'sr_ProxyMethod' ,
          'sr_Title'          => 'Proxy method' ,
          'sr_Descr'          => 'Set the method to use in proxy request; Ignored for HTTPS downloads ("Tunnel" used)' ,
          'ar_AltValueList'   => array (
            'get'    => 'Get' ,
            'tunnel' => 'Tunnel'
          ) ,
          'sr_DefaultVal'     => 'Get' ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'referer'                 => array (
          'sr_OduxVarName'    => 'sr_Referer' ,
          'sr_Title'          => 'HTTP referer' ,
          'sr_Descr'          => 'Set HTTP(S) referer. Use "*" to use the source URL' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'remote-time'         => array (
          'sr_OduxVarName'    => 'bl_UseServerFileTimestamp' ,
          'sr_Title'          => 'Use remote timestamp' ,
          'sr_Descr'          => 'Retrieve timestamp of the remote file from the remote HTTP/FTP server and if it is available, apply it to the local file' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'retry-wait'       => array (
          'sr_OduxVarName'    => 'nm_RetryWaitDuration' ,
          'sr_Title'          => 'Retry wait duration' ,
          'sr_Descr'          => 'Duration (seconds) to wait between retries when the HTTP server returns a 503 response. 0 ⇒ disabled' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'reuse-uri'                => array (
          'sr_OduxVarName'    => 'bl_ReuseSource' ,
          'sr_Title'          => 'Reuse sources' ,
          'sr_Descr'          => 'Reuse already used URIs if no unused URIs are left' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'split'    => array (
          'sr_OduxVarName'    => 'nm_ThreadCountPerFile' ,
          'sr_Title'          => 'Number of connections' ,
          'sr_Descr'          => 'Number of connections per file' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 5 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'stream-piece-selector' => array (
          'sr_OduxVarName'    => 'sr_PieceSelectionAlgorithm' ,
          'sr_Title'          => 'Piece selection algorithm' ,
          'sr_Descr'          => 'Specify piece selection algorithm used in HTTP/FTP download' ,
          'ar_AltValueList'   => array (
            'default' => 'Default' ,
            'inorder' => 'Ordered' ,
            'random'  => 'Random' ,
            'geom'    => 'Geometric'
          ) ,
          'sr_DefaultVal'     => 'Default' ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'timeout'                 => array (
          'sr_OduxVarName'    => 'nm_PostConnectTimeout' ,
          'sr_Title'          => 'Timeout' ,
          'sr_Descr'          => 'Set timeout in seconds after initial connection' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'uri-selector'   => array (
          'sr_OduxVarName'    => 'sr_UrlSelectionAlgorithm' ,
          'sr_Title'          => 'URL selection algorithm' ,
          'sr_Descr'          => 'Specify URL selection algorithm' ,
          'ar_AltValueList'   => array (
            'inorder'  => 'Ordered' ,
            'feedback' => 'Feedback' ,
            'adaptive' => 'Adaptive'
          ) ,
          'sr_DefaultVal'     => 'Feedback' ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'use-head'  => array (
          'sr_OduxVarName'    => 'bl_UseHttpHeadMethod' ,
          'sr_Title'          => 'Use HEAD for first request' ,
          'sr_Descr'          => 'Use HEAD method for the first request to the HTTP server' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'conditional-get'        => array (
          'sr_OduxVarName'    => 'bl_DownloadIfNew' ,
          'sr_Title'          => 'Download if new' ,
          'sr_Descr'          => 'Download file only when the local file is older than remote file; Only for HTTP(S) downloads; Ignored if file size specified in Metalink, or if control file exists; Ignores Content-Disposition header' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'header' => array (
          'sr_OduxVarName'    => 'sr_HttpHeader' ,
          'sr_Title'          => 'Additional HTTP header' ,
          'sr_Descr'          => 'Append HEADER to HTTP request header' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-accept-gzip'           => array (
          'sr_OduxVarName'    => 'bl_AcceptCompressedStream' ,
          'sr_Title'          => 'Accept compressed stream' ,
          'sr_Descr'          => 'Accept Gzip or Deflate encoded compressed content in the data stream; auto-inflate in receipt' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-auth-challenge'     => array (
          'sr_OduxVarName'    => 'bl_AuthOnChallenge' ,
          'sr_Title'          => 'Authenticate only if challenged' ,
          'sr_Descr'          => 'Send HTTP authorisation header only when it is requested by the server; else always send' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-no-cache'          => array (
          'sr_OduxVarName'    => 'bl_RequestAvoidCachedContent' ,
          'sr_Title'          => 'Request avoid cached' ,
          'sr_Descr'          => 'Request server to avoid cached content' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-user'             => array (
          'sr_OduxVarName'    => 'sr_HttpAuthUser' ,
          'sr_Title'          => 'HTTP username' ,
          'sr_Descr'          => 'Set HTTP authentication username for HTTP(S) downloads' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-passwd'             => array (
          'sr_OduxVarName'    => 'sr_HttpAuthPass' ,
          'sr_Title'          => 'HTTP password' ,
          'sr_Descr'          => 'Set HTTP authentication password for HTTP(S) downloads' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-proxy'            => array (
          'sr_OduxVarName'    => 'sr_HttpTaskProxy' ,
          'sr_Title'          => 'Proxy string (HTTP)' ,
          'sr_Descr'          => 'Use a proxy server for all HTTP downloads; Format "[http://][USER:PASSWORD@]HOST[:PORT]"; "" ⇒ remove previously defined' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-proxy-user'            => array (
          'sr_OduxVarName'    => 'sr_HttpTaskProxyUser' ,
          'sr_Title'          => 'Proxy username (HTTP)' ,
          'sr_Descr'          => 'Set proxy username for HTTP downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'http-proxy-passwd'        => array (
          'sr_OduxVarName'    => 'sr_HttpTaskProxyPass' ,
          'sr_Title'          => 'Proxy password (HTTP)' ,
          'sr_Descr'          => 'Set proxy password for HTTP downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'https-proxy'      => array (
          'sr_OduxVarName'    => 'sr_HttpsTaskProxy' ,
          'sr_Title'          => 'Proxy string (HTTPS)' ,
          'sr_Descr'          => 'Use a proxy server for all HTTPS downloads; Format [http://][USER:PASSWORD@]HOST[:PORT]; "" ⇒ remove previously defined' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'https-proxy-user'      => array (
          'sr_OduxVarName'    => 'sr_HttpsTaskProxyUser' ,
          'sr_Title'          => 'Proxy username (HTTPS)' ,
          'sr_Descr'          => 'Proxy username for HTTPS downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'https-proxy-passwd'  => array (
          'sr_OduxVarName'    => 'sr_HttpsTaskProxyPass' ,
          'sr_Title'          => 'Proxy password (HTTPS)' ,
          'sr_Descr'          => 'Proxy password for HTTPS downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'user-agent'            => array (
          'sr_OduxVarName'    => 'sr_HttpUserAgent' ,
          'sr_Title'          => 'HTTP(S) user agent' ,
          'sr_Descr'          => 'User agent string for HTTP(S) downloads' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 'aria2/<VERSION>' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ca-certificate' => array (
          'sr_OduxVarName'    => 'sr_HttpCaCertFile' ,
          'sr_Title'          => 'CA file to verify peers' , 
          'sr_Descr'          => 'Certificate authorities file to verify the peers, in PEM format; Can contain multiple CA certificates' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'certificate' => array (
          'sr_OduxVarName'    => 'sr_HttpClientCertFile' ,
          'sr_Title'          => 'Client certificate file' , 
          'sr_Descr'          => 'Client certificate file in PKCS12 (certificate, key and optional chain of additional certificates; with a blank import password) or PEM (separately specify private key) formats; PKCS12 (.p12, .pfx) or PEM file location' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'private-key' => array (
          'sr_OduxVarName'    => 'sr_HttpClientPrivKeyFile' ,
          'sr_Title'          => 'Client private key' , 
          'sr_Descr'          => 'Client private key file in decrypted PEM format' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'check-certificate' => array (
          'sr_OduxVarName'    => 'bl_HttpVerifyPeerCert' ,
          'sr_Title'          => 'Verify peer certificate' , 
          'sr_Descr'          => 'Verify the peer using supplied CA certificates' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'load-cookies' => array (
          'sr_OduxVarName'    => 'sr_HttpCookieFile' ,
          'sr_Title'          => 'Load cookies from (file)' , 
          'sr_Descr'          => 'Load Cookies from FILE using the Firefox3 format (SQLite3), Chromium/Google Chrome (SQLite3) and the Mozilla/Firefox(1.x/2.x)/Netscape format' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-user'          => array (
          'sr_OduxVarName'    => 'sr_FtpAuthUser' ,
          'sr_Title'          => 'FTP username' ,
          'sr_Descr'          => 'Set FTP authentication username for FTP downloads' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 'anonymous' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-passwd'          => array (
          'sr_OduxVarName'    => 'sr_FtpAuthPass' ,
          'sr_Title'          => 'FTP password' ,
          'sr_Descr'          => 'Set FTP authentication password for FTP downloads (if not available in .netrc)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 'ARIA2USER@' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-pasv' => array (
          'sr_OduxVarName'    => 'bl_FtpPassiveModeEnable' ,
          'sr_Title'          => 'Enable passive mode' ,
          'sr_Descr'          => 'Use the passive mode in FTP. Active mode used if false. Ignored for SFTP' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-proxy'         => array (
          'sr_OduxVarName'    => 'sr_FtpTaskProxy' ,
          'sr_Title'          => 'Proxy string (FTP)' ,
          'sr_Descr'          => 'Proxy server for all FTP downloads; Format "[http://][USER:PASSWORD@]HOST[:PORT]"; "" ⇒ remove previously defined' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-proxy-user'         => array (
          'sr_OduxVarName'    => 'sr_FtpTaskProxyUser' ,
          'sr_Title'          => 'Proxy username (FTP)' ,
          'sr_Descr'          => 'Proxy username for FTP downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-proxy-passwd'     => array (
          'sr_OduxVarName'    => 'sr_FtpTaskProxyPass' ,
          'sr_Title'          => 'Proxy password (FTP)' ,
          'sr_Descr'          => 'Proxy password for FTP downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-reuse-connection'   => array (
          'sr_OduxVarName'    => 'bl_FtpReuseConnection' ,
          'sr_Title'          => 'Reuse FTP connection' ,
          'sr_Descr'          => 'Reuse existing FTP connection' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'ftp-type'   => array (
          'sr_OduxVarName'    => 'sr_FtpTransferType' ,
          'sr_Title'          => 'FTP transfer type' ,
          'sr_Descr'          => 'Type of FTP transfer approach; Ignored for SFTP' ,
          'ar_AltValueList'   => array (
            'binary' => 'Binary' ,
            'ascii'  => 'ASCII'
          ) ,
          'sr_DefaultVal'     => 'Binary' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'no-netrc'      => array (
          'sr_OduxVarName'    => 'bl_NetrcDisable' ,
          'sr_Title'          => 'Disable .netrc' ,
          'sr_Descr'          => 'Disable netrc support' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 16 ,
            'nm_Task_Ftp'      => 16 ,
            'nm_Task_Sftp'     => 16 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'follow-metalink'          => array (
          'sr_OduxVarName'    => 'sr_MetalinkCreateSubtask' ,
          'sr_Title'          => 'Follow metalink' ,
          'sr_Descr'          => 'Create subtasks for contents of the provided metalink file task; If no, treat the metalink file as the task; If yes, specify if the metalink file should be saved or held in memory' ,
          'ar_AltValueList'   => array (
            'true'  => 'True' ,
            'false' => 'False' ,
            'mem'   => 'Memory'
          ) ,
          'sr_DefaultVal'     => 'True' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-base-uri'                 => array (
          'sr_OduxVarName'    => 'sr_MetalinkBaseLocn' ,
          'sr_Title'          => 'Metalink base location (URL/URI/path)' ,
          'sr_Descr'          => 'Base URI to resolve relative URI in "metalink:url" and "metalink:metaurl" elements in a metalink file stored in local disk; If URI points to a directory, URI must end with /' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-enable-unique-protocol'    => array (
          'sr_OduxVarName'    => 'bl_MetalinkUseUniqueProtocol' ,
          'sr_Title'          => 'Enable unique protocol' ,
          'sr_Descr'          => 'Use only one protocol for mirrors if more than one specified in metalink file; Preferred protocol can be specified separately' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-language'        => array (
          'sr_OduxVarName'    => 'sr_MetalinkFileLang' ,
          'sr_Title'          => 'Download language' ,
          'sr_Descr'          => 'The language of the file to download; Use two-tag notation as per ISO639-1 and ISO3166-1 (e.g., en-US)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-location' => array (
          'sr_OduxVarName'    => 'sr_MetalinkPreferServerLocn' ,
          'sr_Title'          => 'Preferred server location' ,
          'sr_Descr'          => 'The location of the preferred server; One or more (comma-delimited list) locations using two-character country names (e.g. jp,us)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-os'              => array (
          'sr_OduxVarName'    => 'sr_MetalinkFileOs' ,
          'sr_Title'          => 'Download OS for file' ,
          'sr_Descr'          => 'The operating system of the file to download' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-preferred-protocol'       => array (
          'sr_OduxVarName'    => 'sr_MetalinkPreferProtocol' ,
          'sr_Title'          => 'Preferred protocol' ,
          'sr_Descr'          => 'Specify preferred protocol; "None" ⇒ disable' ,
          'ar_AltValueList'   => array (
            'none'  => 'None' ,
            'http'  => 'HTTP' ,
            'https' => 'HTTPS' ,
            'ftp'   => 'FTP'
          ) ,
          'sr_DefaultVal'     => 'None' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'metalink-version'         => array (
          'sr_OduxVarName'    => 'sr_MetalinkFileVersion' ,
          'sr_Title'          => 'File version' ,
          'sr_Descr'          => 'The version of the file to download' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'all-proxy'                    => array (
          'sr_OduxVarName'    => 'sr_AllProxy' ,
          'sr_Title'          => 'Proxy string (all)' ,
          'sr_Descr'          => 'Proxy server for all protocols (unconfirmed for metalink and BitTorrent); Format "[http://][USER:PASSWORD@]HOST[:PORT]"' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'all-proxy-user'                => array (
          'sr_OduxVarName'    => 'sr_AllProxyUser' ,
          'sr_Title'          => 'Proxy username (all)' ,
          'sr_Descr'          => 'Set proxy username for all downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'all-proxy-passwd'                => array (
          'sr_OduxVarName'    => 'sr_AllProxyPass' ,
          'sr_Title'          => 'Proxy password (all)' ,
          'sr_Descr'          => 'Set proxy password for all downloads (if not specified as part of the proxy string)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'allow-overwrite'                  => array (
          'sr_OduxVarName'    => 'bl_AllowOverwrite' ,
          'sr_Title'          => 'Allow overwrite' ,
          'sr_Descr'          => 'Restart download from scratch if no corresponding control file found' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'allow-piece-length-change'          => array (
          'sr_OduxVarName'    => 'bl_AllowDiffPieceSize' ,
          'sr_Title'          => 'Allow different piece size' ,
          'sr_Descr'          => 'Do not abort when a piece length is different from one in a control file' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'always-resume'                    => array (
          'sr_OduxVarName'    => 'bl_ForceAlwaysResume' ,
          'sr_Title'          => 'Always resume' ,
          'sr_Descr'          => 'Always try to resume download, abort if not possible; If not opted, download file from start if cannot resume' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'async-dns'                        => array (
          'sr_OduxVarName'    => 'bl_AsyncDns' ,
          'sr_Title'          => 'Asynchronous DNS' ,
          'sr_Descr'          => 'Enable asynchronous DNS' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'check-integrity'                  => array (
          'sr_OduxVarName'    => 'bl_CheckFileIntegrity' ,
          'sr_Title'          => 'Check file integrity' ,
          'sr_Descr'          => 'Check file integrity by validating piece hashes or a hash of entire file' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'enable-http-keep-alive'      => array (
          'sr_OduxVarName'    => 'bl_HttpConnPersistence' ,
          'sr_Title' => 'HTTP connection persistence' ,
          'sr_Descr'          => 'Enable HTTP/1.1 persistent connection' ,
          'ar_AltValueList'   => array ( TRUE , FALSE ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'enable-http-pipelining'            => array (
          'sr_OduxVarName'    => 'bl_HttpPipeliningEnable' ,
          'sr_Title'          => 'Enable HTTP pipelining' ,
          'sr_Descr'          => 'Enable HTTP/1.1 pipelining' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'enable-mmap'                 => array (
          'sr_OduxVarName'    => 'bl_FileMemMap' ,
          'sr_Title'          => 'Enable memory map' ,
          'sr_Descr'          => 'Map files into memory; May not work if file space is not pre-allocated on disk' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'file-allocation'            => array (
          'sr_OduxVarName'    => 'sr_FileAllocMethod' ,
          'sr_Title'          => 'File allocation method' ,
          'sr_Descr'          => 'Specify file allocation method' ,
          'ar_AltValueList'   => array (
            'prealloc' => 'PreAlloc',
            'none'     => 'None' ,
            'trunc'    => 'Trunc' ,
            'falloc'   => 'FAlloc'
          ) ,
          'sr_DefaultVal'     => 'PreAlloc' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'force-save'                => array (
          'sr_OduxVarName'    => 'bl_ForceSaveTask' ,
          'sr_Title'          => 'Remember download' ,
          'sr_Descr'          => 'Save download even if the download is completed or removed; Also saves control file; May be useful to save BitTorrent seeding which is recognised as completed state' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 31 ,
            'nm_Task_Http'     => 31 ,
            'nm_Task_Ftp'      => 31 ,
            'nm_Task_Sftp'     => 31 ,
            'nm_Task_Bt'       => 31 ,
            'nm_Task_Metalink' => 31
          )
        ) ,
        'hash-check-only'               => array (
          'sr_OduxVarName'    => 'bl_HashCheckExit' ,
          'sr_Title'          => 'Check integrity and abort' ,
          'sr_Descr'          => 'After hash check (when called), abort download whether or not complete' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'max-resume-failure-tries'            => array (
          'sr_OduxVarName'    => 'nm_ResumeFailTryCount_Ceiling' ,
          'sr_Title'          => 'Maximum URIs for resume failure' ,
          'sr_Descr'          => 'Maximum number of source locations to try to resume from before restarting download from scratch' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'no-file-allocation-limit'        => array (
          'sr_OduxVarName'    => 'sr_NoAllocFilesize_Floor' ,
          'sr_Title'          => 'Minimum file size to preallocate space' ,
          'sr_Descr'          => 'No file allocation is made for files whose size is smaller than SIZE' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '5M' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'realtime-chunk-checksum'         => array (
          'sr_OduxVarName'    => 'bl_RealtimChunkChecksum' ,
          'sr_Title'          => 'Check chunk integrity during download' ,
          'sr_Descr'          => 'Validate chunk of data by calculating checksum while downloading a file if chunk checksums are provided' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'remove-control-file' => array (
          'sr_OduxVarName'    => 'bl_PreremoveControlFile' ,
          'sr_Title'          => 'Remove control file before download' ,
          'sr_Descr'          => 'Remove control file before download; May be useful if behind proxy which disables resume' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'pause'      => array (
          'sr_OduxVarName'    => 'bl_AddPaused' ,
          'sr_Title'          => 'Add paused' ,
          'sr_Descr'          => 'Pause download after added' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 4 ,
            'nm_Task_Ftp'      => 4 ,
            'nm_Task_Sftp'     => 4 ,
            'nm_Task_Bt'       => 4 ,
            'nm_Task_Metalink' => 4
          )
        ) ,
        'gid'       => array (
          'sr_OduxVarName'    => 'sr_TaskRef' ,
          'sr_Title'          => 'Task Reference' ,
          'sr_Descr'          => 'Specify GID; Hex string of 16 alphanumeric characters' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 'Auto generated' ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'parameterized-uri' => array (
          'sr_OduxVarName'    => 'bl_ParamUrlSupport' ,
          'sr_Title'          => 'Parameterised URL support' ,
          'sr_Descr'          => 'Enable parameterised URL support; Allowed : set of parts "http://{sv1,sv2,sv3}/foo.iso", numeric sequences with optional step counter "http://host/image[000-100:2].img"; If all sources do not point to the same file, sequential source support is required to continue' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'index-out'         => array (
          'sr_OduxVarName'    => 'sr_TaskRef' ,
          'sr_Title'          => 'Individual file path' ,
          'sr_Descr'          => 'Specify individual file index and corresponding file path relative to the task save path; Format "<index>=/path/to/file"' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 6 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'select-file'                 => array (
          'sr_OduxVarName'    => 'sr_SelectFile' ,
          'sr_Title'          => 'Select files to download' ,
          'sr_Descr'          => 'Select file to download by index; Multiple indexes allowed when separated by "," or "-" to specify a range or a combination of both' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 6 ,
            'nm_Task_Metalink' => 6
          )
        ) ,
        'checksum'                => array (
          'sr_OduxVarName'    => 'sr_HttpFtpFileChecksum' ,
          'sr_Title'          => 'HTTP(S)/FTP file checksum' ,
          'sr_Descr'          => 'Set checksum hash type and hex digest; Format "<TYPE>=<DIGEST>"' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 4 ,
            'nm_Task_Ftp'      => 4 ,
            'nm_Task_Sftp'     => 4 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'out'      => array (
          'sr_OduxVarName'    => 'sr_HttpFtpSaveFile' ,
          'sr_Title'          => 'Downloaded filename' ,
          'sr_Descr'          => 'The file name of the downloaded file, relative to the save directory' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dry-run'             => array (
          'sr_OduxVarName'    => 'bl_HttpFtpTestTask' ,
          'sr_Title'         => 'Test only' ,
          'sr_Descr'          => 'Checks if remote file is available, but not download or add task' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'force-sequential'      => array (
          'sr_OduxVarName'    => 'bl_ForceMultitask' ,
          'sr_Title'          => 'Split URIs into individual downloads' ,
          'sr_Descr'          => 'Fetch URLs sequentially and download each source as a separate task' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'torrent-file'              => array (
          'sr_OduxVarName'    => 'sr_TorrentInputFile' ,
          'sr_Title'          => 'Path to torrent file' ,
          'sr_Descr'          => 'Path to the ".torrent" file; Not required as the ".torrent" file can specified as a source and autodetected' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 4 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'metalink-file'              => array (
          'sr_OduxVarName'    => 'sr_MetalinkInputFile' ,
          'sr_Title'          => 'Path to metalink file' ,
          'sr_Descr'          => 'Path to ".meta4" and ".metalink" file; Not required as ".metalink" file can be specified as a source and autodetected' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 4
          )
        ) ,
        'server-stat-if' => array (
          'sr_OduxVarName'    => 'sr_ServerStatsInputFile' ,
          'sr_Title'          => 'Server statistics input file' , 
          'sr_Descr'          => 'Specify the file path and name from which performance profile of the servers is loaded' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 16 ,
            'nm_Task_Ftp'      => 16 ,
            'nm_Task_Sftp'     => 16 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'server-stat-timeout'   => array (
          'sr_OduxVarName'    => 'nm_ServerStatsTtl' ,
          'sr_Title'          => 'Server timeout for statistics' ,
          'sr_Descr'          => 'Specifies timeout in seconds to invalidate performance profile of the servers since the last contact to them' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 86400 ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'enable-rpc' => array (
          'sr_OduxVarName'    => 'bl_RpcEnable' ,
          'sr_Title'          => 'Enable RPC interface' , 
          'sr_Descr'          => 'Enable JSON-RPC/XML-RPC server.' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-allow-origin-all' => array (
          'sr_OduxVarName'    => 'bl_RpcConnOriginAllAllow' ,
          'sr_Title'          => 'Allow RPC connects from all origins' , 
          'sr_Descr'          => 'Add Access-Control-Allow-Origin header field with value "*" to the RPC response' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-certificate' => array (
          'sr_OduxVarName'    => 'sr_RpcServerCertFile' ,
          'sr_Title'          => 'RPC server certificate file' , 
          'sr_Descr'          => 'RPC server certificate file in PKCS12 (certificate, key and optional chain of additional certificates; with a blank import password) or PEM (separately specify private key) formats' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-listen-all' => array (
          'sr_OduxVarName'    => 'bl_RpcListenInterfaceAll' ,
          'sr_Title'          => 'Listen for RPC requests on all interfaces' , 
          'sr_Descr'          => 'Listen for RPC requests on all network interfaces; If not, listen only on local loopback interface' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-listen-port' => array (
          'sr_OduxVarName'    => 'nm_RpcListenPort' ,
          'sr_Title'          => 'RPC server listen port' , 
          'sr_Descr'          => 'Port number for RPC server to listen to; Allowed 1024 ~ 65535' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 6800 ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-max-request-size' => array (
          'sr_OduxVarName'    => 'sr_RpcRequestSize_Ceiling' ,
          'sr_Title'          => 'Max size of RPC request' , 
          'sr_Descr'          => 'Max size of RPC request; Drop connection if request size is more than specified' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '2M' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-passwd' => array (
          'sr_OduxVarName'    => 'sr_RpcAuthPass' ,
          'sr_Title'          => 'RPC password' , 
          'sr_Descr'          => 'Password to access RPC interface (deprecated in favour of RPC auth token)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-private-key' => array (
          'sr_OduxVarName'    => 'sr_RpcServerPrivKeyFile' ,
          'sr_Title'          => 'RPC server private key file' , 
          'sr_Descr'          => 'RPC server private key file in decrypted PEM format' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-secret' => array (
          'sr_OduxVarName'    => 'sr_RpcAuthToken' ,
          'sr_Title'          => 'RPC secret authorization token' , 
          'sr_Descr'          => 'RPC secret authorization token' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-secure' => array (
          'sr_OduxVarName'    => 'bl_RpcSecureConnEnable' ,
          'sr_Title'          => 'Secure RPC' , 
          'sr_Descr'          => 'Encrypt RPC transport by SSL/TLS; Specify  server certificate and private key and private key separately' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rpc-user' => array (
          'sr_OduxVarName'    => 'sr_RpcAuthUser' ,
          'sr_Title'          => 'RPC username' , 
          'sr_Descr'          => 'Username to access RPC interface (deprecated in favour of RPC auth token)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'async-dns-server' => array (
          'sr_OduxVarName'    => 'sr_AsyncDnsServerList' ,
          'sr_Title'          => 'DNS server(s) for asynchronous DNS' , 
          'sr_Descr'          => 'Comma separated list of DNS server address used in asynchronous DNS resolver instead of usual list from /etc/resolv.conf' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'auto-save-interval' => array (
          'sr_OduxVarName'    => 'nm_ControlFileSaveInterval' ,
          'sr_Title'          => 'Control file save interval' , 
          'sr_Descr'          => 'Interval to save control file (*.aria2); Allowed 0 ~ 600; 0 ⇒ save control file when task stops' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 28 ,
            'nm_Task_Http'     => 28 ,
            'nm_Task_Ftp'      => 28 ,
            'nm_Task_Sftp'     => 28 ,
            'nm_Task_Bt'       => 28 ,
            'nm_Task_Metalink' => 28
          )
        ) ,
        'conf-path' => array (
          'sr_OduxVarName'    => 'sr_ConfigFilePath' ,
          'sr_Title'          => 'Configuration file' , 
          'sr_Descr'          => 'Configuration file path' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '$HOME/.aria2/aria2.conf or $XDG_CONFIG_HOME/aria2/aria2.conf' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'console-log-level' => array (
          'sr_OduxVarName'    => 'sr_ConsoleLogVerbosity' ,
          'sr_Title'          => 'Console log verbosity' , 
          'sr_Descr'          => 'Set log level to output to console' ,
          'ar_AltValueList'   => array (
            'error'  => 'Error' ,
            'warn'   => 'Warn' ,
            'notice' => 'Notice' ,
            'info'   => 'Info' ,
            'debug'  => 'Debug'
          ) ,
          'sr_DefaultVal'     => 'Notice' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'daemon' => array (
          'sr_OduxVarName'    => 'bl_DaemonEnable' ,
          'sr_Title'          => 'Daemon mode' , 
          'sr_Descr'          => 'Run as daemon; Current working directory will be changed to "/"; Standard input, standard output and standard error will be redirected to "/dev/null"' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'deferred-input' => array (
          'sr_OduxVarName'    => 'bl_InputDeferEnable' ,
          'sr_Title'          => 'Deferred input' , 
          'sr_Descr'          => 'Prevent reading all sources and options from the input file at startup' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'input-file' => array (
          'sr_OduxVarName'    => 'sr_InputFilePath' ,
          'sr_Title'          => 'Input file' , 
          'sr_Descr'          => 'Path to input file containing download sources and options' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 4 ,
            'nm_Task_Ftp'      => 4 ,
            'nm_Task_Sftp'     => 4 ,
            'nm_Task_Bt'       => 4 ,
            'nm_Task_Metalink' => 4
          )
        ) ,
        'disable-ipv6' => array (
          'sr_OduxVarName'    => 'bl_Ipv6Disable' ,
          'sr_Title'          => 'Disable IPv6' , 
          'sr_Descr'          => 'Disable IPv6 support at app level' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'disk-cache' => array (
          'sr_OduxVarName'    => 'sr_DiskCacheSize' ,
          'sr_Title'          => 'Disk cache size' , 
          'sr_Descr'          => 'Enable disk cache in memory. 0 ⇒ disabled.' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => '16M' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'rlimit-nofile' => array (
          'sr_OduxVarName'    => 'nm_OpenFileCount_SoftCeiling' ,
          'sr_Title'          => 'Soft limit of open file descriptors' , 
          'sr_Descr'          => 'Soft limit of open file descriptors; Available only if system supports "rlimit" API, does not exceed hard limit or, is not lower than current soft limit' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'enable-color' => array (
          'sr_OduxVarName'    => 'bl_ConsoleColourEnable' ,
          'sr_Title'          => 'Enable colour terminal' , 
          'sr_Descr'          => 'Enable color output for a terminal' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'event-poll' => array (
          'sr_OduxVarName'    => 'sr_EventPollMethod' ,
          'sr_Title'          => 'Event polling method' , 
          'sr_Descr'          => 'Specify the method for polling events; Availability of options dpends on system' ,
          'ar_AltValueList'   => array (
            'epoll'  => 'EPoll' ,
            'kqueue' => 'KQueue' ,
            'port'   => 'Port' ,
            'poll'   => 'Poll' ,
            'select' => 'Select'
          ) ,
          'sr_DefaultVal'     => 'Default may vary depending on base system' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'human-readable' => array (
          'sr_OduxVarName'    => 'bl_ConsoleHumaniseByteEnable' ,
          'sr_Title'          => 'Human readable sizes in console output' , 
          'sr_Descr'          => 'Print sizes and speed in human readable format (e.g., 1.2Ki, 3.4Mi) in the console readout' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'interface' => array (
          'sr_OduxVarName'    => 'sr_SocketBindInterface' ,
          'sr_Title'          => 'Bind socket to interface/IP/host' , 
          'sr_Descr'          => 'Bind sockets to given interface; Interface, IP address or host name' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'min-tls-version' => array (
          'sr_OduxVarName'    => 'sr_TlsVersion_Floor' ,
          'sr_Title'          => 'Minimum SSL/TLS version' , 
          'sr_Descr'          => 'Specify minimum SSL/TLS version to enable' ,
          'ar_AltValueList'   => array (
            'SSLv3'   => 'SSL v3' ,
            'TLSv1'   => 'TLS v1' ,
            'TLSv1.1' => 'TLS v1.1' ,
            'TLSv1.2' => 'TLS v1.2'
          ) ,
          'sr_DefaultVal'     => 'TLS v1' ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'on-download-complete' => array (
          'sr_OduxVarName'    => 'sr_TaskComplete_Cmd' ,
          'sr_Title'          => 'Command on task complete' , 
          'sr_Descr'          => 'Command to be executed after download completed (after seeding for BitTorrent)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'on-download-error' => array (
          'sr_OduxVarName'    => 'sr_TaskError_Cmd' ,
          'sr_Title'          => 'Command on task error' , 
          'sr_Descr'          => 'Command to be executed after download aborted on error' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'on-download-pause' => array (
          'sr_OduxVarName'    => 'sr_TaskPause_Cmd' ,
          'sr_Title'          => 'Command on task pause' , 
          'sr_Descr'          => 'Command to be executed after download paused' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'on-download-start' => array (
          'sr_OduxVarName'    => 'sr_TaskStart_Cmd' ,
          'sr_Title'          => 'Command on task start' , 
          'sr_Descr'          => 'Command to be executed after download started' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'on-download-stop' => array (
          'sr_OduxVarName'    => 'sr_TaskStop_Cmd' ,
          'sr_Title'          => 'Command on task stopped' , 
          'sr_Descr'          => 'Command to be executed after download stopped (includes complete and error states)' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 30 ,
            'nm_Task_Ftp'      => 30 ,
            'nm_Task_Sftp'     => 30 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 30
          )
        ) ,
        'on-bt-download-complete' => array (
          'sr_OduxVarName'    => 'sr_BtDownComplete_Cmd' ,
          'sr_Title'          => 'Command on BitTorrent download complete' , 
          'sr_Descr'          => 'Command to be executed after BitTorrent download completed but before seeding complete' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 30 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'show-console-readout' => array (
          'sr_OduxVarName'    => 'bl_ConsoleOutputDisplayEnable' ,
          'sr_Title'          => 'Display console output' , 
          'sr_Descr'          => 'Show console readout' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'summary-interval' => array (
          'sr_OduxVarName'    => 'nm_SummaryRefresh_Duration' ,
          'sr_Title'          => 'Summary refresh duration' , 
          'sr_Descr'          => 'Set interval (seconds) to output download progress summary; 0 ⇒ suppress output' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'no-conf' => array (
          'sr_OduxVarName'    => 'bl_ConfigFileDisable' ,
          'sr_Title'          => 'Disable configuration file' , 
          'sr_Descr'          => 'Disable loading "aria2.conf" file' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'quiet' => array (
          'sr_OduxVarName'    => 'bl_ConsoleDisable' ,
          'sr_Title'          => 'Disable console output' , 
          'sr_Descr'          => 'no console output' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'save-session-interval'               => array (
          'sr_OduxVarName'    => 'nm_SessionSaveInterval' ,
          'sr_Title'          => 'Session save interval' ,
          'sr_Descr'          => 'Interval (seconds) to save error/unfinished downloads to file specified separately; 0 ⇒ save on exit' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'stop' => array (
          'sr_OduxVarName'    => 'nm_ExitDelay' ,
          'sr_Title'          => 'Exit daemon after duration' , 
          'sr_Descr'          => 'Stop application after specified delay (seconds); 0 ⇒ disabled' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'stop-with-process' => array (
          'sr_OduxVarName'    => 'nm_ExitWithPid' ,
          'sr_Title'          => 'Exit daemon with process PID' , 
          'sr_Descr'          => 'PID of application which, when not running, causes downloader to exit' ,
          'ar_AltValueList'   => NULL ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'truncate-console-readout' => array (
          'sr_OduxVarName'    => 'bl_ConsoleOutputTruncate_Enable' ,
          'sr_Title'          => 'Fit console output in a single line' , 
          'sr_Descr'          => 'Truncate console readout to fit in a single line' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 16 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 0 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-max-open-files'          => array (
          'sr_OduxVarName'    => 'nm_BtOpenFileCount_Ceiling' ,
          'sr_Title'          => 'Maximum open files' ,
          'sr_Descr'          => 'Specify maximum number of files to open in multi-file BitTorrent/Metalink download globally' ,
          'sr_DefaultVal'     => 100 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 24 ,
            'nm_Task_Metalink' => 24
          )
        ) ,
        'bt-enable-lpd'             => array (
          'sr_OduxVarName'    => 'bl_Lpd_Enable' ,
          'sr_Title'          => 'Enable local peer discovery' ,
          'sr_Descr'          => 'Enable discovery of peers in the local network; ignored if torrent is private' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-exclude-tracker'        => array (
          'sr_OduxVarName'    => 'sr_BtTrackerBlacklist' ,
          'sr_Title'          => 'Exclude trackers' ,
          'sr_Descr'          => 'Comma separated list of BitTorrent tracker announce URIs to remove; "*" ⇒ all URIs,' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-external-ip'            => array (
          'sr_OduxVarName'    => 'sr_BtSetLocalIp' ,
          'sr_Title'          => 'External IP' ,
          'sr_Descr'          => 'IP address to use in BitTorrent download and DHT (may be sent to tracker); set to local node for DHT (in a private network)' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-force-encryption'       => array (
          'sr_OduxVarName'    => 'bl_BtForceEncrypt_Enable' ,
          'sr_Title'          => 'Force encryption' ,
          'sr_Descr'          => 'Deny legacy BitTorrent handshake and only use Obfuscation handshake and always encrypt message payload with ARC4' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-hash-check-seed'         => array (
          'sr_OduxVarName'    => 'bl_BtSeedAfterHashcheck_Enable' ,
          'sr_Title'          => 'Continue to seed after hash check' ,
          'sr_Descr'          => 'Continue to seed torrent after hash check' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-max-peers'              => array (
          'sr_OduxVarName'    => 'nm_BtPeerCount_Ceiling' ,
          'sr_Title'          => 'Maximum peers' ,
          'sr_Descr'          => 'Maximum number of peers per torrent. 0 ⇒ unlimited' ,
          'sr_DefaultVal'     => 55 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-metadata-only'          => array (
          'sr_OduxVarName'    => 'bl_BtMetadataOnly_Enable' ,
          'sr_Title'          => 'Metadata only' ,
          'sr_Descr'          => 'Download meta data only; not the file(s) described in meta data (only for BitTorrent Magnet URI)' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-min-crypto-level'        => array (
          'sr_OduxVarName'    => 'sr_BtCryptoLevel_Floor' ,
          'sr_Title'          => 'Minimum encryption level' ,
          'sr_Descr'          => 'Minimum level of encryption method (even if peer accepts higher levels)' ,
          'ar_AltValueList'   => array (
            'plain' => 'Plain' ,
            'arc4'  => 'ARC4'
          ) ,
          'sr_DefaultVal'     => 'plain' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-prioritize-piece'       => array (
          'sr_OduxVarName'    => 'sr_BtPrioritisePiece' ,
          'sr_Title'          => 'Prioritise piece' ,
          'sr_Descr'          => 'Prioritise specified pieces of each file for download; can contain "head" (from beginning) and/or "tail" (from end) parameters separated by comma; values for these specify the size to prioritise (can include binary scale multipliers like K or M); If only parameter specified with now value, 1M is used' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-remove-unselected-file'  => array (
          'sr_OduxVarName'    => 'bl_BtRemoveIgnoredFile_Enable' ,
          'sr_Title'          => 'Remove unselected file(s)' ,
          'sr_Descr'          => 'Delete the unselected files from disk when BitTorrent task complete (use with caution)' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-request-peer-speed-limit' => array (
          'sr_OduxVarName'    => 'sr_BtRequestPeerAtSpeed_Floor' ,
          'sr_Title'          => 'Request peer speed limit' ,
          'sr_Descr'          => 'Temporarily increase the number of peers to try to increase download speed to match specified value (can include binary scale multipliers like K or M)' ,
          'sr_DefaultVal'     => '50K' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-require-crypto'         => array (
          'sr_OduxVarName'    => 'bl_BtRequireCrypto_Enable' ,
          'sr_Title'          => 'Require encryption' ,
          'sr_Descr'          => 'Enforce Obfuscation handshake and reject connection with legacy BitTorrent handshake (\19BitTorrent protocol)' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-save-metadata'          => array (
          'sr_OduxVarName'    => 'bl_BtSaveMagnetMetadata_Enable' ,
          'sr_Title'          => 'Save metadata' ,
          'sr_Descr'          => 'Save Magnet URI metadata as file (hex encoded info hash with suffix ".torrent") in the same directory where download file is saved (does not overwrite existing file)' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-seed-unverified'        => array (
          'sr_OduxVarName'    => 'bl_BtSeedUnverified_Enable' ,
          'sr_Title'          => 'Seed unverified' ,
          'sr_Descr'          => 'Seed previously downloaded files without verifying piece hashes' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-stop-timeout'      => array (
          'sr_OduxVarName'    => 'nm_BtStopTimeout' ,
          'sr_Title'          => 'Stop after timeout' ,
          'sr_Descr'          => 'Duration after which BitTorrent download should stop if download speed is 0; 0 ⇒ disabled' ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-tracker' => array (
          'sr_OduxVarName'    => 'sr_BtTracker' ,
          'sr_Title'          => 'Additional tracker URIs' ,
          'sr_Descr'          => 'Comma separated list of additional BitTorrent tracker announce URIs' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-tracker-connect-timeout' => array (
          'sr_OduxVarName'    => 'nm_BtTrackerConnTimeout' ,
          'sr_Title'          => 'Tracker connection timeout' ,
          'sr_Descr'          => 'Connect timeout in seconds to establish connection to tracker' ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-tracker-interval'       => array (
          'sr_OduxVarName'    => 'nm_BtTrackererRequestInterval' ,
          'sr_Title'          => 'Tracker requests interval' ,
          'sr_Descr'          => 'Interval (sec) between tracker requests (ignores value in the response of tracker). 0 ⇒ based on tracker response and download progress' ,
          'sr_DefaultVal'     => 0 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-tracker-timeout'        => array (
          'sr_OduxVarName'    => 'nm_BtTrackerTimeout' ,
          'sr_Title'          => 'Tracker timeout' ,
          'sr_Descr'          => 'Tracker timeout (sec)' ,
          'sr_DefaultVal'     => 60 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'enable-peer-exchange'    => array (
          'sr_OduxVarName'    => 'bl_BtPeerExchange_Enable' ,
          'sr_Title'          => 'Enable peer exchange' ,
          'sr_Descr'          => 'Enable Peer Exchange extension (disabled for private download)' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'follow-torrent'         => array (
          'sr_OduxVarName'    => 'sr_BtTorrentFollow_Enable' ,
          'sr_Title'          => 'Follow torrent' ,
          'sr_Descr'          => 'Create subtasks for contents of the provided torrent file task; If no, treat the torrent file as the task; If yes, specify if the torrent file should be saved to disk or held in memory' ,
          'ar_AltValueList'   => array (
            'true'  => 'True' ,
            'false' => 'False' ,
            'mem'   => 'Memory'
          ) ,
          'sr_DefaultVal'     => 'True' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'seed-ratio'             => array (
          'sr_OduxVarName'    => 'sr_BtSeedRatio_Ceiling' ,
          'sr_Title'          => 'Seed ratio' ,
          'sr_Descr'          => 'Maximum share ratio for completed torrents; 0.0 ⇒ unlimited; seeding stops if seeding time limit is achieved' ,
          'sr_DefaultVal'     => '1.0' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'seed-time'              => array (
          'sr_OduxVarName'    => 'sr_BtSeedDuration_Ceiling' ,
          'sr_Title'          => 'Maximum seed duration' ,
          'sr_Descr'          => 'Maximum seeding time (fractional minutes)' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-detach-seed-only' => array (
          'sr_OduxVarName'    => 'bl_BtDontCountSeeding_Enable' ,
          'sr_Title'          => 'Exclude seeding from active count' ,
          'sr_Descr'          => 'Exclude seed only downloads when counting concurrent active downloads' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => FALSE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 24 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'bt-lpd-interface' => array (
          'sr_OduxVarName'    => 'sr_BtLpdInterface' ,
          'sr_Title'          => 'Interface for Local Peer Discovery' ,
          'sr_Descr'          => 'Interface (and/or IP address) for Local Peer Discovery; <blank> ⇒ default interface' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-entry-point' => array (
          'sr_OduxVarName'    => 'sr_BtDhtEntryIpv4' ,
          'sr_Title'          => 'DHT entry point (IPv4)' ,
          'sr_Descr'          => 'Host and port as an entry point to IPv4 DHT network' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-entry-point6' => array (
          'sr_OduxVarName'    => 'sr_BtDhtEntryIpv6' ,
          'sr_Title'          => 'DHT entry point (IPv6)' ,
          'sr_Descr'          => 'Host and port as an entry point to IPv6 DHT network' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-file-path' => array (
          'sr_OduxVarName'    => 'sr_BtDhtRouteIpv4TablePath' ,
          'sr_Title'          => 'DHT routing table file (IPv4)' ,
          'sr_Descr'          => 'IPv4 DHT routing table file path' ,
          'sr_DefaultVal'     => '$HOME/.aria2/dht.dat or $XDG_CACHE_HOME/aria2/dht.dat' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-file-path6' => array (
          'sr_OduxVarName'    => 'sr_BtDhtRouteIpv6TablePath' ,
          'sr_Title'          => 'DHT routing table file (IPv6)' ,
          'sr_Descr'          => 'IPv6 DHT routing table file path' ,
          'sr_DefaultVal'     => '$HOME/.aria2/dht6.dat or $XDG_CACHE_HOME/aria2/dht6.dat' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-listen-addr6' => array (
          'sr_OduxVarName'    => 'sr_BtDhtBindAddrIpv6' ,
          'sr_Title'          => 'DHT bind address (IPv6)' ,
          'sr_Descr'          => 'Global unicast IPv6 address to bind socket for IPv6 DHT' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-listen-port' => array (
          'sr_OduxVarName'    => 'sr_BtDhtListenPort' ,
          'sr_Title'          => 'DHT/UDP listen port(s)' ,
          'sr_Descr'          => 'UDP listening port used by DHT(IPv4, IPv6) and UDP tracker; Multiple values allowed (separated by "," or "-" for a range or a combination of both)' ,
          'sr_DefaultVal'     => '6881-6999' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dht-message-timeout' => array (
          'sr_OduxVarName'    => 'nm_BtDhtMesgTimeout' ,
          'sr_Title'          => 'DHT message timeout' ,
          'sr_Descr'          => 'DHT message timeout (sec)' ,
          'sr_DefaultVal'     => 10 ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'enable-dht' => array (
          'sr_OduxVarName'    => 'bl_BtDhtIPv4_Enable' ,
          'sr_Title'          => 'Enable DHT (IPv4)' ,
          'sr_Descr'          => 'Enable IPv4 DHT and UDP tracker support; ignored for private torrent' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => TRUE ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'enable-dht6' => array (
          'sr_OduxVarName'    => 'bl_BtDhtIpv6_Enable' ,
          'sr_Title' => 'Enable DHT (IPv6)' ,
          'sr_Descr'          => 'Enable IPv6 DHT; ignored for private torrent' ,
          'ar_AltValueList'   => array (
            'true'  => TRUE ,
            'false' => FALSE
          ) ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 30 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'listen-port' => array (
          'sr_OduxVarName'    => 'sr_BtListenPort' ,
          'sr_Title'          => 'BitTorrent TCP listen port(s)' ,
          'sr_Descr'          => 'TCP listen port number for BitTorrent downloads; Multiple values allowed (separated by "," or "-" for a range or a combination of both)' ,
          'sr_DefaultVal'     => '6881-6999' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'peer-id-prefix' => array (
          'sr_OduxVarName'    => 'sr_BtPeerIdPrefix' ,
          'sr_Title'          => 'Peer ID prefix' ,
          'sr_Descr'          => 'Prefix of peer ID; either truncated to include only first 20 bytes or random byte data added to make length of 20 bytes' ,
          'sr_DefaultVal'     => 'A2-$MAJOR-$MINOR-$PATCH-' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ) ,
        'dscp' => array (
          'sr_OduxVarName'    => 'sr_BtDscp' ,
          'sr_Title'          => 'DSCP for outgoing BitTorrent traffic' , 
          'sr_Descr'          => 'DSCP (only DSCP bits in TOS field of IP packets) value in outgoing IP packets of BitTorrent traffic for QoS; for values from /usr/include/netinet/ip.h divide them by 4 (otherwise values would be incorrect, e.g. your CS1 class would turn into CS4)' ,
          'sr_DefaultVal'     => NULL ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        ),
        'help'         => array (
          'sr_OduxVarName'    => 'sr_Help' ,
          'sr_Title'          => 'Help messages' ,
          'sr_Descr'          => 'Help messages, classified with tags; Available Values: #basic, #advanced, #http, #https, #ftp, #metalink, #bittorrent, #cookie, #hook, #file, #rpc, #checksum, #experimental, #deprecated, #help, #all' ,
          'sr_DefaultVal'     => '#basic' ,
          'ar_Scope'          => array (
            'nm_App'           => 0 ,
            'nm_Task_Http'     => 0 ,
            'nm_Task_Ftp'      => 0 ,
            'nm_Task_Sftp'     => 0 ,
            'nm_Task_Bt'       => 16 ,
            'nm_Task_Metalink' => 0
          )
        )
  ) ;
?>