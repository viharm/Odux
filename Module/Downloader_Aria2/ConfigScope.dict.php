<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * Global static = Global options that cannot be changed once the application (Aria2) has started
  * Global dynamic = Global options that can be changed whilst the application is running
  * Task static = Options for a download task that cannot be changed once the application has started, even if paused.
  * Task dynamic = Options for a download task that can be changed once the application has started, but task has to be paused.
  * Task active = Options for a download task that can be changed whilst downloading.
  *
  *  Value  |   Global static  |  Global dynamic  |   Task static    |   Task dynamic   |   Task active    |
  *     00  |                  |                  |                  |                  |                  |
  *     01  |                  |                  |                  |                  |         1        |
  *     02  |                  |                  |                  |         1        |                  |
  *     03  |                  |                  |                  |         1        |         1        |
  * ->  04  |                  |                  |         1        |                  |                  |
  *     05  |                  |                  |         1        |                  |         1        |
  * ->  06  |                  |                  |         1        |         1        |                  |
  * ->  07  |                  |                  |         1        |         1        |         1        |
  *     08  |                  |         1        |                  |                  |                  |
  *     09  |                  |         1        |                  |                  |         1        |
  *     10  |                  |         1        |                  |         1        |                  |
  *     11  |                  |         1        |                  |         1        |         1        |
  *     12  |                  |         1        |         1        |                  |                  |
  *     13  |                  |         1        |         1        |                  |         1        |
  *     14  |                  |         1        |         1        |         1        |                  |
  *     15  |                  |         1        |         1        |         1        |         1        |
  * ->  16  |         1        |                  |                  |                  |                  |
  *     17  |         1        |                  |                  |                  |         1        |
  *     18  |         1        |                  |                  |         1        |                  |
  *     19  |         1        |                  |                  |         1        |         1        |
  * ->  20  |         1        |                  |         1        |                  |                  |
  *     21  |         1        |                  |         1        |                  |         1        |
  *     22  |         1        |                  |         1        |         1        |                  |
  *     23  |         1        |                  |         1        |         1        |         1        |
  * ->  24  |         1        |         1        |                  |                  |                  |
  *     25  |         1        |         1        |                  |                  |         1        |
  *     26  |         1        |         1        |                  |         1        |                  |
  *     27  |         1        |         1        |                  |         1        |         1        |
  * ->  28  |         1        |         1        |         1        |                  |                  |
  *     29  |         1        |         1        |         1        |                  |         1        |
  * ->  30  |         1        |         1        |         1        |         1        |                  |
  * ->  31  |         1        |         1        |         1        |         1        |         1        |
  */
  $ar_Downloader_ConfigScope = array (
    'ar_Global_Static'  => array ( 16 , 17 , 18 , 19 , 20 , 21 , 22 , 23 , 24 , 25 , 26 , 27 , 28 , 29 , 30 , 31 ) ,
    'ar_Global_Dynamic' => array ( 8 , 9 , 10 , 11 , 12 , 13 , 14 , 15 , 24 , 25 , 26 , 27 , 28 , 29 , 30 , 31 ) ,
    'ar_Task_Static'    => array ( 4 , 5 , 6 , 7 , 12 , 13 , 14 , 15 , 20 , 21 , 22 , 23 , 28 , 29 , 30 , 31 ) ,
    'ar_Task_Dynamic'   => array ( 2 , 3 , 6 , 7 , 10 , 11 , 14 , 15 , 18 , 19 , 22 , 23 , 26 , 27 , 30 , 31 ) ,
    'ar_Task_Active'    => array ( 1 , 3 , 5 , 7 , 9 , 11 , 13 , 15 , 17 , 19 , 21 , 23 , 25 , 27 , 29 , 31 )
  ) ;
?>