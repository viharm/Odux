<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include bootstrap script
  require_once('Bootstrap.php') ;
  
  // Set page title
  $ar_Page['sr_Title']  = 'Overview' ;
  
  @fn_Debug ( 'Summarising variables before processing' ) ;
  @fn_Debug ( 'Site', $ar_Site ) ;
  @fn_Debug ( 'App' , $ar_App ) ;
  @fn_Debug ( 'UI' , $ar_Ui ) ;
  @fn_Debug ( 'Page' , $ar_Page ) ;
  
  // Check if downloader class exists
  @fn_Debug ('Checking if downloader class exists') ;
  if ( class_exists('cl_Downloader') ) {
    @fn_Debug ('Downloader class exists') ;
    // Create the downloader object
    $ob_Downloader = new cl_Downloader($ar_App['ar_Downloader']['ar_Conn']['ar_Config']) ;
    @fn_Debug ( 'Created downloader object' , $ob_Downloader ) ;


    // Check if connection to downloader has been established
    if ( $ob_Downloader->bl_ConnStatus === TRUE ) {
      @fn_Debug ( 'Connected to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      // Set working variable for connection status
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = TRUE ;
      
      
      // Process any commands, to include updates and changes in the statuses
      if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
        if ( strstr ( $_POST['bt_DownloaderControl'] , 'SummariseTask' ) ) {
          header ( 'Location: Task_Summary.php?sr_DownloaderInstruction=' . $_POST['bt_DownloaderControl'] ) ;
        } // Task detail
        else {
          $ar_Ui['ar_Status']['sr_Mesg'] = fn_Action ( $ob_Downloader , $_POST , 'Downloader' ) ; 
        } // Catch all
      }

      // Get list of tasks
      fn_Debug ( 'Fetching list of tasks' ) ;
      $ar_Ui['ar_Downloader']['ar_Task'] = array (
        'ar_Active'  => cl_Translator::fn_Translate (
          $ob_Downloader->SummariseActive() ,
          'Downloader' ,
          'TaskDetail'
        ) ,
        'ar_Queued' => cl_Translator::fn_Translate (
          $ob_Downloader->SummariseQueued ( 0 , 1000 ) ,
          'Downloader' ,
          'TaskDetail'
        ) ,
        'ar_Stopped' => cl_Translator::fn_Translate (
          $ob_Downloader->SummariseStopped ( 0 , 1000 ) ,
          'Downloader' ,
          'TaskDetail'
        )
      ) ;
  
      // Humanise size and speed units
      foreach ( $ar_Ui['ar_Downloader']['ar_Task'] as $sr_TaskTypeKey=>$ar_TaskTypeList ) {
        foreach ( $ar_TaskTypeList as $nm_TaskKey=>$ar_Task ) {
           $ar_Ui['ar_Downloader']['ar_Task'][$sr_TaskTypeKey][$nm_TaskKey]['sr_TaskSizeCompleted'] = fn_HumaniseBytes ( floatval($ar_Task['nm_TaskSizeCompleted']) ) ; 
           $ar_Ui['ar_Downloader']['ar_Task'][$sr_TaskTypeKey][$nm_TaskKey]['sr_TaskSizeTotal'] = fn_HumaniseBytes ( floatval($ar_Task['nm_TaskSizeTotal']) ) ;
           $ar_Ui['ar_Downloader']['ar_Task'][$sr_TaskTypeKey][$nm_TaskKey]['sr_TaskSpeedDown'] = fn_HumaniseBytes ( floatval($ar_Task['nm_TaskSpeedDown']) , '/s' ) ;
           $ar_Ui['ar_Downloader']['ar_Task'][$sr_TaskTypeKey][$nm_TaskKey]['sr_TaskSpeedUp'] = fn_HumaniseBytes ( floatval($ar_Task['nm_TaskSpeedUp']) , '/s' ) ;
        }
      }
      
      fn_CollectGlobalStat() ;
    } // Connection to downloader established
    else {
      @fn_Debug ( 'Could not connect to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
    } // No connection to downloader
  } // class exists
  else {
    @fn_Debug ('Downloader class does not exist') ;
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
  } // class does not exist
  
  @fn_Debug ( 'Summarising variables after processing' ) ;
  @fn_Debug ( 'Site', $ar_Site ) ;
  @fn_Debug ( 'App' , $ar_App ) ;
  @fn_Debug ( 'UI' , $ar_Ui ) ;
  @fn_Debug ( 'Page' , $ar_Page ) ;

  // Interface logic
  // Check if interface is text based
  if ( $ar_Ui['bl_Cli'] ) {
    @fn_Debug('UI is CLI') ;
  } // UI is CLI
  else {
    @fn_Debug('UI is non-CLI') ;
    $ob_Ui = new cl_WebUi() ;
  } // UI is non-CLI

  // Transfer variables to the UI
  fn_Debug ( 'Transfer variables to UI' ) ;
  $ob_Ui->TransferVar ( 'ar_Site' , $ar_Site ) ;
  $ob_Ui->TransferVar ( 'ar_App' , $ar_App ) ;
  $ob_Ui->TransferVar ( 'ar_Ui' , $ar_Ui ) ;
  $ob_Ui->TransferVar ( 'ar_Page' , $ar_Page ) ;

  // Render UI
  fn_Debug ('Rendering UI') ;
  $ob_Ui->Render('HtmlStart.tpl') ;
  $ob_Ui->Render('DocHead.tpl') ;
  $ob_Ui->Render('DocBodyStart.tpl') ;
  $ob_Ui->Render('PageNav.tpl') ;
  $ob_Ui->Render('PageBody_Overview.tpl') ;
  $ob_Ui->Render('PageFooter.tpl') ;
  $ob_Ui->Render('DocBodyEnd.tpl') ;
  $ob_Ui->Render('HtmlEnd.tpl') ;
?>