# Odux

|             |                                                          |
|:------------|:---------------------------------------------------------|
| Version     | 02.00.00 beta                                            |
| Download    | https://gitlab.com/viharm/Odux/tags                      |
| Repository  | https://gitlab.com/viharm/Odux.git                       |
| Issues      | https://gitlab.com/viharm/Odux/issues                    |
| License     | Apache License 2.0                                       |
| Language    | PHP                                                      |

*Odux* is a modular interface app for downloader daemons. It is a rewrite of the old *Odi* app which was specifically written to create a web interface for *Aria2*.


## Installation


### Pre-requisites

*  *Aria2*
*  Web server
*  *PHP*
*  *Smarty*


### Download


#### Files archive

Get the release archives from the link at the top of this page


#### Clone

Clone the *Git* repository from the location shown at the top of this page. Ensure that submodules are also cloned.

```
git clone --recurse-submodules ...
```

Visit https://www.git-scm.com/docs/git-clone for help with this.


### Deploy

Save the downloaded directory structure in your choice of path within your application. Please ensure that this location is reachable by the web server and has the appropriate permissions.


## Configuration

*Odux* ships with reasonable defaults; however it requires some basic configuration for the downloader and the UI modules to work.

All path references are relative to the install location.


### Odux configuration

Create a custom configuration (`Config/Odux.custom.config.php`) file to set the following optional parameters
*  Site
  *  Name
  *  Owner
  *  Contact
*  App
  *  Name
  *  Description
  *  Author

```php
<?php
  $ar_Site = array_replace ( $ar_Site , array (
    'sr_Name'        => 'Parent site' ,
    'sr_Owner'       => 'Parent site owner/admin' ,
    'sr_Contact'     => 'webmaster@parent.site' ,
  ) ) ;

  $ar_App = array_replace ( $ar_App , array (
    'sr_Name'         => 'Custom webapp name' ,
    'sr_Descr'        => 'Site download manager' ,
    'sr_Author'       => 'Odux' ,
  ) ) ;
?>
```

### Modules


#### Downloader

The default package ships with *Aria2* capabiility.

Create a custom configuration (`Module/Downloader_Aria2/Downloader.custom.config.php`) file to set the following optional parameters
*  Host
*  Port
*  RPC secret

```php
<?php
  $ar_App['ar_Downloader']['ar_Conn']['ar_Config'] = array_replace ( $ar_App['ar_Downloader']['ar_Conn']['ar_Config'] , array (
    'host'      => 'downloader.host' ,
    'port'      => '6800' ,
    'rpcsecret' => 'secret' ,
  ) ) ;
?>
```


#### UI

The default package ships with web interface capabiility via *Smarty*. Ensure that *Smarty* is installed as per the recommended best practices (http://www.smarty.net/docs/en/installation.tpl).

Create a custom configuration (`Module/Ui_Web/Ui.custom.config.php`) file to set the path to the Smarty framework installation.

```php
<?php
  $sr_SmartyPath = '/var/www/Smarty/v3.1.30/' ;
?>
```

It is highly recommended that this location be outside of the web servers document root, for security reasons.


## Usage

Run the downloader daemon (e.g., *Aria2*)
Navigate to the install location


## Known limitations

See the current issues from the link shown at the top of this page


## Support

For issues, queries, suggestions and comments please create an issue/ticket at the issues link shown at the top of this page.

Debugging can be enabled in `Bootstrap.php` file
```
$GLOBALS['bl_DebugSwitch'] = TRUE ;
```

**Warning**: when debugging is enabled, the overall speed of the application is severely reduced due to extensive UI output.


## Contribute

Please feel free to clone/fork and contribute via pull requests. Donations also welcome at https://www.coinbase.com/viharm#tab2.

Please make contact for more information.


## Development environment

*  Server
  *  Debian Jessie
  *  Apache 2.4
  *  PHP 5.6
  *  Aria2 1.18
*  Odux modules
  *  Smarty 3.1
  *  phpAria2rpc 02.03.00
  *  phpKhelper 01.02.02
*  Client
  *  Firefox 50
  *  Chrome 55


## Licence

Licensed under the Apache License v2.0.

A copy of the licence is available...

* in the enclosed `LICENSE` file, or
* at https://www.apache.org/licenses/LICENSE-2.0.html


## Credits


### Tools


#### Kint

*Kint* debugging library (http://raveren.github.io/kint/), used under the MIT license

Copyright (c) 2013 Rokas Å leinius (raveren at gmail dot com)


### Utilities


#### Codiad

*Codiad* web based IDE (https://github.com/Codiad/Codiad), used under a MIT-style license.

Copyright (c) Codiad & Kent Safranski (codiad.com)


#### Ungit

*Ungit* web based interface for *Git* (https://github.com/FredrikNoren/ungit), used under the MIT license.

Copyright (C) 2013-2016 Fredrik Norén


### GitLab

Hosted by *GitLab* code repository (gitlab.com).