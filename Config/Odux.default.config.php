<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // This configuration file is called by the bootstrap file.

  // Dynamic configuration
  
  $ar_Site = array ( // TODO: to be deprecated ?
    'sr_Name'        => 'Odux' ,
    'sr_Owner'       => 'Parent site owner' ,
    'sr_Contact'     => 'webmaster@parent.site' ,
  ) ;
  
  $ar_App = array (
    'sr_Name'         => 'Odux' ,
    'sr_Descr'        => 'Interface for downloaders' ,
    'sr_Author'       => 'viharm' ,
    'ar_Path'         => array (
      'sr_Base' => realpath ( __DIR__ . DIRECTORY_SEPARATOR . '..' ) . DIRECTORY_SEPARATOR
    ) ,
    'sr_CharSet'      => 'UTF-8' ,
    'ar_ModuleList'   => array() ,
    'ar_ModuleActive' => array()
  ) ;

  $ar_Ui = array ( // TODO: Move subset of the Ui configuration to the UI modules
    'sr_Lang'  => 'en' ,
    'ar_Uri'   => array (
      'sr_Base' => $_SESSION['sr_BaseUri'] . '/' ,
      'ar_Js'   => array (
        'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'
      )
    ) ,
    'ar_Downloader' => array (
      'ar_Global' => array (
        'ar_Stat' => array(
          'nm_CountActive' => NULL ,
          'nm_CountWaiting' => NULL ,
          'nm_CountStopped' => NULL
        )
      )
    ) ,
    'bl_Debug' => (array_key_exists('bl_DebugSwitch',$GLOBALS))?$GLOBALS['bl_DebugSwitch']:FALSE ,
    'ar_Status' => array (
      'sr_Mesg' => NULL
    )
  ) ;
  

  // Static configuration, to add more variables
  
  $ar_App['ar_Path'] += [
    'sr_Config' => $ar_App['ar_Path']['sr_Base'] . 'Config' . DIRECTORY_SEPARATOR ,
    'sr_Inc'    => $ar_App['ar_Path']['sr_Base'] . 'Lib' . DIRECTORY_SEPARATOR . 'Inc' . DIRECTORY_SEPARATOR ,
    'sr_Module' => $ar_App['ar_Path']['sr_Base'] . 'Module' . DIRECTORY_SEPARATOR ,
    'sr_Art'    => $ar_App['ar_Path']['sr_Base'] . 'Lib' . DIRECTORY_SEPARATOR . 'Art' . DIRECTORY_SEPARATOR
  ] ;

  $ar_Ui['ar_Uri'] += [ // TODO: Move to UI modules ?
    'sr_Art' => $ar_Ui['ar_Uri']['sr_Base'] . 'Lib/Art/' ,
    'sr_Inc' => $ar_Ui['ar_Uri']['sr_Base'] . 'Lib/Inc/'
  ] ;
  
  $ar_App += [
    'ar_Art' => array (
      'ar_Favicon' => array (
        'sr_Uri' => $ar_Ui['ar_Uri']['sr_Art'] . 'Favicon.ico' ,
        'sr_Path' => $ar_App['ar_Path']['sr_Art'] . 'Favicon.ico'
      ) ,
      'ar_Banner'  => array (
        'sr_Uri'     => $ar_Ui['ar_Uri']['sr_Art'] . 'Banner.png' ,
        'sr_Path'    => $ar_App['ar_Path']['sr_Art'] . 'Banner.png' ,
        'sr_Title'   => $ar_App['sr_Name'] ,
        'sr_AltText' => $ar_App['sr_Name']
      )
    )
  ] ;
  
?>