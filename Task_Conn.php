<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include bootstrap script
  require_once('Bootstrap.php') ;
  
  // Set page title
  $ar_Page['sr_Title']  = 'Task connections' ;
  
  @fn_Debug ( 'Summarising variables before processing' ) ;
  @fn_Debug ( 'Site', $ar_Site ) ;
  @fn_Debug ( 'App' , $ar_App ) ;
  @fn_Debug ( 'UI' , $ar_Ui ) ;
  @fn_Debug ( 'Page' , $ar_Page ) ;

  // Check if interface is text based
  if ( $ar_Ui['bl_Cli'] ) {
    fn_Debug('UI is CLI') ;
  } // UI is CLI
  else {
    fn_Debug('UI is non-CLI') ;
    $ob_Ui = new cl_WebUi() ;
  } // UI is non-CLI
  
  // Create the downloader object
  $ob_Downloader = new cl_Downloader($ar_App['ar_Downloader']['ar_Conn']['ar_Config']) ;
  @fn_Debug ( 'Created downloader object' , $ob_Downloader ) ;
  // Check if connection to downloader has been established
  if ( $ob_Downloader->bl_ConnStatus === TRUE ) {
    @fn_Debug ( 'Connected to downloader' , $ob_Downloader->bl_ConnStatus ) ;
    // Set working variable for connection status
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = TRUE ;

    @fn_Debug ( 'Processing submitted data' ) ;
    $ar_SubmitData = array() ;
    @fn_Debug ( 'Checking request method' , $_SERVER['REQUEST_METHOD'] ) ;
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
      @fn_Debug ( 'Detected post method' , $_POST ) ;
      $ar_SubmitData += $_POST ;
    } // Post
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      @fn_Debug ( 'Detected get method' , $_GET ) ;
      $ar_SubmitData += $_GET ;
    } // Get
    @fn_Debug ( 'Combined submitted data' , $ar_SubmitData ) ;

    @fn_Debug ( 'Checking task reference' , $ar_SubmitData['sr_TaskRef'] ) ;
    if ( array_key_exists ( 'sr_TaskRef' , $ar_SubmitData ) and $ar_SubmitData['sr_TaskRef'] !== '' ) {
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = $ar_SubmitData['sr_TaskRef'] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Task reference available in submitted data
    else if ( array_key_exists ( 'sr_DownloaderInstruction' , $ar_SubmitData ) and $ar_SubmitData['sr_DownloaderInstruction'] !== '' ) {
      @fn_Debug ( 'Task reference not directly provided; extracting from sr_DownloaderInstruction' , $ar_SubmitData['sr_DownloaderInstruction'] ) ;
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = array_map ( 'fn_PurifyNumeric' , explode ( '__' , explode ( '__' , $ar_SubmitData['sr_DownloaderInstruction'] , 2 )[1] ) )[0] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Provided in sr_DownloaderInstruction
    else if ( array_key_exists ( 'bt_DownloaderControl' , $ar_SubmitData ) and $ar_SubmitData['bt_DownloaderControl'] !== '' ) {
      @fn_Debug ( 'Task reference not directly provided; extracting from bt_DownloaderControl' , $ar_SubmitData['bt_DownloaderControl'] ) ;
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = array_map ( 'fn_PurifyNumeric' , explode ( '__' , explode ( '__' , $ar_SubmitData['bt_DownloaderControl'] , 2 )[1] ) )[0] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Provided in bt_DownloaderControl
    else {
      @fn_Debug ( 'Task reference not available' ) ;
    } // Task reference not available in submitted data

    if ( ! is_null($ar_App['ar_Downloader']['ar_Task']['sr_TaskRef']) ) {
      
      @fn_Debug ( 'Task reference found' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['bl_Context'] = TRUE ;
      $ar_Ui['ar_Downloader']['ar_Task']['ar_Summary']['sr_TaskRef']['vr_Value'] = $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ;

      @fn_Debug ( 'Processing post command' ) ;
      if ( array_key_exists ( 'bt_DownloaderControl' , $ar_SubmitData ) ) {
        @fn_Debug ( 'bt_DownloaderControl detected' , $ar_SubmitData['bt_DownloaderControl'] ) ;
        $ar_SubmitData['sr_DownloaderInstruction'] = $ar_SubmitData['bt_DownloaderControl'] ;
        $ar_Ui['ar_Status']['sr_Mesg'] = fn_Action ( $ob_Downloader , $ar_SubmitData , 'Downloader' ) ;
      }
      else {
        @fn_Debug ( 'bt_DownloaderControl not detected' , $ar_SubmitData['bt_DownloaderControl'] ) ;
      }

      @fn_Debug ( 'Getting task Conns' ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'] =
        cl_Translator::fn_TranslateArray (
          fn_Action (
            $ob_Downloader ,
            array (
              'bt_DownloaderControl'=>'SummariseTaskConn__' .
              $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] .
              '__Basic'
            ) ,
            'Downloader'
          ) ,
          'Downloader' ,
          'TaskConn'
        )
      ;
      @fn_Debug ( 'Task Conns fetched' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'] ) ;


    // Humanise size and speed units
    foreach ( $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'] as $nm_TaskConnKey=>$ar_TaskConn ) {
      @fn_Debug ( 'Stepping in...' , $ar_TaskConn ) ;
      foreach ( $ar_TaskConn['ar_TaskConnConn'] as $nm_TaskConnConnkey=>$ar_TaskConnConn ) {
        @fn_Debug ( 'Augmenting connection entry' , $ar_TaskConnConn ) ;
        $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'][$nm_TaskConnKey]['ar_TaskConnConn'][$nm_TaskConnConnkey]['sr_TaskConnSpeedDown']['sr_Title'] = $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'][$nm_TaskConnKey]['ar_TaskConnConn'][$nm_TaskConnConnkey]['nm_TaskConnSpeedDown']['sr_Title'] ; 
        $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'][$nm_TaskConnKey]['ar_TaskConnConn'][$nm_TaskConnConnkey]['sr_TaskConnSpeedDown']['vr_Value'] = fn_HumaniseBytes ( floatval($ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'][$nm_TaskConnKey]['ar_TaskConnConn'][$nm_TaskConnConnkey]['nm_TaskConnSpeedDown']['vr_Value']) ,'/s' ) ;
        @fn_Debug ( 'Augmented connection entry' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'][$nm_TaskConnKey]['ar_TaskConnConn'][$nm_TaskConnConnkey]['sr_TaskConnSpeedDown'] ) ;
      }
    }

    @fn_Debug ( 'Augmented and normalised task connections' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Conn'] ) ;

    } // Task reference available
    else {
      @fn_Debug ( 'Task reference not found' ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['bl_Context'] = FALSE ;
    } // Task reference not available

    fn_CollectGlobalStat() ;

  } // Connection to downloader established
  else {
    @fn_Debug ( 'Could not connect to downloader' , $ob_Downloader->bl_ConnStatus ) ;
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
  } // No connection to downloader
  
  @fn_Debug ( 'Summarising variables after processing' ) ;
  @fn_Debug ( 'Site', $ar_Site ) ;
  @fn_Debug ( 'App' , $ar_App ) ;
  @fn_Debug ( 'UI' , $ar_Ui ) ;
  @fn_Debug ( 'Page' , $ar_Page ) ;

  // Transfer variables to the UI
  fn_Debug ( 'Transfer variables to UI' ) ;
  $ob_Ui->TransferVar ( 'ar_Site' , $ar_Site ) ;
  $ob_Ui->TransferVar ( 'ar_App' , $ar_App ) ;
  $ob_Ui->TransferVar ( 'ar_Ui' , $ar_Ui ) ;
  $ob_Ui->TransferVar ( 'ar_Page' , $ar_Page ) ;

  // Render UI
  fn_Debug ('Rendering UI') ;
  $ob_Ui->Render('HtmlStart.tpl') ;
  $ob_Ui->Render('DocHead.tpl') ;
  $ob_Ui->Render('DocBodyStart.tpl') ;
  $ob_Ui->Render('PageNav.tpl') ;
  $ob_Ui->Render('PageBody_Task_Conn.tpl') ;
  $ob_Ui->Render('PageFooter.tpl') ;
  $ob_Ui->Render('DocBodyEnd.tpl') ;
  $ob_Ui->Render('HtmlEnd.tpl') ;
?>