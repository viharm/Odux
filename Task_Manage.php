<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include bootstrap script
  require_once('Bootstrap.php') ;
  
  // Set page title
  $ar_Page['sr_Title']  = 'Task information' ;
  
  @fn_Debug ( 'Summarising variables before processing' ) ;
  @fn_Debug ( 'Site', $ar_Site) ;
  @fn_Debug ( 'App' , $ar_App) ;
  @fn_Debug ( 'UI' , $ar_Ui) ;
  @fn_Debug ( 'Page' , $ar_Page) ;

  // Check if interface is text based
  if ( $ar_Ui['bl_Cli'] ) {
    fn_Debug('UI is CLI') ;
  } // UI is CLI
  else {
    fn_Debug('UI is non-CLI') ;
    $ob_Ui = new cl_WebUi() ;
  } // UI is non-CLI
  
  // Create the downloader object
  @fn_Debug ( 'Creating downloader object with preset config' , $ar_App['ar_Downloader']['ar_Config'] ) ;
  $ob_Downloader = new cl_Downloader($ar_App['ar_Downloader']['ar_Conn']['ar_Config']) ;
  @fn_Debug ( 'Created downloader object' , $ob_Downloader ) ;
  // Check if connection to downloader has been established
  if ( $ob_Downloader->bl_ConnStatus === TRUE ) {
    @fn_Debug ( 'Connected to downloader' , $ob_Downloader->bl_ConnStatus ) ;
    // Set working variable for connection status
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = TRUE ;

    // Process any commands, to include updates and changes in the statuses
    @fn_Debug ( 'Processing submitted data' ) ;
    $ar_SubmitData = array() ;
    @fn_Debug ( 'Checking request method' , $_SERVER['REQUEST_METHOD'] ) ;
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
      @fn_Debug ( 'Detected post method' , $_POST ) ;
      $ar_SubmitData += $_POST ;
    } // Post
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      @fn_Debug ( 'Detected get method' , $_GET ) ;
      $ar_SubmitData += $_GET ;
    } // Get
    @fn_Debug ( 'Combined submitted data' , $ar_SubmitData ) ;

    @fn_Debug ( 'Checking task reference' , $ar_SubmitData['sr_TaskRef'] ) ;
    if ( array_key_exists ( 'sr_TaskRef' , $ar_SubmitData ) and $ar_SubmitData['sr_TaskRef'] !== '' ) {
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = $ar_SubmitData['sr_TaskRef'] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Task reference available in submitted data
    else if ( array_key_exists ( 'sr_DownloaderInstruction' , $ar_SubmitData ) and $ar_SubmitData['sr_DownloaderInstruction'] !== '' ) {
      @fn_Debug ( 'Task reference not directly provided; extracting from sr_DownloaderInstruction' , $ar_SubmitData['sr_DownloaderInstruction'] ) ;
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = array_map ( 'fn_PurifyNumeric' , explode ( '__' , explode ( '__' , $ar_SubmitData['sr_DownloaderInstruction'] , 2 )[1] ) )[0] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Provided in sr_DownloaderInstruction
    else if ( array_key_exists ( 'bt_DownloaderControl' , $ar_SubmitData ) and $ar_SubmitData['bt_DownloaderControl'] !== '' ) {
      @fn_Debug ( 'Task reference not directly provided; extracting from bt_DownloaderControl' , $ar_SubmitData['bt_DownloaderControl'] ) ;
      $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] = array_map ( 'fn_PurifyNumeric' , explode ( '__' , explode ( '__' , $ar_SubmitData['bt_DownloaderControl'] , 2 )[1] ) )[0] ;
      @fn_Debug ( 'Task reference extracted' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
    } // Provided in bt_DownloaderControl
    else {
      @fn_Debug ( 'Task reference not available' ) ;
    } // Task reference not available in submitted data

    if ( ! is_null($ar_App['ar_Downloader']['ar_Task']['sr_TaskRef']) ) {
      
      @fn_Debug ( 'Task reference found' , $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['bl_Context'] = TRUE ;
      
      @fn_Debug ( 'Processing post command' ) ;
      if ( array_key_exists ( 'bt_DownloaderControl' , $ar_SubmitData ) ) {
        @fn_Debug ( 'bt_DownloaderControl detected' , $ar_SubmitData['bt_DownloaderControl'] ) ;
        if ( strstr ( $ar_SubmitData['bt_DownloaderControl'] , 'SetTaskConfig' ) !== FALSE ) {
          @fn_Debug ( 'Detected task configuration change request' , $ar_SubmitData['bt_DownloaderControl'] ) ;
          $ar_SubmitData['sr_DownloaderInstruction'] = $ar_SubmitData['bt_DownloaderControl'] ;
          $ar_Ui['ar_Status']['sr_Mesg'] = fn_Action ( $ob_Downloader , $ar_SubmitData , 'Downloader' ) ;
        } // Submitted string command contains the task config change request
      }
      
      @fn_Debug ( 'Getting task summary' ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['ar_Summary'] =
        cl_Translator::fn_TranslateArray (
          fn_Action (
            $ob_Downloader ,
            array (
              'bt_DownloaderControl'=>'SummariseTask__' .
              $ar_App['ar_Downloader']['ar_Task']['sr_TaskRef'] .
              '__Minimal'
            ) ,
            'Downloader'
          ) ,
          'Downloader' ,
          'TaskDetail'
        )
      ;
      @fn_Debug ( 'Task summary fetched' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Summary'] ) ;

      $ar_App['ar_Downloader']['ar_Task']['ar_Config'] = cl_Translator::fn_TranslateArray ( $ob_Downloader->SummariseTaskConfig($ar_App['ar_Downloader']['ar_Task']['sr_TaskRef']) , 'Downloader' , 'Configs' ) ;
      @fn_Debug ( 'Task configuration fetched' , $ar_App['ar_Downloader']['ar_Task']['ar_Config'] ) ;

      // Load configs dictionary after fetching the global configs (via fn_CollectGlobalStat)
      $ar_Dict['ar_Configs'] = cl_Translator::fn_LoadDict ( 'Downloader' , 'Configs' ) ;
      $ar_Dict['ar_ConfigScope'] = cl_Translator::fn_LoadDict ( 'Downloader' , 'ConfigScope' ) ;
  
      // Create placeholder variable array
      $ar_Ui['ar_Downloader']['ar_Task']['ar_Config'] = array (
        'ar_Task' => array (
          'General'       => array() ,
          'HTTP'          => array() ,
          'HTTP-FTP'      => array() ,
          'HTTP-SFTP'     => array() ,
          'HTTP-FTP-SFTP' => array() ,
          'FTP'           => array() ,
          'FTP-SFTP'      => array() ,
          'SFTP'          => array() ,
          'BT'            => array() ,
          'Metalink'      => array() ,
          'BT-Metalink'   => array()
        ) ,
      ) ;
      @fn_Debug ( 'Placeholder array created to populate configuration and values to pass to UI' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Config'] ) ;
      
      @fn_Debug ( 'Analysing and setting scope based on task status' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Summary']['sr_TaskStatus']['vr_Value'] ) ;
      switch ( strtolower($ar_Ui['ar_Downloader']['ar_Task']['ar_Summary']['sr_TaskStatus']['vr_Value']) ) {
        case 'active' :
          $sr_ScopeRequest = 'Task_Active' ;
          break ;
        case 'paused' :
        case 'queued' :
          $sr_ScopeRequest = 'Task_Dynamic' ;
          break ;
        default :
          $sr_ScopeRequest = 'Task_Static' ;
      }
      @fn_Debug ( 'Scope set' , $sr_ScopeRequest ) ;
  
      @fn_Debug ( 'Iterating through configs' , $ar_Dict['ar_Configs'] ) ;
      foreach ( $ar_Dict['ar_Configs'] as $sr_Key_01=>$ar_Value_01 ) {
        @fn_Debug ( 'Checking for configs of various levels and scopes' , $sr_Key_01 ) ;
        if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits HTTP(S)' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['HTTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // only HTTP(S) configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits HTTP(S) and FTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['HTTP-FTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // HTTP(S) and FTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits HTTP(S) and SFTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['HTTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // HTTP(S) and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits HTTP(S), FTP and SFTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['HTTP-FTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // HTTP(S), FTP and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits FTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['FTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Ftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // only FTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits FTP and SFTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['FTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Ftp','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // FTP and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits SFTP' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // only SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Fits BT' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['BT'] ,
            $ar_Value_01 ,
            array('nm_Task_Bt') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // only BT configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] !== 0
        ) {
          @fn_Debug ( 'Fits metalink' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['Metalink'] ,
            $ar_Value_01 ,
            array('nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // only metalink configs
        else if (
          $ar_Value_01['ar_Scope']['nm_App']           === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] !== 0
        ) {
          @fn_Debug ( 'Fits BT and metalink' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['BT-Metalink'] ,
            $ar_Value_01 ,
            array('nm_Task_Bt','nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // BT and metalink configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] !== 0
        ) {
          @fn_Debug ( 'Fits general' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Task']['ar_Config']['ar_Task']['General'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp','nm_Task_Sftp','nm_Task_Bt','nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_App['ar_Downloader']['ar_Task']['ar_Config']
          ) ;
        } // all other configs
      } // foreach
      @fn_Debug ( 'Configuration shaped for UI' , $ar_Ui['ar_Downloader']['ar_Task']['ar_Config'] ) ;

    } // Task reference available
    else {
      @fn_Debug ( 'Task reference not found' ) ;
      $ar_Ui['ar_Downloader']['ar_Task']['bl_Context'] = FALSE ;
    }

    fn_CollectGlobalStat() ;

  } // Connection to downloader established
  else {
    @fn_Debug ( 'Could not connect to downloader' , $ob_Downloader->bl_ConnStatus ) ;
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
  } // No connection to downloader
  
  @fn_Debug ( 'Summarising variables after processing' ) ;
  @fn_Debug ( 'Site' , $ar_Site ) ;
  @fn_Debug ( 'App' , $ar_App ) ;
  @fn_Debug ( 'UI' , $ar_Ui ) ;
  @fn_Debug ( 'Page' , $ar_Page ) ;

  // Transfer variables to the UI
  fn_Debug ( 'Transfer variables to UI' ) ;
  $ob_Ui->TransferVar ( 'ar_Site' , $ar_Site ) ;
  $ob_Ui->TransferVar ( 'ar_App' , $ar_App ) ;
  $ob_Ui->TransferVar ( 'ar_Ui' , $ar_Ui ) ;
  $ob_Ui->TransferVar ( 'ar_Page' , $ar_Page ) ;



  // Render UI
  fn_Debug ('Rendering UI') ;
  $ob_Ui->Render('HtmlStart.tpl') ;
  $ob_Ui->Render('DocHead.tpl') ;
  $ob_Ui->Render('DocBodyStart.tpl') ;
  $ob_Ui->Render('PageNav.tpl') ;
  $ob_Ui->Render('PageBody_Task_Manage.tpl') ;
  $ob_Ui->Render('PageFooter.tpl') ;
  $ob_Ui->Render('DocBodyEnd.tpl') ;
  $ob_Ui->Render('HtmlEnd.tpl') ;
?>