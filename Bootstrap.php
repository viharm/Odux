<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* This script bootstraps the application by loading the supporting code in the following sequence (least dependent -> most dependent )
  *  01. Backend
  *      a. Configuration
  *      b. Helper classes and functions
  *      c. Modules (load all modules into an array; then include only active ones)
  *  02. Frontend
  *      a. CSS
  *      b. Javascript
  *  This file must be called by each page
  */

  // Session logic
  session_start();
  $_SESSION['ar_CookieParam'] = session_get_cookie_params() ;
  session_set_cookie_params (
    $_SESSION['ar_CookieParam']['lifetime'] ,
    $_SESSION['ar_CookieParam']['path'] ,
    $_SESSION['ar_CookieParam']['domain'] ,
    $_SESSION['ar_CookieParam']['secure'] ,
    $_SESSION['ar_CookieParam']['httponly']
  ) ;
  session_regenerate_id();

  // Global debug configuration; leverage call to this script from all pages. phpKhelper will be loaded later during modules.
  $GLOBALS['bl_DebugSwitch'] = FALSE ;

  // Create base uri and save to interim variable to avoid creating the array and later overwriting it.
  // This script must be located at the root level of the app.
  if ( ! array_key_exists ( 'sr_BaseUri' , $_SESSION ) ) {
    $ar_Uri = explode ( '/' , $_SERVER['PHP_SELF'] ) ;
    array_pop($ar_Uri) ;
    $_SESSION['sr_BaseUri'] = implode ( '/' , $ar_Uri ) ;
    unset($ar_Uri) ;
  }
  
  // Include default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    'Config' .
    DIRECTORY_SEPARATOR .
    '*.default.config.php'
  ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // Include custom configurations; must be after the default configurations
  $sr_Filename = '' ;
  foreach ( glob ( 
    __DIR__ .
    DIRECTORY_SEPARATOR .
    'Config' .
    DIRECTORY_SEPARATOR .
    '*.custom.config.php'
  ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;

  // Include all libraries
  foreach ( glob ( $ar_App['ar_Path']['sr_Inc'] . '*.inc.php' ) as $sr_Filename ) {
    require_once ( realpath ( $sr_Filename ) ) ;
  }
  unset($sr_Filename) ;
  
  // Once libraries are loaded, check if PHP handler is web or text (CLI) once and save to variable
  if ( fn_IsCli() ) { $ar_Ui['bl_Cli'] = TRUE ; } else { $ar_Ui['bl_Cli'] = FALSE ; }
  
  // Include modules
  // Initialise empty string for directory name
  $sr_Directory = '' ;
  // $nm_Counter_01 = 0 ;
  // Start iterating through all subdirectories under module path
  foreach ( glob ( $ar_App['ar_Path']['sr_Module'] . '*' , GLOB_ONLYDIR ) as $sr_Directory ) {
    // Check if PHP handler is CLI
    if ( $ar_Ui['bl_Cli'] === TRUE ) {
      // CLI, check if UI module is we based
      if ( strpos ( $sr_Directory , 'Ui_Web') !== FALSE ) {
        // UI module is web based; skip this iteration
        continue ;
      }
    } else {
      // If PHP handler is non-CLI then ignore Text UI modules (add additional modules not required in non-CLI)
      if ( strpos ( $sr_Directory , 'Ui_Text') !== FALSE ) {
        continue ;
      }
    }

    // Identify the base dir of the module
    $sr_BaseDir = array_pop ( explode ( DIRECTORY_SEPARATOR , $sr_Directory ) ) ; // Can we use "basename" instead of "explode"--"array_pop"
    // Split the dir name of the module, then delete the base dir variable
    $ar_BaseDirFragment = explode ( '_' , $sr_BaseDir ) ;
    // Delete variable no longer necessary
    unset($sr_BaseDir) ;
    // Formulate module name
    $sr_ModuleName = $ar_BaseDirFragment[count($ar_BaseDirFragment)-1] ;
    // Enlist the module to the global variable
    $ar_App['ar_ModuleList'][$ar_BaseDirFragment[0]][] = array (
      'sr_Name' => $sr_ModuleName ,
      'sr_Path' => $sr_Directory
    ) ;
    // Check if an active module exists
    if ( ! array_key_exists ( $ar_BaseDirFragment[0] , $ar_App['ar_ModuleActive'] ) ) {
      // Active module does not exist, add this module as the active module, will not be overwritten in the next iteration
    	$ar_App['ar_ModuleActive'][$ar_BaseDirFragment[0]] = array (
	      'sr_Name' => $sr_ModuleName ,
    	  'sr_Path' => $sr_Directory
    	) ;
    } // Active module does not exist
  }
  unset ( $sr_Directory , $sr_ModuleName ) ;
  
  // Include inc files inside the active module's directories. These should be ideally limited to one. Modules should bootstrap from this file.
  // The primary consumer of the module paths is the 'LoadDict' method in the 'Translator' class
  // THE KEY LIMITATION OF THIS METHOD IS THAT ONLY ONE MODULE PER TYPE IS LOADED; IF MORE THAN ONE MODULE PER TYPE IS REQUIRED THEN THIS LOGIC WILL HAVE TO CHANGE.
  foreach ( $ar_App['ar_ModuleActive'] as $sr_ModuleType=>$ar_Module ) {
    foreach ( glob ( $ar_Module['sr_Path'] . DIRECTORY_SEPARATOR . '*.inc.php' ) as $sr_Filename ) {
      include_once ( realpath ( $sr_Filename ) ) ;
    }
    unset($sr_Filename) ;
  }

  // Collate Javascript for web UI // MOVE TO WEB UI MODULE IN FUTURE VERSION
  if ( $ar_Ui['bl_Cli'] === FALSE ) {
    foreach ( glob ( $ar_App['ar_Path']['sr_Inc'] . '*.inc.js' ) as $sr_Filename ) {
      @fn_Debug ( 'Found script file in include path' , $sr_Filename ) ;
      $ar_Ui['ar_Uri']['ar_Js'][] =
        str_replace ( DIRECTORY_SEPARATOR , '/' , str_replace ( $ar_App['ar_Path']['sr_Base'] , $ar_Ui['ar_Uri']['sr_Base'] , realpath ( $sr_Filename ) ) ) ;
    }
    unset($sr_Filename) ;
  }

  // Collate CSS for web UI // MOVE TO WEB UI MODULE IN FUTURE VERSION
  if ( $ar_Ui['bl_Cli'] === FALSE ) {
    foreach ( glob ( $ar_App['ar_Path']['sr_Inc'] . '*.inc.css' ) as $sr_Filename ) {
      @fn_Debug ( 'Found stylesheet file in include path' , $sr_Filename ) ;
      $ar_Ui['ar_Uri']['ar_Css'][] =
        str_replace ( DIRECTORY_SEPARATOR , '/' , str_replace ( $ar_App['ar_Path']['sr_Base'] , $ar_Ui['ar_Uri']['sr_Base'] , realpath ( $sr_Filename ) ) ) ;
    }
    unset($sr_Filename) ;
  }
?>