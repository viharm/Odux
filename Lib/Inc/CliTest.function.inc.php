<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* http://stackoverflow.com/questions/933367/php-how-to-best-determine-if-the-current-invocation-is-from-cli-or-web-server */
  function fn_IsCli() {
    if (defined('STDIN')) {
      return true;
    }
    if ( php_sapi_name() === 'cli' ) {
      return true;
    }
    if ( array_key_exists ( 'SHELL' , $_ENV ) ) {
      return true;
    }
    if ( empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0 ) {
      return true;
    } 
    if ( array_key_exists('REQUEST_METHOD', $_SERVER) ) {
      return false;
    }
    return false;
  }
?>