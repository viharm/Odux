<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_PurifyNumeric ( $vr_NumericString ) {
    @fn_Debug ( 'First check if argument is either an integer or a float' , $vr_NumericString ) ;
    if ( is_int($vr_NumericString) or is_float($vr_NumericString) ) {
      @fn_Debug ( 'Argument is pure numeric; No action necessry; Retaining value' , $vr_NumericString ) ;
      $rt_Result = $vr_NumericString ;
    } // argument is integer or float
    else {
      @fn_Debug ( 'Checking if argument is numeric' ) ;
      if (is_numeric($vr_NumericString)) {
        @fn_Debug ( 'Argument is numeric; Checking for a negative sign' , $vr_NumericString ) ;
        if ( strpos ( $vr_NumericString , '-' ) === 0 ) {
          $vr_NumericString = ltrim ( $vr_NumericString , '-' ) ;
          $nm_Multiplier = -1 ;
        } // negative
        else {
          $nm_Multiplier = 1 ;
        }// positive
        @fn_Debug ( 'Argument is numeric; Checking for a decimal point (for floats)' , $vr_NumericString ) ;
        if (ctype_digit($vr_NumericString)) {
          @fn_Debug ( 'No decimal point (for floats); Converting to integer' , $vr_NumericString ) ;
          $rt_Result = intval ($vr_NumericString) * $nm_Multiplier  ;
          @fn_Debug ( 'Converted to integer' , $rt_Result ) ;
        }
        else {
          @fn_Debug ( 'Decimal point (for floats) found; Converting to float' , $vr_NumericString ) ;
          $rt_Result = floatval ($vr_NumericString) * $nm_Multiplier  ;
          @fn_Debug ( 'Converted to float' , $rt_Result ) ;
        }
      } // argument is numeric
      else {
        @fn_Debug ( 'Argument is non-numeric; Retaining value' , $vr_NumericString ) ;
        $rt_Result = $vr_NumericString ;
      } // argument is non-numeric
    } // argument is non-integer or non-float
      @fn_Debug ( 'Returning result' , $rt_Result ) ;
      return $rt_Result ;
  }
  
  ?>