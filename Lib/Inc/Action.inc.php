<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_Action ( $ob_Module , $ar_SubmitData , $sr_ModuleType ) {
    @fn_Debug ( 'Module object' , $ob_Module ) ;
    @fn_Debug ( 'Submitted data' , $ar_SubmitData ) ;
    @fn_Debug ( 'Module type' , $sr_ModuleType ) ;
    global $ar_Ui ;
    $rt_Result = NULL ;
    $ar_ConfigChangeRequest = array () ;
    @fn_debug ( 'Checking if module control button was submitted' , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) ;
    if (isset($ar_SubmitData['bt_'.$sr_ModuleType.'Control'])) {
      @fn_debug ( 'Module control button was submitted; Analysing instruction' ) ;
      
      if ( strstr ( $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] , '__' ) ) {

        @fn_Debug ( 'Splitting input string into command and argument' , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) ;
        $ar_Instruction = explode ( '__' , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] , 2 ) ;
        @fn_Debug ( 'Input string split into command and string of argument, identifying command' , $ar_Instruction ) ;
        $sr_Command = $ar_Instruction[0] ;
        @fn_Debug ( 'Command identified, splitting and purifying arguments' , $sr_Command ) ;
        @fn_Debug ( 'Argument(s) provided, splitting and purifying arguments' , $ar_Instruction[1] ) ;
        $ar_Arg = array_map ( 'fn_PurifyNumeric' , explode ( '__' , $ar_Instruction[1] ) ) ;
        @fn_Debug ( 'Argument string split into array and numeric values purified, running command with split arguments, ' , $ar_Arg ) ;
        $rt_Result = call_user_func_array ( array ( $ob_Module , $sr_Command ) , $ar_Arg ) ;
        @fn_Debug ( 'Command executed, result saved' , $rt_Result ) ;
      } // More than one instruction split by '__'; Comnand with arguments
      else {
        @fn_Debug ( 'Instruction contains only command, no arguments; Checking type of command' ) ;
        switch ( $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) {
          case 'SetTaskConfig' :
            @fn_Debug ( 'Task configuration change requested; Looking for more posted data' , $ar_SubmitData ) ;
            foreach ( $ar_SubmitData as $sr_Key_01=>$vr_Val_01 ) {
              @fn_Debug ( 'Checking input' , $sr_Key_01 ) ;
              $sr_OduxVarName = substr (  $sr_Key_01 , strpos ( $sr_Key_01 , '_' , 3 ) + 1 ) ;
              @fn_Debug ( 'Checking changed flag' ) ;
              if ( @$ar_SubmitData['ck_'.$sr_ModuleType.'_Changed_'.$sr_OduxVarName] === 'on' ) {
                @fn_Debug ( 'Found changed option' , $sr_Key_01 ) ;
                $ar_ConfigChangeRequest[$sr_OduxVarName] = $vr_Val_01 ;
              } // Found changed option
            } // foreach posted parameter
            @fn_Debug ( 'Changed configuration options identified and listed; Reverse translating' , $ar_ConfigChangeRequest ) ;
            $ar_Arg = cl_Translator::fn_RevTranslate ( $ar_ConfigChangeRequest , $sr_ModuleType , NULL , 'Configs' ) ;
            @fn_Debug ( 'Changed config reverse translated' , $ar_Arg ) ;
            $rt_Result = call_user_func_array ( array ( $ob_Module , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) , array($ar_SubmitData['sr_TaskRef'],$ar_Arg) ) ;
            @fn_Debug ( 'Action response' , $rt_Result ) ;
            break ;
          case 'SetGlobalConfig' :
            @fn_Debug ( 'Global configuration change requested; Looking for more posted data' , $ar_SubmitData ) ;
            foreach ( $ar_SubmitData as $sr_Key_01=>$vr_Val_01 ) {
              @fn_Debug ( 'Checking input' , $sr_Key_01 ) ;
              $sr_OduxVarName = substr (  $sr_Key_01 , strpos ( $sr_Key_01 , '_' , 3 ) + 1 ) ;
              @fn_Debug ( 'Checking changed flag' ) ;
              if ( @$ar_SubmitData['ck_'.$sr_ModuleType.'_Changed_'.$sr_OduxVarName] === 'on' ) {
                @fn_Debug ( 'Found changed option' , $sr_Key_01 ) ;
                $ar_ConfigChangeRequest[$sr_OduxVarName] = $vr_Val_01 ;
              } // Found changed option
            } // foreach posted parameter
            @fn_Debug ( 'Changed configuration options identified and listed; Reverse translating' , $ar_ConfigChangeRequest ) ;
            $ar_Arg = cl_Translator::fn_RevTranslate ( $ar_ConfigChangeRequest , $sr_ModuleType , NULL , 'Configs' ) ;
            @fn_Debug ( 'Changed config reverse translated' , $ar_Arg ) ;
            $rt_Result = call_user_func_array ( array ( $ob_Module , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) , array($ar_Arg) ) ;
            @fn_Debug ( 'Action response' , $rt_Result ) ;
            break ;
          case 'AddTask' :
            @fn_Debug ( 'Add task requested; Checking submitted data' , $ar_SubmitData ) ;
            if ( isset($ar_SubmitData['tx_Source']) ) {
              @fn_Debug ( 'Sources found; Extracting...' , $ar_SubmitData['tx_Source'] ) ;
              $ar_TaskSource = fn_ArrayfiUrlList($ar_SubmitData['tx_Source']) ;
              @fn_Debug ( 'Source string extracted to array; Deleting from submitted data' , $ar_TaskSource ) ;
              unset ($ar_SubmitData['tx_Source']) ;
              @fn_Debug ( 'Getting task size for first source' , $ar_TaskSource[0] ) ;
              $nm_TaskSize = fn_RemoteFilesize($ar_TaskSource[0]) ;
              @fn_Debug ( 'Checking task size against disk space' , $nm_TaskSize ) ;
              if ( $nm_TaskSize > $ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace'] ) {
                @fn_Debug ( 'Task size is larger than available disk space' , $nm_TaskSize > $ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace'] ) ;
                $rt_Result = 'Task size is larger than available disk space' ;
                break ;
              } // Task too big
              else {
                @fn_Debug ( 'Task size is smaller than available disk space' , $nm_TaskSize > $ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace'] ) ;
              } // Task size is acceptable
              @fn_Debug ( 'Parsing remaining submitted data' , $ar_SubmitData ) ;
              foreach ( $ar_SubmitData as $sr_Key_01=>$vr_Val_01 ) {
                @fn_Debug ( 'Checking input' , $sr_Key_01 ) ;
                $sr_OduxVarName = substr (  $sr_Key_01 , strpos ( $sr_Key_01 , '_' , 3 ) + 1 ) ;
                @fn_Debug ( 'Checking changed flag' ) ;
                if ( @$ar_SubmitData['ck_'.$sr_ModuleType.'_Changed_'.$sr_OduxVarName] === 'on' ) {
                  @fn_Debug ( 'Found changed option' , $sr_Key_01 ) ;
                  $ar_ConfigChangeRequest[$sr_OduxVarName] = $vr_Val_01 ;
                } // Found changed option
              } // foreach posted parameter
              @fn_Debug ( 'Checking if any options were submitted with task' , $ar_ConfigChangeRequest ) ;
              if ( count($ar_ConfigChangeRequest) > 0 ) {
                @fn_Debug ( 'Changed configuration options identified and listed; Reverse translating' , $ar_ConfigChangeRequest ) ;
                $ar_Arg = array ( $ar_TaskSource , cl_Translator::fn_RevTranslate ( $ar_ConfigChangeRequest , $sr_ModuleType , NULL , 'Configs' ) ) ;
              } // Options exist
              else {
                @fn_Debug ( 'No options' , $ar_ConfigChangeRequest ) ;
                $ar_Arg = array ( $ar_TaskSource ) ;
              } // No options
              @fn_Debug ( 'Arguments formulated' , $ar_Arg ) ;
              
              $rt_Result = call_user_func_array ( array ( $ob_Module , $ar_SubmitData['bt_'.$sr_ModuleType.'Control'] ) , $ar_Arg ) ;
              @fn_Debug ( 'Action response' , $rt_Result ) ;
            } // Source string exists
            else {
              $rt_Result = 'No source provided' ;
              break ;
            }
            break ;
          default :
            @fn_Debug ( 'Non-specific command, sending to module' ) ;
            $rt_Result = $ob_Module->$ar_SubmitData['bt_'.$sr_ModuleType.'Control']() ;
        }
      } // Only one instruction component; Command, no arguments
    }
    return $rt_Result ;

  }
?>