<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Create function to sort configs into arrays for passing to UI
  function fn_CreateConfigArray ( &$ar_Out , $ar_In , $ar_Scope = NULL , $sr_ScopeRequest , $ar_ValueArray ) {
    @fn_Debug ( 'Input array' , $ar_In ) ;
    @fn_Debug ( 'Output array' , $ar_Out ) ;
    @fn_Debug ( 'Scope array' , $ar_Scope ) ;
    @fn_Debug ( 'Scope request' , $ar_ScopeRequest ) ;
    @fn_Debug ( 'Value array' , $ar_ValueArray ) ;
    @fn_Debug ( 'Initialising global variables' ) ;
    global $ar_Dict ;
    @fn_Debug ( 'Initialising a read-only placeholder' ) ;
    $bl_Readonly = TRUE ;
    @fn_Debug ( 'Checking if input scope array is non-null' ) ;
    if ( ! is_null($ar_Scope) ) {
      @fn_Debug ( 'Scope array is non-null; Iterating' ) ;
      foreach ( $ar_Scope as $sr_Scope ) {
        @fn_Debug ( 'Checking scope' , $sr_Scope ) ;
        @fn_Debug ( 'Reference scope' , $ar_Dict['ar_ConfigScope']['ar_'.$sr_ScopeRequest] ) ;
        if ( in_array (
          $ar_In['ar_Scope'][$sr_Scope] ,
// TESTING : 
// THE FOLLOWING REFERENCE ARRAY SHOULD BE A VARIABLE BASED ON WHICH SECTION IS PASSED TO THIS FUNCTION
// ADD ANOTHER PARAMETER TO THIS FUNCTION
          $ar_Dict['ar_ConfigScope']['ar_'.$sr_ScopeRequest] ,
          TRUE
        ) ) {
          @fn_Debug ( 'Provided scope is in reference scope; Removing read-only flag' ) ;
          $bl_Readonly = FALSE ;
        } // Provided scope is in static
        else {
          @fn_Debug ( 'Provided scope is not in reference scope; Setting read-only flag' ) ;
          $bl_Readonly = TRUE ;
        }
      } // foreach
    } // Scope argument array is not null
    $ar_Out[$ar_In['sr_OduxVarName']] =  array (
      'sr_Title'        => $ar_In['sr_Title'] ,
      'sr_Descr'        => $ar_In['sr_Descr'] ,
      'vr_Value'        => (
        array_key_exists (
          $ar_In['sr_OduxVarName'] ,
          $ar_ValueArray
        ) ?
          (is_array($ar_ValueArray[$ar_In['sr_OduxVarName']])?$ar_ValueArray[$ar_In['sr_OduxVarName']]['vr_Value']:$ar_ValueArray[$ar_In['sr_OduxVarName']])
        :
          ''
      ) ,
      'ar_AltValueList' => $ar_In['ar_AltValueList'] ,
      'sr_DefaultVal'   => $ar_In['sr_DefaultVal'] ,
      'ar_Scope'        => $ar_In['ar_Scope'] ,
      'bl_Readonly'     => $bl_Readonly
    ) ;
    @fn_Debug ( 'Created configuration variable sub-array' , $ar_Out[$ar_In['sr_OduxVarName']] ) ;
  } // fn_CreateConfigArray

?>