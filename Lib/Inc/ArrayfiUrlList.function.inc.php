<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_ArrayfiUrlList ( $sr_UrlList ) {
    $sr_UrlInput_Clean = trim ( $sr_UrlList ) ;
    $sr_UrlInput_Clean = str_replace ( ' ' , ',' , $sr_UrlInput_Clean ) ;
    $sr_UrlInput_Clean = str_replace ( "\n\r" , ',' , $sr_UrlInput_Clean ) ;
    $sr_UrlInput_Clean = str_replace ( "\n" , ',' , $sr_UrlInput_Clean ) ;
    $sr_UrlInput_Clean = str_replace ( ';' , ',' , $sr_UrlInput_Clean ) ;
    $sr_UrlInput_Clean = str_replace ( '|' , ',' , $sr_UrlInput_Clean ) ;
    $ar_ArrayfiedUrlList = array_map ( 'trim' , explode ( ',' , $sr_UrlInput_Clean ) ) ;
    
    return $ar_ArrayfiedUrlList ;
  }
?>