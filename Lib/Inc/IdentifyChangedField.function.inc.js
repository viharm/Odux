  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_IdentifyChangedField ( sr_ElementIdToCheck , sr_CheckboxIdToChange ) {
    var ob_ElementToCheck = document.getElementById ( sr_ElementIdToCheck ) ;
    var vr_DefaultValue ;
    var vr_CurrentValue ;

    switch (ob_ElementToCheck.type) {
      case 'text':
        vr_DefaultValue = ob_ElementToCheck.defaultValue ;
        vr_CurrentValue = ob_ElementToCheck.value ;
        break ;
      case 'checkbox':
        vr_DefaultValue = ob_ElementToCheck.defaultChecked ;
        vr_CurrentValue = ob_ElementToCheck.checked ;
        break ;
      case 'select-one':
        for (var nm_Counter_01 = 0 ; nm_Counter_01 < ob_ElementToCheck.childElementCount ; nm_Counter_01++ ) {
          if (ob_ElementToCheck.children[nm_Counter_01].getAttribute('selected')==='true') {
            vr_DefaultValue = nm_Counter_01 ;
          }
        }
        vr_CurrentValue = ob_ElementToCheck.selectedIndex ;
        break ;
    }

    if ( vr_DefaultValue !== vr_CurrentValue ) {
      document.getElementById ( sr_CheckboxIdToChange ).checked = true ;
    } else {
      document.getElementById ( sr_CheckboxIdToChange ).checked = false ;
    }
  }