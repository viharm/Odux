<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /*
  * The translator logic translates the responses received from the modules into the platform variables.
  * This logic will accept the following parameters as arguments
  *   1. An associative array to be translated
  *   2. The type of module
  *   3. Reference to the dictionary to be used for the translation
  * It returns an array similar to the input, but with the keys translated as per the dictionary chosen
  *
  * Points to consider
  *   1. File access is an expensive operation, so only open those dictionaries which are requested
  */
  
  class cl_Translator {

    public static function fn_LoadDict ( $sr_ModuleType , $sr_DictName ) {
      global $ar_App ;
      @fn_Debug ('Including the dictionary requested' , $sr_DictName ) ;
//    @fn_Debug ( 'Formulating dictionary path from module path' , $ar_App['ar_ModuleList'][$sr_ModuleType][0]['sr_Path'] ) ;
      @fn_Debug ( 'Formulating dictionary path from module path' , $ar_App['ar_ModuleActive'][$sr_ModuleType]['sr_Path'] ) ;
      $sr_dictFullpath = $ar_App['ar_ModuleActive'][$sr_ModuleType]['sr_Path'] . DIRECTORY_SEPARATOR . $sr_DictName . '.dict.php' ;
      @fn_Debug ( 'Formulated path and filename for dictionary file' , $sr_dictFullpath ) ;
      if (file_exists($sr_dictFullpath)) {
        // Cannot use require_once as the array in the file dies with the function, so needs to be loaded again from the file. Try using static variables or class variables
        require($sr_dictFullpath) ;
      } else {
        @fn_Debug ( 'Dictionary file not found' ) ;
      }
      @fn_Debug ( 'Transferring dictionary array from file to class to variable' , ${ 'ar_' . $sr_ModuleType . '_' . $sr_DictName } ) ;
      $ar_Dict = ${ 'ar_' . $sr_ModuleType . '_' . $sr_DictName } ;
      @fn_Debug ( 'Saved to function variable, deleting included variable' , $ar_Dict ) ;
      // unset ( ${ 'ar_Downloader_' . $sr_DictName } ) ;
      return $ar_Dict ;
    }
    
    public static function fn_TranslateArray ( $ar_Foreign , $sr_ModuleType , $sr_DictName ) {
      @fn_Debug ( 'Load requested dictionary' , $sr_DictName ) ;
      $ar_Dict = self::fn_LoadDict ( $sr_ModuleType , $sr_DictName ) ;
      $rt_Result = array() ;
      @fn_Debug ('Created empty result array' , $rt_Result ) ;
      @fn_Debug ( 'Iterating through input array' , $ar_Foreign ) ;
      foreach ( $ar_Foreign as $sr_Key_01=>$vr_Value_01) {
        @fn_Debug ( 'Analysing result' , $sr_Key_01 ) ;
        if ( array_key_exists ( 'sr_OduxVarName' , $ar_Dict[$sr_Key_01] ) ) {
          @fn_Debug ( 'Translation exists, setting key' , $ar_Dict[$sr_Key_01]['sr_OduxVarName'] ) ;
          $sr_Var = $ar_Dict[$sr_Key_01]['sr_OduxVarName'] ;
        } // Custom var name has been defined
        else {
          $sr_Var = $sr_Key_01 ;
        } // Custom var name not defined

        // The following section checks if the translated property has a limited list of alternative values
        @fn_Debug ( 'Checking if the translation contains a limited list of alternative values' , $ar_Dict[$sr_Key_01] ) ;
        if ( array_key_exists ( 'ar_AltValueList' , $ar_Dict[$sr_Key_01] ) ) {
          @fn_Debug ( 'Alternative values list found, searching value' , $ar_Dict[$sr_Key_01]['ar_AltValueList'] ) ;
          if ( ! is_null($ar_Dict[$sr_Key_01]['ar_AltValueList']) ) {
            if ( array_key_exists ( $vr_Value_01 , $ar_Dict[$sr_Key_01]['ar_AltValueList'] ) ) {
              @fn_Debug ( 'Alternative value found, translating' , $ar_Dict[$sr_Key_01]['ar_AltValueList'][$vr_Value_01] ) ;
              $vr_Value_01 = $ar_Dict[$sr_Key_01]['ar_AltValueList'][$vr_Value_01] ;
              @fn_debug ( 'Value translated' , $vr_Value_01 ) ;
            } // Alt value translation found
          } // Alt value list is non-null
          else {
            @fn_Debug ( 'Alternative value list is zero-length; skipping value' ) ;
          }
        } // alternative value list exists
        else {
          @fn_Debug ( 'No alternative values found; taking no action on the value' ) ;
        } // no alternative values found
        // The following section steps in if the element is an array
        @fn_Debug ( 'Checking if this item is a subarray' , $vr_Value_01 ) ;
        if ( is_array($vr_Value_01) ) {
          @fn_Debug ( 'Item is a subarray, looking for a dictionary specification' , $ar_Dict[$sr_Key_01] ) ;
          if ( array_key_exists ( 'sr_Dict' , $ar_Dict[$sr_Key_01] ) ) {
            @fn_Debug ('Dictionary specification found in subarray' , $ar_Dict[$sr_Key_01]['sr_Dict'] ) ;
            $sr_DictName = $ar_Dict[$sr_Key_01]['sr_Dict'] ;
          } // Dictionary for subarray is specified
          else {
            @fn_Debug ('Dictionary specification not found in subarray' ) ;
          } // Dictionary not specified
          $rt_Result[$sr_Var] = self::fn_TranslateArray ( $vr_Value_01 , $sr_ModuleType , $sr_DictName ) ;
        } // item is a subarray
        else {
          @fn_Debug ( 'Item is not a subarray, assigning value' , $vr_Value_01 ) ;
          $rt_Result[$sr_Var] = array (
            'sr_Title' => $ar_Dict[$sr_Key_01]['sr_Title'] ,
            'vr_Value' => $vr_Value_01
          ) ;
        } // it is not a subarray
        @fn_Debug ( 'Created information variable sub-array' , $rt_Result[$sr_Var] ) ;
      } // foreach $ar_Foreign

      @fn_Debug ( 'Returning result' , $rt_Result ) ;
      return $rt_Result ;
    }

    // Function to translate module response.
    // Specify:
    //   1. the module's native response as a foreign array
    //   2. the type of module (uses the selected module at index 0 of the module list type)
    //   2. Dictionary to use
    public static function fn_Translate ( $ar_Foreign , $sr_ModuleType , $sr_DictName ) {
      
      @fn_Debug ( 'Load requested dictionary' , $sr_DictName ) ;
      $ar_Dict = self::fn_LoadDict ( $sr_ModuleType , $sr_DictName ) ;

      @fn_Debug ('Creating empty result array' ) ;
      $rt_Result = array() ;
        @fn_Debug ( 'Input array is associative, stepping in' ) ;
        @fn_Debug ( 'Iterating through results fetched' , $ar_Foreign ) ;
        foreach ( $ar_Foreign as $sr_Key_01=>$vr_Value_01 ) {

          @fn_Debug ( 'Check if this key exists in dictionary' , $sr_Key_01 ) ;
          @fn_Debug ( 'Checking dictionary' , $sr_DictName ) ;
          if ( array_key_exists ( $sr_Key_01 , $ar_Dict ) ) {
            @fn_Debug ( 'Key exists in dictionary, then check if translation exists' ) ;
            if ( array_key_exists ( 'sr_OduxVarName' , $ar_Dict[$sr_Key_01] ) ) {
              @fn_Debug ( 'Translation exists, setting key' , $ar_Dict[$sr_Key_01]['sr_OduxVarName'] ) ;
              $sr_ResultKey = $ar_Dict[$sr_Key_01]['sr_OduxVarName'] ;
            } // Translation exists in dictionary

            // The following section checks if the translated property has a limited list of alternative values
            @fn_Debug ( 'Checking if the translation contains a limited list of alternative values' , $ar_Dict[$sr_Key_01] ) ;
            if ( array_key_exists ( 'ar_AltValueList' , $ar_Dict[$sr_Key_01] ) ) {
              @fn_Debug ( 'Alternative values list found, searching value' , $ar_Dict[$sr_Key_01]['ar_AltValueList'] ) ;
              if ( ! is_null($ar_Dict[$sr_Key_01]['ar_AltValueList']) ) {
                if ( array_key_exists ( $vr_Value_01 , $ar_Dict[$sr_Key_01]['ar_AltValueList'] ) ) {
                  @fn_Debug ( 'Alternative value found, translating' , $ar_Dict[$sr_Key_01]['ar_AltValueList'][$vr_Value_01] ) ;
                  $vr_Value_01 = $ar_Dict[$sr_Key_01]['ar_AltValueList'][$vr_Value_01] ;
                  @fn_debug ( 'Value translated' , $vr_Value_01 ) ;
                } // Alt value translation found
              } // Alt value list is non-null
            } // alternative value list exists
            else {
              @fn_Debug ( 'No alternative values found; taking no action on the value' ) ;
            } // no alternative values found
          } // key exists in dictionary 
          else {
            @fn_Debug ( 'Key not found in dictionary, retain key name' , $sr_Key_01 ) ;
            $sr_ResultKey = $sr_Key_01 ;
          } // key not in dictionary
          
          // The following section steps in if the element is an array
          @fn_Debug ( 'Checking if this item is a subarray' , $vr_Value_01 ) ;
          if ( is_array($vr_Value_01) ) {
            @fn_Debug ( 'It is a subarray, looking for a dictionary specification' , $ar_Dict[$sr_Key_01] ) ;
            if ( array_key_exists ( 'sr_Dict' , $ar_Dict[$sr_Key_01] ) ) {
              @fn_Debug ('Dictionary specification found in subarray' , $ar_Dict[$sr_Key_01]['sr_Dict'] ) ;
              $sr_DictName = $ar_Dict[$sr_Key_01]['sr_Dict'] ;
            } // Dictionary for subarray is specified
            else {
              @fn_Debug ('Dictionary specification not found in subarray' ) ;
            } // Dictionary not specified
            $rt_Result[$sr_ResultKey] = self::fn_Translate ( $vr_Value_01 , $sr_ModuleType , $sr_DictName ) ;
          } // item is a subarray
          else {
            @fn_Debug ( 'Item is not a subarray, assigning value' , $vr_Value_01 ) ;
            $rt_Result[$sr_ResultKey] = $vr_Value_01 ;
          } // it is not a subarray

        } // foreach  $ar_Foreign
      return $rt_Result ;
    }
    
    public static function fn_RevTranslate ( $ar_Native , $sr_ModuleType , $ar_Dict = NULL , $sr_DictName ) {

      $rt_Result = array() ;
      
      if ( is_null($ar_Dict) ) {
        @fn_Debug ( 'Load requested dictionary' , $sr_DictName ) ;
        $ar_Dict = self::fn_LoadDict ( $sr_ModuleType , $sr_DictName ) ;
        @fn_Debug ( 'Dictionary loaded, traversing dictionary' , $ar_Dict ) ;
      }
      
      foreach ( $ar_Native as $sr_NativeKey=>$vr_NativeVal ) {
        foreach ( $ar_Dict as $sr_ForeignKey=>$ar_Translation ) {
          @fn_Debug ( 'Checking key ' . $sr_ForeignKey , $ar_Translation ) ;
          if ( $ar_Translation['sr_OduxVarName'] === $sr_NativeKey ) {
            @fn_Debug ( 'Translation exists; checking for alternate values' , $ar_Translation['ar_AltValueList'] ) ;
            if ( is_array($ar_Translation['ar_AltValueList']) ) {
              @fn_Debug ( 'Alternate values list found, finding corresponding value' , $vr_NativeVal ) ;
              $vr_ForeignVal = array_keys ( $ar_Translation['ar_AltValueList'] , $vr_NativeVal ) [0] ;
              @fn_Debug ( 'Foreign value found' , $vr_ForeignVal ) ;
              $rt_Result[$sr_ForeignKey] = $vr_ForeignVal ;
            } // alt values exist
            else {
              @fn_Debug ( 'Alternate values list not found, applying input value' , $vr_NativeVal ) ;
              $rt_Result[$sr_ForeignKey] = $vr_NativeVal ;
            } // no alt values
            
          } // Native config name match found
        }
      }
      return $rt_Result ;
    }
  }

?>