<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  /* Function to write a custom configuration file based on a standardised filename and passed array variable
    Parameters
      Name of variable to update/edit/replace
      Value
      Name of config (used for filename)
      Purge before update?
      Path to config (if not default)
    Return
      True if success, fales if failure
  */
  /* Sample code
  $sr_TestVar = "ar_App['ar_Downloader']['ar_Conn']['ar_Config']" ;
  @fn_Debug ( 'Variable name' , $sr_TestVar ) ;
  $ar_TestVal = array (
    'host'      => 'maltus' ,
    'port'      => '6800' ,
    'rpcsecret' => 'RpcSecretToken' ,
    'secure'    => FALSE ,
    'cacert'    => NULL ,
    'rpcuser'   => NULL ,
    'rpcpass'   => NULL ,
    'proxy'     => array (
      'type' => NULL ,
      'host' => NULL ,
      'port' => NULL ,
      'user' => NULL ,
      'pass' => NULL
    )
  ) ;
  @fn_Debug ( 'Variable value' , $ar_TestVal ) ;
  $sr_TestConfigname = 'Downloader' ;
  @fn_Debug ( 'Config name' , $sr_TestConfigname ) ;
  $sr_TestConfigpath = $ar_App['ar_ModuleActive'][$sr_ModuleType]['sr_Path'] ;
  @fn_Debug ( 'Config path' , $sr_TestConfigpath ) ;
  $bl_WriteStatus = fn_WriteConfig ( $sr_TestVar , $ar_TestVal , $sr_TestConfigname , FALSE , $sr_TestConfigpath ) ;
  @fn_Debug ( 'Write status' , $bl_WriteStatus ) ;
  */

  function fn_WriteConfig ( $sr_VarName , $vr_VarValue , $sr_ConfigName = NULL , $bl_Purge = FALSE , $sr_ConfigPath = NULL ) {
    @fn_Debug ( 'Checking inputs' , func_get_args() ) ;
    if ( isset($sr_VarName) and isset($vr_VarValue) ) {
      @fn_Debug ( 'Variable name and value are provided ; checking config path' , $sr_ConfigPath ) ;
      if (is_null($sr_ConfigPath)) {
        @fn_Debug ( 'Config path is null (probably not specified)' ) ;
        global $ar_App ;
        $sr_ConfigPath = $ar_App['ar_Path']['sr_Config'] ;
        @fn_Debug ( 'Config path set to global config path' , $sr_ConfigPath ) ;
      }
      // Tidy path
      $sr_ConfigPath = rtrim ( $sr_ConfigPath , '/' ) . DIRECTORY_SEPARATOR ;
      @fn_Debug ( 'Tidied config path' , $sr_ConfigPath ) ;
      @fn_Debug ( 'Checking config name' , $sr_ConfigName ) ;
      if (is_null($sr_ConfigName)) {
        @fn_Debug ( 'Config name is null (probably not specified)' ) ;
        $sr_ConfigName = $sr_VarName ;
        @fn_Debug ( 'Config name set to variable name' , $sr_ConfigName ) ;
      }
      @fn_Debug ( 'Checking if purge was requested' ) ;
      if ( $bl_Purge === TRUE ) {
        @fn_Debug ( 'Purge requested; formulating file contents' ) ;
        $sr_FileContent = $sr_VarName . ' = ' . var_export ( $vr_VarValue , TRUE ) ;
      } // Purge of existing value is requested
      else {
        @fn_Debug ( 'Purge not requested; formulating file contents' ) ;
        $sr_FileContent = '<?php' . PHP_EOL . '$' . $sr_VarName . ' = array_replace ( $' . $sr_VarName . ' , ' . var_export ( $vr_VarValue , TRUE ) . ') ;' . PHP_EOL . '?>' ;
      } // purge not requested
      @fn_Debug ( 'File contents formulated; now writing' , $sr_FileContent ) ;
      $ob_File = fopen ( $sr_ConfigPath . $sr_ConfigName . '.custom.config.php' , 'w' ) ;
      @fn_Debug ( 'File opening attempted; checking success' , $ob_File ) ;
      if ( $ob_File !== FALSE ) {
        @fn_Debug ( 'File opened; now atempting write' ) ;
        $vr_WriteResult = fwrite ( $ob_File , $sr_FileContent ) ;
        @fn_Debug ( 'File write attempted; checking status' , $vr_WriteResult ) ;
        if ( $vr_WriteResult !== FALSE ) {
          @fn_Debug ( 'File written, setting return value' ) ;
          $rt_Response = TRUE ;
        } // write success
        else {
          @fn_Debug ( 'File write failed, setting return value' ) ;
          $rt_Response = FALSE ;
        } // write failure
        @fn_Debug ( 'Closing file' ) ;
        fclose($ob_File) ;
      } // successfully opened file
      else {
        @fn_Debug ( 'File open failed, setting return value' ) ;
        $rt_Response = FALSE ;
      } // could not open file
    }
    else {
      @fn_Debug ( 'Insufficient inputs, setting return value' ) ;
      $rt_Response = FALSE ;
    } // Either var name or var value is not provided
    return $rt_Response ;
  }
?>