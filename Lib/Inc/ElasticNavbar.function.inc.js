  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

$(function () { // Same as document.addEventListener("DOMContentLoaded"...

  // Same as document.querySelector("#navbarToggle").addEventListener("blur",...
  $("#bt_ToggleNavbar_Page").blur(function (event) {
    var screenWidth = window.innerWidth;
    console.log(event);
    if (screenWidth < 768) {
      setTimeout(function(){$("#nv_Page").collapse('hide');}, 250);
    }
  });

  $("#bt_ToggleNavbar_Page").click(function (event) {
    $(event.target).focus();
  });
});
