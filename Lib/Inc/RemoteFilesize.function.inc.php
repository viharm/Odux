<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_RemoteFilesize ( $sr_Source ) {
    // http://php.net/manual/en/function.filesize.php
    $ob_Conn = curl_init($sr_Source);
    @fn_Debug ( 'Initiate cURL; Configuring...' , $ob_Conn ) ;
    curl_setopt ( $ob_Conn , CURLOPT_NOBODY, true ) ;
    curl_setopt ( $ob_Conn , CURLOPT_RETURNTRANSFER, true ) ;
    curl_setopt ( $ob_Conn , CURLOPT_HEADER, true ) ;
    curl_setopt ( $ob_Conn , CURLOPT_FOLLOWLOCATION, true ) ; //not necessary unless the file redirects (like the PHP example we're using here)
    $vr_Response = curl_exec($ob_Conn) ;
    @fn_Debug ( 'cURL executed; Closing connection and analysing response' , $vr_Response ) ;
    curl_close($ob_Conn) ;
    if ($vr_Response === false) {
      @fn_Debug ( 'cURL failed' ) ;
      $rt_Result = FALSE ;
    }

    $vr_ContentSize = NULL ;
    $vr_HttpStatus = NULL ;
    if ( preg_match ( '/^HTTP\/1\.[01] (\d\d\d)/' , $vr_Response , $ar_Matches ) ) {
      $vr_HttpStatus = (int)$ar_Matches[1] ;
    }
    if ( preg_match ( '/Content-Length: (\d+)/' , $vr_Response , $ar_Matches ) ) {
      $vr_ContentSize = (int)$ar_Matches[1] ;
      $rt_Result = intval($vr_ContentSize) ;
    }
    
    return $rt_Result ;
  }
?>