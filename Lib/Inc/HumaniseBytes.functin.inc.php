<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

 /*
 * Function to scale Bytes to human readable units
 */
  function fn_HumaniseBytes ( $sr_BytesToHumanise , $sr_SpeedOverTime='' ) {
    $nm_BytesToHumanise = floatval($sr_BytesToHumanise) ;
    if ( $nm_BytesToHumanise >= 1024 ) {
      $nm_HumanisedBytes_K = $nm_BytesToHumanise/1024 ;
      if ( $nm_HumanisedBytes_K >= 1024 ) {
        $nm_HumanisedBytes_M = $nm_HumanisedBytes_K/1024 ;
        if ( $nm_HumanisedBytes_M >= 1024 ) {
          $nm_HumanisedBytes_G = $nm_HumanisedBytes_M/1024 ;
          if ( $nm_HumanisedBytes_G >= 1024 ) {
            $nm_HumanisedBytes_T = $nm_HumanisedBytes_G/1024 ;
            if ( $nm_HumanisedBytes_T >= 1024 ) {
              $nm_HumanisedBytes_P = $nm_HumanisedBytes_T/1024 ;
              $sr_Result = sprintf ( "%0.2f PiB%s" , $nm_HumanisedBytes_P , $sr_SpeedOverTime ) ;
            } else {
              $sr_Result = sprintf ( "%0.2f TiB%s" , $nm_HumanisedBytes_T , $sr_SpeedOverTime ) ;
            }
          } else {
            $sr_Result = sprintf ( "%0.2f GiB%s" , $nm_HumanisedBytes_G , $sr_SpeedOverTime ) ;
          }
        } else {
          $sr_Result = sprintf ( "%0.2f MiB%s" , $nm_HumanisedBytes_M , $sr_SpeedOverTime ) ;
        }
      } else {
        $sr_Result = sprintf ( "%0.2f KiB%s" , $nm_HumanisedBytes_K , $sr_SpeedOverTime ) ;
      }
    } else {
      $sr_Result = sprintf ( "%0.2f B%s" , $nm_BytesToHumanise , $sr_SpeedOverTime ) ;
    }
    return $sr_Result ;
  }
?>