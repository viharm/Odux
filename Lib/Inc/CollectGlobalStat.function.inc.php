<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  function fn_CollectGlobalStat() {
    // Initialise access to global variables, properties and objects
    global $ar_Ui ;
    global $ob_Downloader ;
    
    // Get global statistics
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat'] = cl_Translator::fn_Translate (
      $ob_Downloader->ListGlobalStat() ,
      'Downloader' ,
      'Result'
    ) ;
    @fn_Debug ( 'Fetched stats from the downloader' , $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat'] ) ;
    
    // Humanise speed units
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat']['sr_SpeedDown'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat']['nm_SpeedDown'] , '/s' ) ;
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat']['sr_SpeedUp'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Global']['ar_Stat']['nm_SpeedUp'] , '/s' ) ;
    
    // Get global configuration
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Config'] = cl_Translator::fn_Translate (
      $ob_Downloader->SummariseGlobalConfig() ,
      'Downloader' ,
      'Configs'
    ) ;
    @fn_Debug ( 'Fetched global config from the downloader' , $ar_Ui['ar_Downloader']['ar_Global']['ar_Config'] ) ;

    // Humanise speed units
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['sr_GlobalLimitDown'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['nm_GlobalLimitDown'] , '/s' ) ;
    $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['sr_GlobalLimitUp'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['nm_GlobalLimitUp'] , '/s' ) ;
    
    // Calculate disk size, free space, used space and percentage used; possible only after connection with downloader as the path is needed
    $ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace'] = disk_free_space($ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['sr_SavePath']) ;
    $ar_Ui['ar_Downloader']['ar_Disk']['nm_Size'] = disk_total_space($ar_Ui['ar_Downloader']['ar_Global']['ar_Config']['sr_SavePath']) ;
    $ar_Ui['ar_Downloader']['ar_Disk']['nm_UsedSpace'] = floatval($ar_Ui['ar_Downloader']['ar_Disk']['nm_Size']) - floatval($ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace']) ;
    $ar_Ui['ar_Downloader']['ar_Disk']['nm_UsedPercent'] = $ar_Ui['ar_Downloader']['ar_Disk']['nm_UsedSpace'] / $ar_Ui['ar_Downloader']['ar_Disk']['nm_Size'] ;
    
    // Humanise size units
    $ar_Ui['ar_Downloader']['ar_Disk']['sr_FreeSpace'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Disk']['nm_FreeSpace'] ) ;
    $ar_Ui['ar_Downloader']['ar_Disk']['sr_Size'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Disk']['nm_Size'] ) ;
    $ar_Ui['ar_Downloader']['ar_Disk']['sr_UsedSpace'] =
      fn_HumaniseBytes ( $ar_Ui['ar_Downloader']['ar_Disk']['nm_UsedSpace'] ) ;
  }

?>