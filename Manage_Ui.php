<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include bootstrap script
  require_once('Bootstrap.php') ;
  
  // Set page title
  $ar_Page['sr_Title']  = 'Manage' ;
  @fn_Debug ('Page title set' , $ar_Page['sr_Title'] ) ;
  
  fn_Debug ('Site',$ar_Site);
  fn_Debug ('App',$ar_App);
  fn_Debug ('UI',$ar_Ui);
  fn_Debug ('Page',$ar_Page);

  // Check if interface is text based
  if ( $ar_Ui['bl_Cli'] ) {
    fn_Debug('UI is CLI') ;
  } // UI is CLI
  else {
    fn_Debug('UI is non-CLI') ;
    $ob_Ui = new cl_WebUi() ;
  } // UI is non-CLI
  
  
    // Process any commands, to include updates and changes in the statuses
  if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if (isset($_POST['bt_UiControl'])) {
      $ar_Ui['ar_Status']['sr_Mesg'] = fn_Action ( $ob_Ui , $_POST , 'Ui' ) ; 
    }
  }

  $ar_Ui['ar_UiInfo'] = array (
    'sr_CacheDir' => array (
      'sr_Title' => 'Cache directory' ,
      'vr_Value' => $ob_Ui->CacheDir()
    ) ,
    'sr_CompiledDir' => array (
      'sr_Title' => 'Compiled directory' ,
      'vr_Value' => $ob_Ui->CompiledDir()
    ) ,
    'ar_ConfigDir' => array (
      'sr_Title' => 'Configuration directory' ,
      'vr_Value' => $ob_Ui->ConfigDir()
    ) ,
    'ar_TemplateDir' => array (
      'sr_Title' => 'Template directory' ,
      'vr_Value' => $ob_Ui->TemplateDir()
    ) ,
    'ar_PluginDir' => array (
      'sr_Title' => 'Plugin directory' ,
      'vr_Value' => $ob_Ui->PluginDir()
    ) ,
    'ar_TemplateVar' => array (
      'sr_Title' => 'Template variables' ,
      'vr_Value' => $ob_Ui->ListTemplateVar()
    ) ,
    'ar_ConfigVar' => array (
      'sr_Title' => 'Configuration variables' ,
      'vr_Value' => $ob_Ui->ListConfigVar()
    )
  ) ;

  // Check if downloader class exists
  @fn_Debug ('Checking if downloader class exists') ;
  if ( class_exists('cl_Downloader') ) {
    @fn_Debug ('Downloader class exists') ;
    // Create the downloader object
    $ob_Downloader = new cl_Downloader($ar_App['ar_Downloader']['ar_Conn']['ar_Config']) ;
    @fn_Debug ( 'Created downloader object' , $ob_Downloader ) ;
    // Check if connection to downloader has been established
    if ( $ob_Downloader->bl_ConnStatus === TRUE ) {
      @fn_Debug ( 'Connected to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      // Set working variable for connection status
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = TRUE ;
  
      fn_CollectGlobalStat() ;
  
    } // Connection to downloader established
    else {
      @fn_Debug ( 'Could not connect to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
    } // No connection to downloader
  } // class exists
  else {
    @fn_Debug ('Downloader class does not exist') ;
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
  } // class does not exist

  // Transfer variables to the UI
  fn_Debug ( 'Transfer variables to UI' ) ;
  $ob_Ui->TransferVar ( 'ar_Site' , $ar_Site ) ;
  $ob_Ui->TransferVar ( 'ar_App' , $ar_App ) ;
  $ob_Ui->TransferVar ( 'ar_Ui' , $ar_Ui ) ;
  $ob_Ui->TransferVar ( 'ar_Page' , $ar_Page ) ;

  // Render UI
  fn_Debug ('Rendering UI') ;
  $ob_Ui->Render('HtmlStart.tpl') ;
  $ob_Ui->Render('DocHead.tpl') ;
  $ob_Ui->Render('DocBodyStart.tpl') ;
  $ob_Ui->Render('PageNav.tpl') ;
  $ob_Ui->Render('PageBody_Manage_Ui.tpl') ;
  $ob_Ui->Render('PageFooter.tpl') ;
  $ob_Ui->Render('DocBodyEnd.tpl') ;
  $ob_Ui->Render('HtmlEnd.tpl') ;
?>