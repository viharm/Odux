<?php
  /*
  Copyright 2016 viharm
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  */

  // Include bootstrap script
  require_once('Bootstrap.php') ;
  // Set page title
  $ar_Page['sr_Title']  = 'Manage' ;
  @fn_Debug ('Page title set' , $ar_Page['sr_Title'] ) ;

  fn_Debug ( 'Site' , $ar_Site ) ;
  fn_Debug ( 'App' , $ar_App ) ;
  fn_Debug ( 'UI' , $ar_Ui ) ;
  fn_Debug ( 'Page' , $ar_Page ) ;

  // Check if interface is text based
  if ( $ar_Ui['bl_Cli'] ) {
    fn_Debug('UI is CLI') ;
  } // UI is CLI
  else {
    fn_Debug('UI is non-CLI') ;
    $ob_Ui = new cl_WebUi() ;
  } // UI is non-CLI

  // Check if downloader class exists
  @fn_Debug ('Checking if downloader class exists') ;
  if ( class_exists('cl_Downloader') ) {
    @fn_Debug ('Downloader class exists') ;
    // Create the downloader object
    $ob_Downloader = new cl_Downloader($ar_App['ar_Downloader']['ar_Conn']['ar_Config']) ;
    @fn_Debug ( 'Created downloader object' , $ob_Downloader ) ;
    // Check if connection to downloader has been established
    if ( $ob_Downloader->bl_ConnStatus === TRUE ) {
      @fn_Debug ( 'Connected to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      // Set working variable for connection status
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = TRUE ;
  
      // Process any commands, to include updates and changes in the statuses
      if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
        $ar_Ui['ar_Status']['sr_Mesg'] = fn_Action ( $ob_Downloader , $_POST , 'Downloader' ) ; 
      }
  
      fn_CollectGlobalStat() ;
  
      // Load configs dictionary after fetching the global configs (via fn_CollectGlobalStat)
      $ar_Dict['ar_Configs'] = cl_Translator::fn_LoadDict ( 'Downloader' , 'Configs' ) ;
      $ar_Dict['ar_ConfigScope'] = cl_Translator::fn_LoadDict ( 'Downloader' , 'ConfigScope' ) ;
  
      // Create placeholder variable array
      $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage'] = array (
        'ar_App'  => array() ,
        'ar_Task' => array (
          'General'       => array() ,
          'HTTP'          => array() ,
          'HTTP-FTP'      => array() ,
          'HTTP-SFTP'     => array() ,
          'HTTP-FTP-SFTP' => array() ,
          'FTP'           => array() ,
          'FTP-SFTP'      => array() ,
          'SFTP'          => array() ,
          'BT'            => array() ,
          'Metalink'      => array() ,
          'BT-Metalink'   => array()
        ) ,
        'ar_Info' => array()
      ) ;
      @fn_Debug ( 'Placeholder array created to populate configuration and values to pass to UI' , $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage'] ) ;
      
      $sr_ScopeRequest = 'Global_Dynamic' ;
      @fn_Debug ( 'Iterating through configs' , $ar_Dict['ar_Configs'] ) ;
      foreach ( $ar_Dict['ar_Configs'] as $sr_Key_01=>$ar_Value_01 ) {
        @fn_Debug ( 'Checking for configs of various levels and scopes' ) ;
        if (
          $ar_Value_01['ar_Scope']['nm_App']           !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          @fn_Debug ( 'Config is app-only; Passing config array to function for converting' , $ar_Value_01 ) ;
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_App'] ,
            $ar_Value_01 ,
            array ('nm_App') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only app level configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['HTTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only HTTP(S) configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['HTTP-FTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // HTTP(S) and FTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['HTTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // HTTP(S) and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['HTTP-FTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // HTTP(S), FTP and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['FTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Ftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only FTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['FTP-SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Ftp','nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // FTP and SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['SFTP'] ,
            $ar_Value_01 ,
            array('nm_Task_Sftp') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only SFTP configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] === 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['BT'] ,
            $ar_Value_01 ,
            array('nm_Task_Bt') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only BT configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] !== 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['Metalink'] ,
            $ar_Value_01 ,
            array('nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // only metalink configs
        else if (
          $ar_Value_01['ar_Scope']['nm_Task_Http']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Ftp']      === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Sftp']     === 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Bt']       !== 0 and
          $ar_Value_01['ar_Scope']['nm_Task_Metalink'] !== 0
        ) {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['BT-Metalink'] ,
            $ar_Value_01 ,
            array('nm_Task_Bt','nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // BT and metalink configs
        else {
          fn_CreateConfigArray (
            $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Task']['General'] ,
            $ar_Value_01 ,
            array('nm_Task_Http','nm_Task_Ftp','nm_Task_Sftp','nm_Task_Bt','nm_Task_Metalink') ,
            $sr_ScopeRequest ,
            $ar_Ui['ar_Downloader']['ar_Global']['ar_Config']
          ) ;
        } // all other configs
      } // foreach
  
      // Organise downloader / version information
      $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Info'] += array (
        'ar_HostLocn'  => array (
          'sr_Title' => 'Host' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['host']
        ) ,
        'ar_HostPort'  => array (
          'sr_Title' => 'Port' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['port']
        ) ,
        'ar_RpcSecure' => array (
          'sr_Title' => 'Secure' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['secure']
        ) ,
        'ar_CaCert'    => array (
          'sr_Title' => 'CA cert' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['cacert']
        ) ,
        'ar_ProxyType' => array (
          'sr_Title' => 'Proxy type' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['proxy']['type']
        ) ,
        'ar_ProxyLocn' => array (
          'sr_Title' => 'Proxy host' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['proxy']['host']
        ) ,
        'ar_ProxyPort' => array (
          'sr_Title' => 'Proxy port' ,
          'vr_Value' => $ar_App['ar_Downloader']['ar_Conn']['ar_Config']['proxy']['port']
        )
      ) ;
      $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Info'] += cl_Translator::fn_TranslateArray ( $ob_Downloader->SessionInfo() , 'Downloader' , 'Result' ) ;
      $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Info'] += cl_Translator::fn_TranslateArray ( $ob_Downloader->Version() , 'Downloader' , 'Result' ) ;
      @fn_Debug ( 'Created info variables' , $ar_Ui['ar_Downloader']['ar_Global']['ar_ConfigPage']['ar_Info'] ) ;
  
    } // Connection to downloader established
    else {
      @fn_Debug ( 'Could not connect to downloader' , $ob_Downloader->bl_ConnStatus ) ;
      $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
    } // No connection to downloader
  } // class exists
  else {
    @fn_Debug ('Downloader class does not exist') ;
    $ar_Ui['ar_Downloader']['ar_Global']['bl_ConnStatus'] = FALSE ;
  } // class does not exist

  // Transfer variables to the UI
  fn_Debug ( 'Transfer variables to UI' ) ;
  $ob_Ui->TransferVar ( 'ar_Site' , $ar_Site ) ;
  $ob_Ui->TransferVar ( 'ar_App' , $ar_App ) ;
  $ob_Ui->TransferVar ( 'ar_Ui' , $ar_Ui ) ;
  $ob_Ui->TransferVar ( 'ar_Page' , $ar_Page ) ;

  // Render UI
  fn_Debug ('Rendering UI') ;
  $ob_Ui->Render('HtmlStart.tpl') ;
  $ob_Ui->Render('DocHead.tpl') ;
  $ob_Ui->Render('DocBodyStart.tpl') ;
  $ob_Ui->Render('PageNav.tpl') ;
  $ob_Ui->Render('PageBody_Manage_Downloader.tpl') ;
  $ob_Ui->Render('PageFooter.tpl') ;
  $ob_Ui->Render('DocBodyEnd.tpl') ;
  $ob_Ui->Render('HtmlEnd.tpl') ;
?>